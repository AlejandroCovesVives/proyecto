/**
 * Created by Esteban on 17/01/2016.
 */


function gentablapdf(quien){
    switch (quien) {

        case 1:genpdf(document.getElementById("t1"));
            break;
        case 2: genpdf(document.getElementById("t2"));
            break;
        case 3 :genpdf(document.getElementById("t3"));
            break;
    }
}


function imprimirDocumento(){
    var doc = new jsPDF({unit: 'pt', lineHeight: 1.5, orientation: 'p'});
    tabla1=document.getElementById("t1");
    tabla2=document.getElementById("t2");
    tabla3=document.getElementById("t3");

    var res = doc.autoTableHtmlToJson(tabla1);
    doc.autoTable(res.columns, res.data, {startY: 60});
    doc.addPage();

    var res2 = doc.autoTableHtmlToJson(tabla2);
    doc.autoTable(res2.columns, res2.data, {startY: 60});
    doc.addPage();

    var res3 = doc.autoTableHtmlToJson(tabla3);
    doc.autoTable(res3.columns, res3.data, {startY: 60});
    doc.addPage();

    doc.autoTable(res.columns, res.data, {startY: 60});
    //para que la posicion del siguiente la pille bien
    var pos=doc.autoTableEndPosY();
    doc.autoTable(res2.columns, res2.data, {startY: pos+10});
    pos=doc.autoTableEndPosY();
    doc.autoTable(res3.columns, res3.data, {startY: pos+10});

    var array=new Array();
    for(var i=0; i<10;i++){
        array[i]=new Array();
    }

    for(var i=0; i<array.length; i++){
        for(var j=0; j<10; j++){
            array[i][j]=j;
        }
    }


        /*var columnas_array=[
            {title: "Colum1", dataKey: "Colum1"},
            {title: "Colum2", dataKey: "Colum2"},
            {title: "Colum3", dataKey: "Colum3"},
            {title: "Colum4", dataKey: "Colum4"},
            {title: "Colum5", dataKey: "Colum5"},
            {title: "Colum6", dataKey: "Colum6"},
            {title: "Colum7", dataKey: "Colum7"},
            {title: "Colum8", dataKey: "Colum8"},
            {title: "Colum9", dataKey: "Colum9"},
            {title: "Colum10", dataKey: "Colum10"},
        ];

    var filas_array=[];
    var ayyay_temp;
    for (var i=0; i<array.length;i++){
        array_temp=array[i];
        for(var j=0; j<array[0].length;j++){
            filas_array[i]={"Colum1":array_temp[j],
                "j":array[i][j]
            };
        }
    }*/




    var filas=JSON.parse(CJSON);

    doc.addPage();
    doc.autoTable(columnas,filas,{startY: 60});

    var string = doc.output('datauristring');
    var x = window.open();
    x.document.open();
    x.document.location=string;
}



function genpdf(tabla){
    var doc = new jsPDF({unit: 'pt', lineHeight: 1.5, orientation: 'p'});
    var res = doc.autoTableHtmlToJson(tabla);
    doc.autoTable(res.columns, res.data, {startY: 60});
    var string = doc.output('datauristring');
    var x = window.open();
    x.document.open();
    x.document.location=string;
}

function genpdfVertical(tabla){
    var doc = new jsPDF({unit: 'pt', lineHeight: 1.5, orientation: 'l'});
    var res = doc.autoTableHtmlToJson(tabla);
    doc.autoTable(res.columns, res.data, {startY: 60});
    var string = doc.output('datauristring');
    var x = window.open();
    x.document.open();
    x.document.location=string;
}



