CREATE TABLE IF NOT EXISTS `PERSONA` (
  `nick` VARCHAR(50) NOT NULL,
  `contra` VARCHAR(50) NOT NULL,
  `nombre` VARCHAR(50) DEFAULT NULL,
  `apellido1` VARCHAR(50) DEFAULT NULL,
  `apellido2` VARCHAR(50) DEFAULT NULL,
CONSTRAINT PK_PERSONA PRIMARY KEY (`nick`)
)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `ANIMAL` (
  `id_animal` INT AUTO_INCREMENT NOT NULL,
  `id_persona` VARCHAR(50) NOT NULL,
  `nombre` VARCHAR(50) NULL DEFAULT NULL,
  `clase` VARCHAR(50) NULL DEFAULT NULL,
  `perfil` TINYINT(1) DEFAULT 0,
  CONSTRAINT PK_ANIMAL PRIMARY KEY (`id_animal`,`id_persona`),
  CONSTRAINT FK_ANIMAL_PERSONA FOREIGN KEY (`id_persona`) REFERENCES PERSONA (`nick`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT FK_ANIMAL_CLASE FOREIGN KEY (`clase`) REFERENCES clase (`tipo`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX index_FK_ANIMAL_PERSONA (`id_persona`),
  INDEX index_FK_ANIMAL_CLASE (`clase`)

)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `SEGUIDORES` (
  `id_animal_user` INT NOT NULL,
  `id_persona_user` VARCHAR(50) NOT NULL,
  `id_animal_contacto` INT NOT NULL,
  `id_persona_contacto` VARCHAR(50) NOT NULL,
  `bloqueo` TINYINT(1) DEFAULT 0,
  CONSTRAINT PK_SEGUIDORES PRIMARY KEY (`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT FK_SEGUIDORES_ANIMAL_USER FOREIGN KEY (`id_animal_user`,`id_persona_user`) REFERENCES ANIMAL (`id_animal`,`id_persona`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT FK_SEGUIDORES_ANIMAL_CONTACTO FOREIGN KEY (`id_animal_contacto`,`id_persona_contacto`) REFERENCES ANIMAL (`id_animal`,`id_persona`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX index_FK_SEGUIDORES_ANIMAL_USER (`id_animal_user`,`id_persona_user`),
  INDEX index_FK_SEGUIDORES_ANIMAL_CONTACTO (`id_animal_contacto`,`id_persona_contacto`)

)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `MENSAJES` (
  `fecha_mensaje` timestamp  DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` INT NOT NULL,
  `id_persona_user` VARCHAR(50) NOT NULL,
  `id_animal_contacto` INT NOT NULL,
  `id_persona_contacto` VARCHAR(50) NOT NULL,
  `mensaje` VARCHAR(500),
  `visto` TINYINT(1) DEFAULT 0,
  CONSTRAINT PK_MENSAJES PRIMARY KEY (`fecha_mensaje`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT FK_MENSAJES_SEGUIDORES FOREIGN KEY (`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`) REFERENCES SEGUIDORES (`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX index_FK_MENSAJES_SEGUIDORES (`id_animal_user`,`id_persona_user`)

)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `ACTIVIDAD` (
  `fecha_act` timestamp  DEFAULT CURRENT_TIMESTAMP NOT NULL,
  `id_animal_user` INT NOT NULL,
  `id_persona_user` VARCHAR(50) NOT NULL,
  `mensaje` VARCHAR(500),
  `veces_editado` INT DEFAULT 0,
  `fecha_ult_edicion` datetime DEFAULT null,
  CONSTRAINT PK_ACTIVIDAD PRIMARY KEY (`fecha_act`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT FK_ACTIVIDAD_ANIMAL FOREIGN KEY (`id_animal_user`,`id_persona_user`) REFERENCES ANIMAL (`id_animal`,`id_persona`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX index_FK_ACTIVIDAD_ANIMAL (`id_animal_user`,`id_persona_user`)

)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `ME_GUSTA_ACTIVIDAD` (
  `fecha_act` timestamp NOT NULL,
  `id_animal_user` INT NOT NULL,
  `id_persona_user` VARCHAR(50) NOT NULL,
  `id_animal_contacto` INT NOT NULL,
  `id_persona_contacto` VARCHAR(50) NOT NULL,
  `me_gusta` TINYINT(1) DEFAULT 1,
  CONSTRAINT PK_ME_GUSTA_ACTIVIDAD PRIMARY KEY (`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT FK_ME_GUSTA_ACTIVIDAD_ANIMAL FOREIGN KEY (`id_animal_contacto`,`id_persona_contacto`) REFERENCES ANIMAL (`id_animal`,`id_persona`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT FK_ME_GUSTA_ACTIVIDAD_ACTIVIDAD FOREIGN KEY (`fecha_act`,`id_animal_user`,`id_persona_user`) REFERENCES ACTIVIDAD (`fecha_act`,`id_animal_user`,`id_persona_user`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX index_ME_GUSTA_ACTIVIDAD_ANIMAL (`id_animal_contacto`,`id_persona_contacto`),
  INDEX index_ME_GUSTA_ACTIVIDAD_ACTIVIDAD (`fecha_act`,`id_animal_user`,`id_persona_user`)

)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `COMENTARIO_ACTIVIDAD` (
  `fecha_comen` timestamp  DEFAULT CURRENT_TIMESTAMP NOT NULL,
  `fecha_act` timestamp NOT NULL,
  `id_animal_user` INT NOT NULL,
  `id_persona_user` VARCHAR(50) NOT NULL,
  `id_animal_contacto` INT NOT NULL,
  `id_persona_contacto` VARCHAR(50) NOT NULL,
  `mensaje` VARCHAR(500),
  `veces_editado` INT DEFAULT 0,
  `fecha_ult_edicion` datetime DEFAULT null,
  CONSTRAINT PK_COMENTARIO_ACTIVIDAD PRIMARY KEY (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`, `id_animal_contacto`, `id_persona_contacto`),
  CONSTRAINT FK_COMENTARIO_ACTIVIDAD_ANIMAL FOREIGN KEY (`id_animal_contacto`,`id_persona_contacto`) REFERENCES ANIMAL (`id_animal`,`id_persona`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT FK_COMENTARIO_ACTIVIDAD_ACTIVIDAD FOREIGN KEY (`fecha_act`,`id_animal_user`,`id_persona_user`) REFERENCES ACTIVIDAD (`fecha_act`,`id_animal_user`,`id_persona_user`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,

  INDEX index_FK_ACTIVIDAD_ANIMAL (`id_animal_contacto`,`id_persona_contacto`),
  INDEX index_FK_ACTIVIDAD_ACTIVIDAD (`fecha_act`,`id_animal_user`,`id_persona_user`)

)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `ME_GUSTA_COMENTARIO_ACTIVIDAD` (
  `fecha_comen` timestamp  DEFAULT CURRENT_TIMESTAMP NOT NULL,
  `fecha_act` timestamp NOT NULL,
  `id_animal_user` INT NOT NULL,
  `id_persona_user` VARCHAR(50) NOT NULL,
  `id_animal_contacto` INT NOT NULL,
  `id_persona_contacto` VARCHAR(50) NOT NULL,
  `id_animal_ledamegusta` INT NOT NULL,
  `id_persona_ledamegusta` VARCHAR(50) NOT NULL,
  `me_gusta` TINYINT(1) DEFAULT 1,
  CONSTRAINT PK_ME_GUSTA_COMENTARIO_ACTIVIDAD PRIMARY KEY (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`, `id_animal_contacto`, `id_persona_contacto`,`id_animal_ledamegusta`,`id_persona_ledamegusta` ),
  CONSTRAINT FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_COMENTARIO_ACTIVIDAD FOREIGN KEY (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`, `id_animal_contacto`, `id_persona_contacto`) REFERENCES COMENTARIO_ACTIVIDAD (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`, `id_animal_contacto`, `id_persona_contacto`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL FOREIGN KEY (`id_animal_ledamegusta`,`id_persona_ledamegusta`) REFERENCES ANIMAL (`id_animal`,`id_persona`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,

  INDEX index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_COMENTARIO_ACTIVIDAD (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`, `id_animal_contacto`, `id_persona_contacto`),
  INDEX index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL (`id_animal_ledamegusta`,`id_persona_ledamegusta`)

)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `TIPOS_LOGRO` (
  `id_logro` INT AUTO_INCREMENT,
  `descripcion_logro` VARCHAR (200) NOT NULL,
  CONSTRAINT PK_TIPOS_LOGRO PRIMARY KEY (`id_logro`)
)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `LOGROS` (
  `fecha_logro` timestamp  DEFAULT CURRENT_TIMESTAMP NOT NULL,
  `id_animal_user` INT NOT NULL,
  `id_persona_user` VARCHAR(50) NOT NULL,
  `id_logro` INT NOT NULL,
  `visto` TINYINT(1) DEFAULT 0,
  CONSTRAINT PK_LOGROS PRIMARY KEY (`fecha_logro`,`id_animal_user`,`id_persona_user`,`id_logro`),
  CONSTRAINT FK_LOGROS_TIPOS_LOGRO FOREIGN KEY (`id_logro`) REFERENCES TIPOS_LOGRO (`id_logro`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  INDEX index_FK_LOGROS_TIPOS_LOGRO (`id_logro`)
)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `COMBATE` (
  `fecha_combate` timestamp  DEFAULT CURRENT_TIMESTAMP NOT NULL,
  `id_animal_user` INT NOT NULL,
  `id_persona_user` VARCHAR(50) NOT NULL,
  `id_animal_contacto` INT NOT NULL,
  `id_persona_contacto` VARCHAR(50) NOT NULL,
  `visto_user` TINYINT(1) DEFAULT 0,
  `visto_contacto` TINYINT(1) DEFAULT 0,
  CONSTRAINT PK_COMBATE PRIMARY KEY (`fecha_combate`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),

  CONSTRAINT FK_COMBATE_ANIMAL_USER FOREIGN KEY (`id_animal_user`,`id_persona_user` ) REFERENCES ANIMAL (`id_animal`,`id_persona`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  CONSTRAINT FK_COMBATE_ANIMAL_CONTACTO FOREIGN KEY (`id_animal_contacto`,`id_persona_contacto`) REFERENCES ANIMAL (`id_animal`,`id_persona`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,

  INDEX index_FK_COMBATE_ANIMAL_USER (`id_animal_user`,`id_persona_user` ),
  INDEX index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL (`id_animal_contacto`,`id_persona_contacto`)

)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `FOTOS` (
  `fecha_foto` timestamp  DEFAULT CURRENT_TIMESTAMP NOT NULL,
  `id_animal_user` INT NOT NULL,
  `id_persona_user` VARCHAR(50) NOT NULL,
  `nombre_foto` VARCHAR(50),
  `comentario_foto` VARCHAR(200),
  `lugar` VARCHAR(100),
  CONSTRAINT PK_FOTOS PRIMARY KEY (`fecha_foto`,`id_animal_user`,`id_persona_user`),

  CONSTRAINT FK_FOTOS_ANIMAL FOREIGN KEY (`id_animal_user`,`id_persona_user` ) REFERENCES ANIMAL (`id_animal`,`id_persona`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,

  INDEX index_FK_FOTOS_ANIMAL (`id_animal_user`,`id_persona_user`)

)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `COMENTARIOS_FOTOS` (
  `fecha_comentario` timestamp  DEFAULT CURRENT_TIMESTAMP NOT NULL,
  `id_animal_contacto` INT NOT NULL,
  `id_persona_contacto` VARCHAR(50) NOT NULL,
  `fecha_foto` timestamp  NOT NULL,
  `id_animal_user` INT NOT NULL,
  `id_persona_user` VARCHAR(50) NOT NULL,
  `comentario_contacto` VARCHAR(500),
  `visto_user` TINYINT(1) DEFAULT 0,
  `veces_editado` INT DEFAULT 0,
  `fecha_ult_edicion` datetime DEFAULT null,


  CONSTRAINT PK_COMENTARIOS_FOTOS PRIMARY KEY (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`),

  CONSTRAINT FK_COMENTARIOS_FOTOS_ANIMAL FOREIGN KEY (`id_animal_contacto`,`id_persona_contacto`) REFERENCES ANIMAL (`id_animal`,`id_persona`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,

  INDEX index_FK_COMENTARIOS_FOTOS_ANIMAL (`id_animal_contacto`,`id_persona_contacto`)

)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `ME_GUSTA_COMENTARIOS_FOTOS` (
  `fecha_comentario` timestamp NOT NULL,
  `id_animal_contacto` INT NOT NULL,
  `id_persona_contacto` VARCHAR(50) NOT NULL,
  `fecha_foto` timestamp  NOT NULL,
  `id_animal_user` INT NOT NULL,
  `id_persona_user` VARCHAR(50) NOT NULL,
  `id_animal_ledamegusta` INT NOT NULL,
  `id_persona_ledamegusta` VARCHAR(50) NOT NULL,
  `me_gusta` TINYINT(1) DEFAULT 1,

  CONSTRAINT ME_GUSTA_COMENTARIOS_FOTOS PRIMARY KEY (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),

  CONSTRAINT FK_ME_GUSTA_COMENTARIOS_FOTOS_ANIMAL FOREIGN KEY (`id_animal_ledamegusta`,`id_persona_ledamegusta`) REFERENCES ANIMAL (`id_animal`,`id_persona`)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,

  INDEX index_FK_ME_GUSTA_COMENTARIOS_FOTOS_ANIMAL (`id_animal_ledamegusta`,`id_persona_ledamegusta`)

)
  COLLATE='latin1_spanish_ci'
  ENGINE=InnoDB;
