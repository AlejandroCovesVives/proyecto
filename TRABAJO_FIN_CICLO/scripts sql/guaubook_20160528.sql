-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.13-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para guaubook
CREATE DATABASE IF NOT EXISTS `guaubook` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci */;
USE `guaubook`;


-- Volcando estructura para tabla guaubook.actividad
CREATE TABLE IF NOT EXISTS `actividad` (
  `fecha_act` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`fecha_act`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_ACTIVIDAD_ANIMAL` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.actividad: ~16 rows (aproximadamente)
DELETE FROM `actividad`;
/*!40000 ALTER TABLE `actividad` DISABLE KEYS */;
INSERT INTO `actividad` (`fecha_act`, `id_animal_user`, `id_persona_user`, `mensaje`, `veces_editado`, `fecha_ult_edicion`) VALUES
	('2016-05-10 22:38:41', 33, 'paquito', 'juas juas', 0, NULL),
	('2016-05-23 21:27:20', 33, 'paquito', 'ahihahahahahahaahaha que es coñe como mola esto ', 0, NULL),
	('2016-05-23 21:37:10', 28, 'prueba', 'bah, esto es una castaña (cara enfadada)', 0, NULL),
	('2016-05-27 22:44:26', 29, 'prueba', 'nuevo comentario xD', 0, NULL),
	('2016-05-27 23:10:23', 33, 'paquito', 'a la tercera va la vencida xD', 0, NULL),
	('2016-05-27 23:23:00', 33, 'paquito', 'prueba de dooooom', 0, NULL),
	('2016-05-27 23:23:50', 33, 'paquito', 'prueba de verdad xDD', 0, NULL),
	('2016-05-27 23:26:37', 33, 'paquito', 'esto no sale mierda', 0, NULL),
	('2016-05-27 23:27:47', 33, 'paquito', 'funciona joder', 0, NULL),
	('2016-05-27 23:30:23', 33, 'paquito', 'que bien eh?', 0, NULL),
	('2016-05-27 23:31:02', 33, 'paquito', 'joder bien el puto append ya va bien', 0, NULL),
	('2016-05-27 23:32:11', 33, 'paquito', 'prueba de limpiar', 0, NULL),
	('2016-05-28 13:27:41', 33, 'paquito', 'coment pa probar', 0, NULL),
	('2016-05-28 19:06:20', 33, 'paquito', 'reset ', 0, NULL),
	('2016-05-28 19:14:02', 33, 'paquito', 'fgggg', 0, NULL),
	('2016-05-28 21:41:47', 33, 'paquito', 'xDDDD áéíóú', 0, NULL);
/*!40000 ALTER TABLE `actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.animal
CREATE TABLE IF NOT EXISTS `animal` (
  `id_animal` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `clase` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `perfil` tinyint(1) DEFAULT NULL,
  `nivel` int(11) NOT NULL DEFAULT '1',
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `experiencia` int(11) NOT NULL DEFAULT '0',
  `ruta_foto_perfil` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `ciudad` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `poblacion` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `seguidores` int(11) DEFAULT '0',
  `siguiendo` int(11) DEFAULT '0',
  `peticiones_amistad` int(11) DEFAULT '0',
  PRIMARY KEY (`id_animal`,`id_persona`),
  KEY `index_FK_ANIMAL_PERSONA` (`id_persona`),
  KEY `index_FK_ANIMAL_CLASE` (`clase`),
  CONSTRAINT `FK_ANIMAL_CLASE` FOREIGN KEY (`clase`) REFERENCES `clase` (`tipo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ANIMAL_PERSONA` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`nick`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.animal: ~8 rows (aproximadamente)
DELETE FROM `animal`;
/*!40000 ALTER TABLE `animal` DISABLE KEYS */;
INSERT INTO `animal` (`id_animal`, `id_persona`, `nombre`, `clase`, `perfil`, `nivel`, `fecha_creacion`, `experiencia`, `ruta_foto_perfil`, `ciudad`, `poblacion`, `seguidores`, `siguiendo`, `peticiones_amistad`) VALUES
	(28, 'prueba', 'Borrico', 'Elefante', 1, 1, '2016-04-30 14:29:57', 0, NULL, 'asdasd', 'asdasdsd', 5, 1, 1),
	(29, 'prueba', 'cxcxcxc', 'Dromedario', 0, 1, '2016-04-30 14:39:05', 0, NULL, 'xcxcx', 'xcxcxc', 1, 0, 0),
	(30, 'prueba', 'okkoko', 'Gato', 0, 1, '2016-04-30 15:31:16', 0, NULL, 'ijiji', 'jijij', 0, 0, 0),
	(31, 'prueba', 'prueba', 'Perro', 0, 1, '2016-05-02 21:08:22', 0, NULL, 'sadsad', 'asdsdsd', 0, 0, 0),
	(32, 'prueba', 'Pepito', 'Gato', 1, 1, '2016-05-02 21:14:54', 0, NULL, 'Alicante', 'Elche', 0, 0, 0),
	(33, 'paquito', 'chucho', 'Perro', 0, 1, '2016-05-07 18:20:14', 0, 'perfil.jpg', 'Alicante', 'Elche', 1, 6, 0),
	(42, 'paquito', 'adsdsd', 'Perro', 0, 1, '2016-05-17 22:55:42', 0, NULL, 'sdsdsd', 'sadsdsd', 0, 0, 0),
	(43, 'paquito', 'Privation', 'Perro', 0, 1, '2016-05-18 22:17:26', 0, 'perfil.jpg', 'asdsd', 'adasdsd', 2, 0, 0);
/*!40000 ALTER TABLE `animal` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.clase
CREATE TABLE IF NOT EXISTS `clase` (
  `tipo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.clase: ~8 rows (aproximadamente)
DELETE FROM `clase`;
/*!40000 ALTER TABLE `clase` DISABLE KEYS */;
INSERT INTO `clase` (`tipo`, `descripcion`) VALUES
	('Canario', NULL),
	('Dromedario', NULL),
	('Elefante', NULL),
	('Gato', NULL),
	('Otro', NULL),
	('Periquito', NULL),
	('Perro', NULL),
	('Tortuga', NULL);
/*!40000 ALTER TABLE `clase` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.combate
CREATE TABLE IF NOT EXISTS `combate` (
  `fecha_combate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `visto_user` tinyint(1) DEFAULT '0',
  `visto_contacto` tinyint(1) DEFAULT '0',
  `id_animal_vencedor` int(11) NOT NULL,
  `id_persona_vencedor` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`fecha_combate`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_COMBATE_ANIMAL_USER` (`id_animal_user`,`id_persona_user`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `FK_COMBATE_ANIMAL_VENCEDOR` (`id_animal_vencedor`,`id_persona_vencedor`),
  CONSTRAINT `FK_COMBATE_ANIMAL_CONTACTO` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_COMBATE_ANIMAL_USER` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_COMBATE_ANIMAL_VENCEDOR` FOREIGN KEY (`id_animal_vencedor`, `id_persona_vencedor`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.combate: ~2 rows (aproximadamente)
DELETE FROM `combate`;
/*!40000 ALTER TABLE `combate` DISABLE KEYS */;
INSERT INTO `combate` (`fecha_combate`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`, `visto_user`, `visto_contacto`, `id_animal_vencedor`, `id_persona_vencedor`) VALUES
	('2016-05-14 11:41:32', 33, 'paquito', 28, 'prueba', 0, 0, 33, 'paquito'),
	('2016-05-14 11:42:03', 29, 'prueba', 33, 'paquito', 0, 0, 33, 'paquito');
/*!40000 ALTER TABLE `combate` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.comentarios_fotos
CREATE TABLE IF NOT EXISTS `comentarios_fotos` (
  `fecha_comentario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_foto` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `comentario_contacto` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `visto_user` tinyint(1) DEFAULT '0',
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_COMENTARIOS_FOTOS_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_COMENTARIOS_FOTOS_FOTOS` (`fecha_foto`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_COMENTARIOS_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_COMENTARIOS_FOTOS_FOTOS` FOREIGN KEY (`fecha_foto`, `id_animal_user`, `id_persona_user`) REFERENCES `fotos` (`fecha_foto`, `id_animal_user`, `id_persona_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.comentarios_fotos: ~0 rows (aproximadamente)
DELETE FROM `comentarios_fotos`;
/*!40000 ALTER TABLE `comentarios_fotos` DISABLE KEYS */;
INSERT INTO `comentarios_fotos` (`fecha_comentario`, `id_animal_contacto`, `id_persona_contacto`, `fecha_foto`, `id_animal_user`, `id_persona_user`, `comentario_contacto`, `visto_user`, `veces_editado`, `fecha_ult_edicion`) VALUES
	('2016-05-13 22:50:47', 28, 'prueba', '2016-05-13 19:38:12', 33, 'paquito', 'to guapo chucho!', 0, 0, NULL);
/*!40000 ALTER TABLE `comentarios_fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.comentario_actividad
CREATE TABLE IF NOT EXISTS `comentario_actividad` (
  `fecha_comen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_act` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ACTIVIDAD_ACTIVIDAD` (`fecha_act`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_COMENTARIO_ACTIVIDAD_ACTIVIDAD` FOREIGN KEY (`fecha_act`, `id_animal_user`, `id_persona_user`) REFERENCES `actividad` (`fecha_act`, `id_animal_user`, `id_persona_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_COMENTARIO_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.comentario_actividad: ~44 rows (aproximadamente)
DELETE FROM `comentario_actividad`;
/*!40000 ALTER TABLE `comentario_actividad` DISABLE KEYS */;
INSERT INTO `comentario_actividad` (`fecha_comen`, `fecha_act`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`, `mensaje`, `veces_editado`, `fecha_ult_edicion`) VALUES
	('2016-05-14 10:35:48', '2016-05-10 22:38:41', 33, 'paquito', 28, 'prueba', 'to guapo shurperro', 1, '2016-05-25 22:37:15'),
	('2016-05-25 22:34:14', '2016-05-10 22:38:41', 33, 'paquito', 33, 'paquito', 'a que si? ahihahahaha', 0, NULL),
	('2016-05-28 12:42:32', '2016-05-27 23:32:11', 33, 'paquito', 33, 'paquito', 'hola?', 0, NULL),
	('2016-05-28 12:43:53', '2016-05-27 23:32:11', 33, 'paquito', 33, 'paquito', 'uff', 0, NULL),
	('2016-05-28 12:46:23', '2016-05-27 23:32:11', 33, 'paquito', 33, 'paquito', 'putos comentarios', 0, NULL),
	('2016-05-28 12:52:27', '2016-05-27 23:32:11', 33, 'paquito', 33, 'paquito', 'porque esto no tira :\'(', 0, NULL),
	('2016-05-28 12:58:36', '2016-05-27 23:32:11', 33, 'paquito', 33, 'paquito', 'funciona shur', 0, NULL),
	('2016-05-28 13:21:42', '2016-05-27 23:31:02', 33, 'paquito', 33, 'paquito', 'que va xD', 0, NULL),
	('2016-05-28 13:25:13', '2016-05-27 23:31:02', 33, 'paquito', 33, 'paquito', 'dasdasd', 0, NULL),
	('2016-05-28 13:27:45', '2016-05-28 13:27:41', 33, 'paquito', 33, 'paquito', 'hola', 0, NULL),
	('2016-05-28 13:29:26', '2016-05-28 13:27:41', 33, 'paquito', 33, 'paquito', 'ahia', 0, NULL),
	('2016-05-28 13:39:05', '2016-05-28 13:27:41', 33, 'paquito', 33, 'paquito', 'ezto no va', 0, NULL),
	('2016-05-28 13:40:58', '2016-05-28 13:27:41', 33, 'paquito', 33, 'paquito', 'nope', 0, NULL),
	('2016-05-28 13:41:58', '2016-05-23 21:27:20', 33, 'paquito', 33, 'paquito', 'sip', 0, NULL),
	('2016-05-28 13:42:22', '2016-05-23 21:27:20', 33, 'paquito', 33, 'paquito', 'dios si xD', 0, NULL),
	('2016-05-28 18:00:10', '2016-05-28 13:27:41', 33, 'paquito', 33, 'paquito', 'ahora si xD', 0, NULL),
	('2016-05-28 18:51:25', '2016-05-28 13:27:41', 33, 'paquito', 33, 'paquito', 'adasdsadÃ§', 0, NULL),
	('2016-05-28 18:51:34', '2016-05-28 13:27:41', 33, 'paquito', 33, 'paquito', 'Ã±Ã±Ã±', 0, NULL),
	('2016-05-28 19:06:24', '2016-05-28 19:06:20', 33, 'paquito', 33, 'paquito', 'asdasdasd', 0, NULL),
	('2016-05-28 19:07:00', '2016-05-28 19:06:20', 33, 'paquito', 33, 'paquito', 'zxzcxzc', 0, NULL),
	('2016-05-28 19:10:25', '2016-05-28 19:06:20', 33, 'paquito', 33, 'paquito', 'sddsdf', 0, NULL),
	('2016-05-28 19:13:39', '2016-05-28 19:06:20', 33, 'paquito', 33, 'paquito', 'cxvxv', 0, NULL),
	('2016-05-28 19:14:06', '2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 'vvvv', 0, NULL),
	('2016-05-28 19:16:03', '2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 'czxxzc', 0, NULL),
	('2016-05-28 19:19:14', '2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 'dgdgfdg', 0, NULL),
	('2016-05-28 19:19:49', '2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 'fhghgfhgh', 0, NULL),
	('2016-05-28 19:19:54', '2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 'Ã±klÃ±kÃ±lÃ±', 0, NULL),
	('2016-05-28 19:20:46', '2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 'dsfsdf', 0, NULL),
	('2016-05-28 19:21:30', '2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 'sfsdfdf', 0, NULL),
	('2016-05-28 19:26:21', '2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 'asdsd', 0, NULL),
	('2016-05-28 19:28:21', '2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 'sdfsdfdf', 0, NULL),
	('2016-05-28 19:28:28', '2016-05-28 19:06:20', 33, 'paquito', 33, 'paquito', 'xcvxcv', 0, NULL),
	('2016-05-28 19:41:32', '2016-05-28 19:06:20', 33, 'paquito', 33, 'paquito', 'sdsdfdf', 0, NULL),
	('2016-05-28 19:49:29', '2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 'werer', 0, NULL),
	('2016-05-28 19:49:38', '2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 'ññññ', 0, '2016-05-28 21:40:11'),
	('2016-05-28 21:42:08', '2016-05-28 21:41:47', 33, 'paquito', 33, 'paquito', 'fundions', 0, NULL),
	('2016-05-28 23:45:01', '2016-05-28 21:41:47', 33, 'paquito', 33, 'paquito', 'siuuuuuuuu', 0, NULL);
/*!40000 ALTER TABLE `comentario_actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.fotos
CREATE TABLE IF NOT EXISTS `fotos` (
  `fecha_foto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre_foto` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `comentario_foto` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `lugar` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`fecha_foto`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_FOTOS_ANIMAL` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.fotos: ~0 rows (aproximadamente)
DELETE FROM `fotos`;
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
INSERT INTO `fotos` (`fecha_foto`, `id_animal_user`, `id_persona_user`, `nombre_foto`, `comentario_foto`, `lugar`) VALUES
	('2016-05-13 19:38:12', 33, 'paquito', NULL, 'aqui sufriendo con el amo guau', 'Elche');
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.logros
CREATE TABLE IF NOT EXISTS `logros` (
  `fecha_logro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_logro` int(11) NOT NULL,
  `visto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`fecha_logro`,`id_animal_user`,`id_persona_user`,`id_logro`),
  KEY `index_FK_LOGROS_TIPOS_LOGRO` (`id_logro`),
  KEY `index_FK_LOGROS_ANIMAL` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_LOGROS_ANIMAL` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_LOGROS_TIPOS_LOGRO` FOREIGN KEY (`id_logro`) REFERENCES `tipos_logro` (`id_logro`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.logros: ~2 rows (aproximadamente)
DELETE FROM `logros`;
/*!40000 ALTER TABLE `logros` DISABLE KEYS */;
INSERT INTO `logros` (`fecha_logro`, `id_animal_user`, `id_persona_user`, `id_logro`, `visto`) VALUES
	('2016-05-13 19:01:39', 33, 'paquito', 6, 0),
	('2016-05-13 19:01:51', 33, 'paquito', 2, 0);
/*!40000 ALTER TABLE `logros` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.mensajes
CREATE TABLE IF NOT EXISTS `mensajes` (
  `fecha_mensaje` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `visto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`fecha_mensaje`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_MENSAJES_ANMAL_USER` (`id_animal_user`,`id_persona_user`),
  KEY `index_FK_MENSAJES_ANMAL_CONTACTO` (`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT `FK_MENSAJES_ANMAL_CONTACTO` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MENSAJES_ANMAL_USER` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.mensajes: ~2 rows (aproximadamente)
DELETE FROM `mensajes`;
/*!40000 ALTER TABLE `mensajes` DISABLE KEYS */;
INSERT INTO `mensajes` (`fecha_mensaje`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`, `mensaje`, `visto`) VALUES
	('2016-05-13 19:02:36', 33, 'paquito', 28, 'prueba', 'Hola borrico guau guau', 0),
	('2016-05-13 19:03:11', 28, 'prueba', 33, 'paquito', 'yei shurperro que pasa?', 0),
	('2016-05-13 19:03:31', 33, 'paquito', 31, 'prueba', 'ahiahahaha', 0);
/*!40000 ALTER TABLE `mensajes` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_actividad
CREATE TABLE IF NOT EXISTS `me_gusta_actividad` (
  `fecha_act` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_ME_GUSTA_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_ME_GUSTA_ACTIVIDAD_ACTIVIDAD` (`fecha_act`,`id_animal_user`,`id_persona_user`),
  KEY `FK_ME_GUSTA_ACTIVIDAD_ACTIVIDAD` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ME_GUSTA_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_me_gusta_actividad_actividad` FOREIGN KEY (`fecha_act`, `id_animal_user`, `id_persona_user`) REFERENCES `actividad` (`fecha_act`, `id_animal_user`, `id_persona_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_actividad: ~0 rows (aproximadamente)
DELETE FROM `me_gusta_actividad`;
/*!40000 ALTER TABLE `me_gusta_actividad` DISABLE KEYS */;
INSERT INTO `me_gusta_actividad` (`fecha_act`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`, `me_gusta`) VALUES
	('2016-05-27 23:26:37', 33, 'paquito', 33, 'paquito', 1),
	('2016-05-27 23:32:11', 33, 'paquito', 33, 'paquito', 1),
	('2016-05-28 19:14:02', 33, 'paquito', 33, 'paquito', 1),
	('2016-05-28 21:41:47', 33, 'paquito', 33, 'paquito', 1);
/*!40000 ALTER TABLE `me_gusta_actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_comentarios_fotos
CREATE TABLE IF NOT EXISTS `me_gusta_comentarios_fotos` (
  `fecha_comentario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_foto` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_ledamegusta` int(11) NOT NULL,
  `id_persona_ledamegusta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_COMENTARIOS_FOTOS_ANIMAL` (`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_COMENTARIOS_FOTOS_COMENTARIOS_FOTOS` (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ME_GUSTA_COMENTARIOS_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_ledamegusta`, `id_persona_ledamegusta`) REFERENCES `animal` (`id_animal`, `id_persona`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_COMENTARIOS_FOTOS_COMENTARIOS_FOTOS` FOREIGN KEY (`fecha_comentario`, `id_animal_contacto`, `id_persona_contacto`, `fecha_foto`, `id_animal_user`, `id_persona_user`) REFERENCES `comentarios_fotos` (`fecha_comentario`, `id_animal_contacto`, `id_persona_contacto`, `fecha_foto`, `id_animal_user`, `id_persona_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_comentarios_fotos: ~0 rows (aproximadamente)
DELETE FROM `me_gusta_comentarios_fotos`;
/*!40000 ALTER TABLE `me_gusta_comentarios_fotos` DISABLE KEYS */;
INSERT INTO `me_gusta_comentarios_fotos` (`fecha_comentario`, `id_animal_contacto`, `id_persona_contacto`, `fecha_foto`, `id_animal_user`, `id_persona_user`, `id_animal_ledamegusta`, `id_persona_ledamegusta`, `me_gusta`) VALUES
	('2016-05-13 22:50:47', 28, 'prueba', '2016-05-13 19:38:12', 33, 'paquito', 29, 'prueba', 1);
/*!40000 ALTER TABLE `me_gusta_comentarios_fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_comentario_actividad
CREATE TABLE IF NOT EXISTS `me_gusta_comentario_actividad` (
  `fecha_comen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_act` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_ledamegusta` int(11) NOT NULL,
  `id_persona_ledamegusta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_COMENTARIO_ACTIVIDAD` (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` (`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  CONSTRAINT `FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_ledamegusta`, `id_persona_ledamegusta`) REFERENCES `animal` (`id_animal`, `id_persona`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_COMENTARIO_ACTIVIDAD` FOREIGN KEY (`fecha_comen`, `fecha_act`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`) REFERENCES `comentario_actividad` (`fecha_comen`, `fecha_act`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_comentario_actividad: ~0 rows (aproximadamente)
DELETE FROM `me_gusta_comentario_actividad`;
/*!40000 ALTER TABLE `me_gusta_comentario_actividad` DISABLE KEYS */;
INSERT INTO `me_gusta_comentario_actividad` (`fecha_comen`, `fecha_act`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`, `id_animal_ledamegusta`, `id_persona_ledamegusta`, `me_gusta`) VALUES
	('2016-05-14 10:35:48', '2016-05-10 22:38:41', 33, 'paquito', 28, 'prueba', 29, 'prueba', 1),
	('2016-05-28 21:42:08', '2016-05-28 21:41:47', 33, 'paquito', 33, 'paquito', 33, 'paquito', 1);
/*!40000 ALTER TABLE `me_gusta_comentario_actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_fotos
CREATE TABLE IF NOT EXISTS `me_gusta_fotos` (
  `fecha_foto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_ledamegusta` int(11) NOT NULL,
  `id_persona_ledamegusta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_foto`,`id_animal_user`,`id_persona_user`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_FOTOS_ANIMAL` (`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_FOTOS_FOTOS` (`fecha_foto`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ME_GUSTA_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_ledamegusta`, `id_persona_ledamegusta`) REFERENCES `animal` (`id_animal`, `id_persona`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_FOTOS_FOTOS` FOREIGN KEY (`fecha_foto`, `id_animal_user`, `id_persona_user`) REFERENCES `fotos` (`fecha_foto`, `id_animal_user`, `id_persona_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_fotos: ~0 rows (aproximadamente)
DELETE FROM `me_gusta_fotos`;
/*!40000 ALTER TABLE `me_gusta_fotos` DISABLE KEYS */;
INSERT INTO `me_gusta_fotos` (`fecha_foto`, `id_animal_user`, `id_persona_user`, `id_animal_ledamegusta`, `id_persona_ledamegusta`, `me_gusta`) VALUES
	('2016-05-13 19:38:12', 33, 'paquito', 28, 'prueba', 1);
/*!40000 ALTER TABLE `me_gusta_fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.persona
CREATE TABLE IF NOT EXISTS `persona` (
  `nick` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `contra` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellido1` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellido2` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.persona: ~6 rows (aproximadamente)
DELETE FROM `persona`;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` (`nick`, `contra`, `nombre`, `apellido1`, `apellido2`, `email`) VALUES
	('adasd', 'asdasdsad', 'asdsad', 'asdsad', 'asdasd', 'asdasdsad'),
	('nick', 'asdfgh', 'asdasd', 'dadasd', 'dadasd', 'dadsad@dasdasdsad.com'),
	('paquito', 'sasuke', 'alejandro', 'coves', 'vives', 'vivex@hotmail.com'),
	('pequezo', '123456', 'dfdf', 'dfdf', 'dfdfd', 'viv_ex@hotmail.com'),
	('prueba', '1234', 'Manolo', 'Perez', 'Ahia', 'vivexxx@gmail.com'),
	('prueba2', '123456', 'sdadsd', 'sadasd', 'dasdsad', 'asdasdsad');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.seguidores
CREATE TABLE IF NOT EXISTS `seguidores` (
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `bloqueo_user` tinyint(1) DEFAULT '0',
  `bloqueo_contacto` tinyint(1) DEFAULT '0',
  `aceptado` tinyint(1) DEFAULT '1',
  `visto_por_contacto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_SEGUIDORES_ANIMAL_USER` (`id_animal_user`,`id_persona_user`),
  KEY `index_FK_SEGUIDORES_ANIMAL_CONTACTO` (`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT `FK_SEGUIDORES_ANIMAL_CONTACTO` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_SEGUIDORES_ANIMAL_USER` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.seguidores: ~3 rows (aproximadamente)
DELETE FROM `seguidores`;
/*!40000 ALTER TABLE `seguidores` DISABLE KEYS */;
INSERT INTO `seguidores` (`id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`, `bloqueo_user`, `bloqueo_contacto`, `aceptado`, `visto_por_contacto`) VALUES
	(28, 'prueba', 33, 'paquito', 0, 0, 1, 0),
	(33, 'paquito', 28, 'prueba', 0, 0, 1, 0),
	(33, 'paquito', 29, 'prueba', 0, 0, 1, 0),
	(33, 'paquito', 31, 'prueba', 0, 0, 1, 0),
	(33, 'paquito', 43, 'paquito', 0, 0, 1, 0);
/*!40000 ALTER TABLE `seguidores` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.tipos_logro
CREATE TABLE IF NOT EXISTS `tipos_logro` (
  `id_logro` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_logro` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_logro`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.tipos_logro: ~11 rows (aproximadamente)
DELETE FROM `tipos_logro`;
/*!40000 ALTER TABLE `tipos_logro` DISABLE KEYS */;
INSERT INTO `tipos_logro` (`id_logro`, `descripcion_logro`) VALUES
	(1, 'El más querido'),
	(2, 'El más guatástico'),
	(3, 'El más curioso'),
	(4, 'El más amistoso'),
	(5, 'El más activo'),
	(6, 'El más fotogénico'),
	(7, 'El más ladrador'),
	(8, 'El más guauChuck guauNorris'),
	(9, 'El más jugueton'),
	(10, 'El más vago'),
	(11, 'Admirador de goofy');
/*!40000 ALTER TABLE `tipos_logro` ENABLE KEYS */;


-- Volcando estructura para disparador guaubook.bins_seguidores
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `bins_seguidores` BEFORE INSERT ON `seguidores` FOR EACH ROW BEGIN
  
	DECLARE profile_followed integer;
	
	SELECT perfil from guaubook.animal where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto into profile_followed;
	
	if(profile_followed = 1) THEN
		update guaubook.animal set peticiones_amistad = peticiones_amistad +1 where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto;
		set new.aceptado = 0;
		
	end if;
	
	if(profile_followed = 0) THEN
		update guaubook.animal set seguidores = seguidores +1 where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto;
		update guaubook.animal set siguiendo = siguiendo +1 where id_animal = new.id_animal_user and id_persona = new.id_persona_user;
	end if;
	
  END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Volcando estructura para disparador guaubook.bup_seguidores
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `bup_seguidores` BEFORE UPDATE ON `seguidores` FOR EACH ROW BEGIN
  
	if(old.aceptado = 0 AND new.aceptado = 1) THEN
		update guaubook.animal set seguidores = seguidores +1, peticiones_amistad =  peticiones_amistad -1 where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto;
		update guaubook.animal set siguiendo = siguiendo +1 where id_animal = new.id_animal_user and id_persona = new.id_persona_user;
		
	end if;
	
  END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
