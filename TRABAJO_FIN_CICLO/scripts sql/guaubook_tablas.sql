-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.32 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para guaubook
CREATE DATABASE IF NOT EXISTS `guaubook` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci */;
USE `guaubook`;


-- Volcando estructura para tabla guaubook.actividad
CREATE TABLE IF NOT EXISTS `actividad` (
  `fecha_act` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`fecha_act`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_ACTIVIDAD_ANIMAL` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.animal
CREATE TABLE IF NOT EXISTS `animal` (
  `id_animal` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `clase` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `perfil` tinyint(1) DEFAULT '0',
  `nivel` int(11) NOT NULL DEFAULT '1',
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `experiencia` int(11) NOT NULL DEFAULT '0',
  `ruta_foto_perfil` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `ciudad` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `poblacion` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_animal`,`id_persona`),
  KEY `index_FK_ANIMAL_PERSONA` (`id_persona`),
  KEY `index_FK_ANIMAL_CLASE` (`clase`),
  CONSTRAINT `FK_ANIMAL_CLASE` FOREIGN KEY (`clase`) REFERENCES `clase` (`tipo`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ANIMAL_PERSONA` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`nick`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.clase
CREATE TABLE IF NOT EXISTS `clase` (
  `tipo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.combate
CREATE TABLE IF NOT EXISTS `combate` (
  `fecha_combate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `visto_user` tinyint(1) DEFAULT '0',
  `visto_contacto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`fecha_combate`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_COMBATE_ANIMAL_USER` (`id_animal_user`,`id_persona_user`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT `FK_COMBATE_ANIMAL_USER` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_COMBATE_ANIMAL_CONTACTO` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.comentarios_fotos
CREATE TABLE IF NOT EXISTS `comentarios_fotos` (
  `fecha_comentario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_foto` timestamp NOT NULL,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `comentario_contacto` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `visto_user` tinyint(1) DEFAULT '0',
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_COMENTARIOS_FOTOS_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT `FK_COMENTARIOS_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.comentario_actividad
CREATE TABLE IF NOT EXISTS `comentario_actividad` (
  `fecha_comen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_act` timestamp NOT NULL,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ACTIVIDAD_ACTIVIDAD` (`fecha_act`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_COMENTARIO_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_COMENTARIO_ACTIVIDAD_ACTIVIDAD` FOREIGN KEY (`fecha_act`, `id_animal_user`, `id_persona_user`) REFERENCES `actividad` (`fecha_act`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.fotos
CREATE TABLE IF NOT EXISTS `fotos` (
  `fecha_foto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre_foto` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `comentario_foto` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `lugar` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`fecha_foto`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_FOTOS_ANIMAL` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.logros
CREATE TABLE IF NOT EXISTS `logros` (
  `fecha_logro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_logro` int(11) NOT NULL,
  `visto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`fecha_logro`,`id_animal_user`,`id_persona_user`,`id_logro`),
  KEY `index_FK_LOGROS_TIPOS_LOGRO` (`id_logro`),
  CONSTRAINT `FK_LOGROS_TIPOS_LOGRO` FOREIGN KEY (`id_logro`) REFERENCES `tipos_logro` (`id_logro`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.mensajes
CREATE TABLE IF NOT EXISTS `mensajes` (
  `fecha_mensaje` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `visto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`fecha_mensaje`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `FK_MENSAJES_SEGUIDORES` (`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_MENSAJES_SEGUIDORES` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_MENSAJES_SEGUIDORES` FOREIGN KEY (`id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`) REFERENCES `seguidores` (`id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.me_gusta_actividad
CREATE TABLE IF NOT EXISTS `me_gusta_actividad` (
  `fecha_act` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_ME_GUSTA_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_ME_GUSTA_ACTIVIDAD_ACTIVIDAD` (`fecha_act`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ME_GUSTA_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_ACTIVIDAD_ACTIVIDAD` FOREIGN KEY (`fecha_act`, `id_animal_user`, `id_persona_user`) REFERENCES `actividad` (`fecha_act`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.me_gusta_comentarios_fotos
CREATE TABLE IF NOT EXISTS `me_gusta_comentarios_fotos` (
  `fecha_comentario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_foto` timestamp NOT NULL,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_ledamegusta` int(11) NOT NULL,
  `id_persona_ledamegusta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_COMENTARIOS_FOTOS_ANIMAL` (`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  CONSTRAINT `FK_ME_GUSTA_COMENTARIOS_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_ledamegusta`, `id_persona_ledamegusta`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.me_gusta_comentario_actividad
CREATE TABLE IF NOT EXISTS `me_gusta_comentario_actividad` (
  `fecha_comen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_act` timestamp NOT NULL,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_ledamegusta` int(11) NOT NULL,
  `id_persona_ledamegusta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_COMENTARIO_ACTIVIDAD` (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` (`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  CONSTRAINT `FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_COMENTARIO_ACTIVIDAD` FOREIGN KEY (`fecha_comen`, `fecha_act`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`) REFERENCES `comentario_actividad` (`fecha_comen`, `fecha_act`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_ledamegusta`, `id_persona_ledamegusta`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.persona
CREATE TABLE IF NOT EXISTS `persona` (
  `nick` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `contra` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellido1` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellido2` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.seguidores
CREATE TABLE IF NOT EXISTS `seguidores` (
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `bloqueo` tinyint(1) DEFAULT '0',
  `visto_por_contacto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_SEGUIDORES_ANIMAL_USER` (`id_animal_user`,`id_persona_user`),
  KEY `index_FK_SEGUIDORES_ANIMAL_CONTACTO` (`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT `FK_SEGUIDORES_ANIMAL_CONTACTO` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_SEGUIDORES_ANIMAL_USER` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.


-- Volcando estructura para tabla guaubook.tipos_logro
CREATE TABLE IF NOT EXISTS `tipos_logro` (
  `id_logro` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_logro` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_logro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
