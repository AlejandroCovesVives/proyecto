-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.32 - MySQL Community Server (GPL)
-- SO del servidor:              Win32
-- HeidiSQL Versión:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para guaubook
CREATE DATABASE IF NOT EXISTS `guaubook` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci */;
USE `guaubook`;


-- Volcando estructura para tabla guaubook.actividad
CREATE TABLE IF NOT EXISTS `actividad` (
  `fecha_act` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`fecha_act`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_ACTIVIDAD_ANIMAL` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.actividad: ~0 rows (aproximadamente)
DELETE FROM `actividad`;
/*!40000 ALTER TABLE `actividad` DISABLE KEYS */;
/*!40000 ALTER TABLE `actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.animal
CREATE TABLE IF NOT EXISTS `animal` (
  `id_animal` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `clase` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `perfil` tinyint(1) DEFAULT NULL,
  `nivel` int(11) NOT NULL DEFAULT '1',
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `experiencia` int(11) NOT NULL DEFAULT '0',
  `ruta_foto_perfil` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `ciudad` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `poblacion` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `seguidores` int(11) DEFAULT '0',
  `siguiendo` int(11) DEFAULT '0',
  `peticiones_amistad` int(11) DEFAULT '0',
  PRIMARY KEY (`id_animal`,`id_persona`),
  KEY `index_FK_ANIMAL_PERSONA` (`id_persona`),
  KEY `index_FK_ANIMAL_CLASE` (`clase`),
  CONSTRAINT `FK_ANIMAL_CLASE` FOREIGN KEY (`clase`) REFERENCES `clase` (`tipo`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT `FK_ANIMAL_PERSONA` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`nick`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.animal: ~6 rows (aproximadamente)
DELETE FROM `animal`;
/*!40000 ALTER TABLE `animal` DISABLE KEYS */;
INSERT INTO `animal` (`id_animal`, `id_persona`, `nombre`, `clase`, `perfil`, `nivel`, `fecha_creacion`, `experiencia`, `ruta_foto_perfil`, `ciudad`, `poblacion`, `seguidores`, `siguiendo`, `peticiones_amistad`) VALUES
	(28, 'prueba', 'Borrico', 'Elefante', 1, 1, '2016-04-30 14:29:57', 0, NULL, 'asdasd', 'asdasdsd', 5, 0, 1),
	(29, 'prueba', 'cxcxcxc', 'Dromedario', 0, 1, '2016-04-30 14:39:05', 0, NULL, 'xcxcx', 'xcxcxc', 1, 0, 0),
	(30, 'prueba', 'okkoko', 'Gato', 0, 1, '2016-04-30 15:31:16', 0, NULL, 'ijiji', 'jijij', 0, 0, 0),
	(31, 'prueba', 'prueba', 'Perro', 0, 1, '2016-05-02 21:08:22', 0, NULL, 'sadsad', 'asdsdsd', 0, 0, 0),
	(32, 'prueba', 'Pepito', 'Gato', 1, 1, '2016-05-02 21:14:54', 0, NULL, 'Alicante', 'Elche', 0, 0, 0),
	(33, 'paquito', 'chucho', 'Perro', 0, 1, '2016-05-07 18:20:14', 0, NULL, 'Alicante', 'Elche', 0, 2, 0);
/*!40000 ALTER TABLE `animal` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.clase
CREATE TABLE IF NOT EXISTS `clase` (
  `tipo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.clase: ~8 rows (aproximadamente)
DELETE FROM `clase`;
/*!40000 ALTER TABLE `clase` DISABLE KEYS */;
INSERT INTO `clase` (`tipo`, `descripcion`) VALUES
	('Canario', NULL),
	('Dromedario', NULL),
	('Elefante', NULL),
	('Gato', NULL),
	('Otro', NULL),
	('Periquito', NULL),
	('Perro', NULL),
	('Tortuga', NULL);
/*!40000 ALTER TABLE `clase` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.combate
CREATE TABLE IF NOT EXISTS `combate` (
  `fecha_combate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `visto_user` tinyint(1) DEFAULT '0',
  `visto_contacto` tinyint(1) DEFAULT '0',
  `id_animal_vencedor` int(11) NOT NULL,
  `id_persona_vencedor` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`fecha_combate`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_COMBATE_ANIMAL_USER` (`id_animal_user`,`id_persona_user`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `FK_COMBATE_ANIMAL_VENCEDOR` (`id_animal_vencedor`,`id_persona_vencedor`),
  CONSTRAINT `FK_COMBATE_ANIMAL_CONTACTO` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_COMBATE_ANIMAL_USER` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_COMBATE_ANIMAL_VENCEDOR` FOREIGN KEY (`id_animal_vencedor`, `id_persona_vencedor`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.combate: ~0 rows (aproximadamente)
DELETE FROM `combate`;
/*!40000 ALTER TABLE `combate` DISABLE KEYS */;
/*!40000 ALTER TABLE `combate` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.comentarios_fotos
CREATE TABLE IF NOT EXISTS `comentarios_fotos` (
  `fecha_comentario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_foto` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `comentario_contacto` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `visto_user` tinyint(1) DEFAULT '0',
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_COMENTARIOS_FOTOS_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_COMENTARIOS_FOTOS_FOTOS` (`fecha_foto`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_COMENTARIOS_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT `FK_COMENTARIOS_FOTOS_FOTOS` FOREIGN KEY (`fecha_foto`, `id_animal_user`, `id_persona_user`) REFERENCES `fotos` (`fecha_foto`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.comentarios_fotos: ~0 rows (aproximadamente)
DELETE FROM `comentarios_fotos`;
/*!40000 ALTER TABLE `comentarios_fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentarios_fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.comentario_actividad
CREATE TABLE IF NOT EXISTS `comentario_actividad` (
  `fecha_comen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_act` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ACTIVIDAD_ACTIVIDAD` (`fecha_act`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_COMENTARIO_ACTIVIDAD_ACTIVIDAD` FOREIGN KEY (`fecha_act`, `id_animal_user`, `id_persona_user`) REFERENCES `actividad` (`fecha_act`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT `FK_COMENTARIO_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.comentario_actividad: ~0 rows (aproximadamente)
DELETE FROM `comentario_actividad`;
/*!40000 ALTER TABLE `comentario_actividad` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentario_actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.fotos
CREATE TABLE IF NOT EXISTS `fotos` (
  `fecha_foto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre_foto` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `comentario_foto` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `lugar` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`fecha_foto`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_FOTOS_ANIMAL` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.fotos: ~0 rows (aproximadamente)
DELETE FROM `fotos`;
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.logros
CREATE TABLE IF NOT EXISTS `logros` (
  `fecha_logro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_logro` int(11) NOT NULL,
  `visto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`fecha_logro`,`id_animal_user`,`id_persona_user`,`id_logro`),
  KEY `index_FK_LOGROS_TIPOS_LOGRO` (`id_logro`),
  KEY `index_FK_LOGROS_ANIMAL` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_LOGROS_ANIMAL` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_LOGROS_TIPOS_LOGRO` FOREIGN KEY (`id_logro`) REFERENCES `tipos_logro` (`id_logro`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.logros: ~0 rows (aproximadamente)
DELETE FROM `logros`;
/*!40000 ALTER TABLE `logros` DISABLE KEYS */;
/*!40000 ALTER TABLE `logros` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.mensajes
CREATE TABLE IF NOT EXISTS `mensajes` (
  `fecha_mensaje` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `visto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`fecha_mensaje`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_MENSAJES_ANMAL_USER` (`id_animal_user`,`id_persona_user`),
  KEY `index_FK_MENSAJES_ANMAL_CONTACTO` (`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT `FK_MENSAJES_ANMAL_CONTACTO` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT `FK_MENSAJES_ANMAL_USER` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.mensajes: ~0 rows (aproximadamente)
DELETE FROM `mensajes`;
/*!40000 ALTER TABLE `mensajes` DISABLE KEYS */;
/*!40000 ALTER TABLE `mensajes` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_actividad
CREATE TABLE IF NOT EXISTS `me_gusta_actividad` (
  `fecha_act` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_ME_GUSTA_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_ME_GUSTA_ACTIVIDAD_ACTIVIDAD` (`fecha_act`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ME_GUSTA_ACTIVIDAD_ACTIVIDAD` FOREIGN KEY (`fecha_act`, `id_animal_user`, `id_persona_user`) REFERENCES `actividad` (`fecha_act`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_actividad: ~0 rows (aproximadamente)
DELETE FROM `me_gusta_actividad`;
/*!40000 ALTER TABLE `me_gusta_actividad` DISABLE KEYS */;
/*!40000 ALTER TABLE `me_gusta_actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_comentarios_fotos
CREATE TABLE IF NOT EXISTS `me_gusta_comentarios_fotos` (
  `fecha_comentario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_foto` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_ledamegusta` int(11) NOT NULL,
  `id_persona_ledamegusta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_COMENTARIOS_FOTOS_ANIMAL` (`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_COMENTARIOS_FOTOS_COMENTARIOS_FOTOS` (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ME_GUSTA_COMENTARIOS_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_ledamegusta`, `id_persona_ledamegusta`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_COMENTARIOS_FOTOS_COMENTARIOS_FOTOS` FOREIGN KEY (`fecha_comentario`, `id_animal_contacto`, `id_persona_contacto`, `fecha_foto`, `id_animal_user`, `id_persona_user`) REFERENCES `comentarios_fotos` (`fecha_comentario`, `id_animal_contacto`, `id_persona_contacto`, `fecha_foto`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_comentarios_fotos: ~0 rows (aproximadamente)
DELETE FROM `me_gusta_comentarios_fotos`;
/*!40000 ALTER TABLE `me_gusta_comentarios_fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `me_gusta_comentarios_fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_comentario_actividad
CREATE TABLE IF NOT EXISTS `me_gusta_comentario_actividad` (
  `fecha_comen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_act` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_ledamegusta` int(11) NOT NULL,
  `id_persona_ledamegusta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_COMENTARIO_ACTIVIDAD` (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` (`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  CONSTRAINT `FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_ledamegusta`, `id_persona_ledamegusta`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_COMENTARIO_ACTIVIDAD` FOREIGN KEY (`fecha_comen`, `fecha_act`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`) REFERENCES `comentario_actividad` (`fecha_comen`, `fecha_act`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_comentario_actividad: ~0 rows (aproximadamente)
DELETE FROM `me_gusta_comentario_actividad`;
/*!40000 ALTER TABLE `me_gusta_comentario_actividad` DISABLE KEYS */;
/*!40000 ALTER TABLE `me_gusta_comentario_actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_fotos
CREATE TABLE IF NOT EXISTS `me_gusta_fotos` (
  `fecha_foto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_ledamegusta` int(11) NOT NULL,
  `id_persona_ledamegusta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_foto`,`id_animal_user`,`id_persona_user`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_FOTOS_ANIMAL` (`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_FOTOS_FOTOS` (`fecha_foto`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ME_GUSTA_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_ledamegusta`, `id_persona_ledamegusta`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_FOTOS_FOTOS` FOREIGN KEY (`fecha_foto`, `id_animal_user`, `id_persona_user`) REFERENCES `fotos` (`fecha_foto`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_fotos: ~0 rows (aproximadamente)
DELETE FROM `me_gusta_fotos`;
/*!40000 ALTER TABLE `me_gusta_fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `me_gusta_fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.persona
CREATE TABLE IF NOT EXISTS `persona` (
  `nick` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `contra` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellido1` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellido2` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `email` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.persona: ~6 rows (aproximadamente)
DELETE FROM `persona`;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` (`nick`, `contra`, `nombre`, `apellido1`, `apellido2`, `email`) VALUES
	('adasd', 'asdasdsad', 'asdsad', 'asdsad', 'asdasd', 'asdasdsad'),
	('nick', 'asdfgh', 'asdasd', 'dadasd', 'dadasd', 'dadsad@dasdasdsad.com'),
	('paquito', 'sasuke', 'alejandro', 'coves', 'vives', 'vivex@hotmail.com'),
	('pequezo', '123456', 'dfdf', 'dfdf', 'dfdfd', 'viv_ex@hotmail.com'),
	('prueba', '1234', 'Manolo', 'Perez', 'Ahia', 'vivexxx@gmail.com'),
	('prueba2', '123456', 'sdadsd', 'sadasd', 'dasdsad', 'asdasdsad');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.seguidores
CREATE TABLE IF NOT EXISTS `seguidores` (
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `bloqueo_user` tinyint(1) DEFAULT '0',
  `bloqueo_contacto` tinyint(1) DEFAULT '0',
  `aceptado` tinyint(1) DEFAULT '1',
  `visto_por_contacto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_SEGUIDORES_ANIMAL_USER` (`id_animal_user`,`id_persona_user`),
  KEY `index_FK_SEGUIDORES_ANIMAL_CONTACTO` (`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT `FK_SEGUIDORES_ANIMAL_CONTACTO` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT `FK_SEGUIDORES_ANIMAL_USER` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.seguidores: ~3 rows (aproximadamente)
DELETE FROM `seguidores`;
/*!40000 ALTER TABLE `seguidores` DISABLE KEYS */;
INSERT INTO `seguidores` (`id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`, `bloqueo_user`, `bloqueo_contacto`, `aceptado`, `visto_por_contacto`) VALUES
	(33, 'paquito', 28, 'prueba', 0, 0, 1, 0),
	(33, 'paquito', 29, 'prueba', 0, 0, 1, 0),
	(33, 'paquito', 31, 'prueba', 0, 0, 1, 0);
/*!40000 ALTER TABLE `seguidores` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.tipos_logro
CREATE TABLE IF NOT EXISTS `tipos_logro` (
  `id_logro` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_logro` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_logro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.tipos_logro: ~0 rows (aproximadamente)
DELETE FROM `tipos_logro`;
/*!40000 ALTER TABLE `tipos_logro` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipos_logro` ENABLE KEYS */;


-- Volcando estructura para disparador guaubook.bins_seguidores
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `bins_seguidores` BEFORE INSERT ON `seguidores` FOR EACH ROW BEGIN
  
	DECLARE profile_followed integer;
	
	SELECT perfil from guaubook.animal where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto into profile_followed;
	
	if(profile_followed = 1) THEN
		update guaubook.animal set peticiones_amistad = peticiones_amistad +1 where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto;
		set new.aceptado = 0;
		
	end if;
	
	if(profile_followed = 0) THEN
		update guaubook.animal set seguidores = seguidores +1 where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto;
		update guaubook.animal set siguiendo = siguiendo +1 where id_animal = new.id_animal_user and id_persona = new.id_persona_user;
	end if;
	
  END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Volcando estructura para disparador guaubook.bup_seguidores
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `bup_seguidores` BEFORE UPDATE ON `seguidores` FOR EACH ROW BEGIN
  
	if(old.aceptado = 0 AND new.aceptado = 1) THEN
		update guaubook.animal set seguidores = seguidores +1, peticiones_amistad =  peticiones_amistad -1 where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto;
		update guaubook.animal set siguiendo = siguiendo +1 where id_animal = new.id_animal_user and id_persona = new.id_persona_user;
		
	end if;
	
  END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
