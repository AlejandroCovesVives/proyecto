-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.11-log - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura de base de datos para guaubook
CREATE DATABASE IF NOT EXISTS `guaubook` /*!40100 DEFAULT CHARACTER SET latin1 COLLATE latin1_spanish_ci */;
USE `guaubook`;


-- Volcando estructura para tabla guaubook.actividad
CREATE TABLE IF NOT EXISTS `actividad` (
  `fecha_act` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`fecha_act`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_ACTIVIDAD_ANIMAL` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.actividad: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `actividad` DISABLE KEYS */;
/*!40000 ALTER TABLE `actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.animal
CREATE TABLE IF NOT EXISTS `animal` (
  `id_animal` int(11) NOT NULL AUTO_INCREMENT,
  `id_persona` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `clase` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `perfil` tinyint(1) DEFAULT '0',
  `nivel` int(11) NOT NULL DEFAULT '1',
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `experiencia` int(11) NOT NULL DEFAULT '0',
  `ruta_foto_perfil` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `ciudad` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `poblacion` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id_animal`,`id_persona`),
  KEY `index_FK_ANIMAL_PERSONA` (`id_persona`),
  KEY `index_FK_ANIMAL_CLASE` (`clase`),
  CONSTRAINT `FK_ANIMAL_CLASE` FOREIGN KEY (`clase`) REFERENCES `clase` (`tipo`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ANIMAL_PERSONA` FOREIGN KEY (`id_persona`) REFERENCES `persona` (`nick`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.animal: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `animal` DISABLE KEYS */;
/*!40000 ALTER TABLE `animal` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.clase
CREATE TABLE IF NOT EXISTS `clase` (
  `tipo` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `descripcion` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.clase: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `clase` DISABLE KEYS */;
/*!40000 ALTER TABLE `clase` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.combate
CREATE TABLE IF NOT EXISTS `combate` (
  `fecha_combate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `visto_user` tinyint(1) DEFAULT '0',
  `visto_contacto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`fecha_combate`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_COMBATE_ANIMAL_USER` (`id_animal_user`,`id_persona_user`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT `FK_COMBATE_ANIMAL_CONTACTO` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_COMBATE_ANIMAL_USER` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.combate: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `combate` DISABLE KEYS */;
/*!40000 ALTER TABLE `combate` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.comentarios_fotos
CREATE TABLE IF NOT EXISTS `comentarios_fotos` (
  `fecha_comentario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_foto` timestamp NOT NULL,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `comentario_contacto` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `visto_user` tinyint(1) DEFAULT '0',
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_COMENTARIOS_FOTOS_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_COMENTARIOS_FOTOS_FOTOS` (`fecha_foto`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_COMENTARIOS_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_COMENTARIOS_FOTOS_FOTOS` FOREIGN KEY (`fecha_foto`, `id_animal_user`, `id_persona_user`) REFERENCES `fotos` (`fecha_foto`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.comentarios_fotos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `comentarios_fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentarios_fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.comentario_actividad
CREATE TABLE IF NOT EXISTS `comentario_actividad` (
  `fecha_comen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_act` timestamp NOT NULL,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `veces_editado` int(11) DEFAULT '0',
  `fecha_ult_edicion` datetime DEFAULT NULL,
  PRIMARY KEY (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ACTIVIDAD_ACTIVIDAD` (`fecha_act`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_COMENTARIO_ACTIVIDAD_ACTIVIDAD` FOREIGN KEY (`fecha_act`, `id_animal_user`, `id_persona_user`) REFERENCES `actividad` (`fecha_act`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE,
  CONSTRAINT `FK_COMENTARIO_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.comentario_actividad: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `comentario_actividad` DISABLE KEYS */;
/*!40000 ALTER TABLE `comentario_actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.fotos
CREATE TABLE IF NOT EXISTS `fotos` (
  `fecha_foto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre_foto` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `comentario_foto` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `lugar` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`fecha_foto`,`id_animal_user`,`id_persona_user`),
  KEY `index_FK_FOTOS_ANIMAL` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.fotos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.logros
CREATE TABLE IF NOT EXISTS `logros` (
  `fecha_logro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_logro` int(11) NOT NULL,
  `visto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`fecha_logro`,`id_animal_user`,`id_persona_user`,`id_logro`),
  KEY `index_FK_LOGROS_TIPOS_LOGRO` (`id_logro`),
  KEY `index_FK_LOGROS_ANIMAL` (`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_LOGROS_ANIMAL` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_LOGROS_TIPOS_LOGRO` FOREIGN KEY (`id_logro`) REFERENCES `tipos_logro` (`id_logro`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.logros: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `logros` DISABLE KEYS */;
/*!40000 ALTER TABLE `logros` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.mensajes
CREATE TABLE IF NOT EXISTS `mensajes` (
  `fecha_mensaje` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `mensaje` varchar(500) COLLATE latin1_spanish_ci DEFAULT NULL,
  `visto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`fecha_mensaje`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_MENSAJES_ANMAL_USER` (`id_animal_user`,`id_persona_user`),
  KEY `index_FK_MENSAJES_ANMAL_CONTACTO` (`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT `FK_MENSAJES_ANMAL_CONTACTO` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_MENSAJES_ANMAL_USER` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.mensajes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `mensajes` DISABLE KEYS */;
/*!40000 ALTER TABLE `mensajes` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_actividad
CREATE TABLE IF NOT EXISTS `me_gusta_actividad` (
  `fecha_act` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_ME_GUSTA_ACTIVIDAD_ANIMAL` (`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_ME_GUSTA_ACTIVIDAD_ACTIVIDAD` (`fecha_act`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ME_GUSTA_ACTIVIDAD_ACTIVIDAD` FOREIGN KEY (`fecha_act`, `id_animal_user`, `id_persona_user`) REFERENCES `actividad` (`fecha_act`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_actividad: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `me_gusta_actividad` DISABLE KEYS */;
/*!40000 ALTER TABLE `me_gusta_actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_comentarios_fotos
CREATE TABLE IF NOT EXISTS `me_gusta_comentarios_fotos` (
  `fecha_comentario` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `fecha_foto` timestamp NOT NULL,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_ledamegusta` int(11) NOT NULL,
  `id_persona_ledamegusta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_COMENTARIOS_FOTOS_ANIMAL` (`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_COMENTARIOS_FOTOS_COMENTARIOS_FOTOS` (`fecha_comentario`,`id_animal_contacto`,`id_persona_contacto`,`fecha_foto`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ME_GUSTA_COMENTARIOS_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_ledamegusta`, `id_persona_ledamegusta`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_COMENTARIOS_FOTOS_COMENTARIOS_FOTOS` FOREIGN KEY (`fecha_comentario`, `id_animal_contacto`, `id_persona_contacto`, `fecha_foto`, `id_animal_user`, `id_persona_user`) REFERENCES `comentarios_fotos` (`fecha_comentario`, `id_animal_contacto`, `id_persona_contacto`, `fecha_foto`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_comentarios_fotos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `me_gusta_comentarios_fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `me_gusta_comentarios_fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_comentario_actividad
CREATE TABLE IF NOT EXISTS `me_gusta_comentario_actividad` (
  `fecha_comen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_act` timestamp NOT NULL,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_ledamegusta` int(11) NOT NULL,
  `id_persona_ledamegusta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_COMENTARIO_ACTIVIDAD` (`fecha_comen`,`fecha_act`,`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` (`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  CONSTRAINT `FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_ANIMAL` FOREIGN KEY (`id_animal_ledamegusta`, `id_persona_ledamegusta`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_COMENTARIO_ACTIVIDAD_COMENTARIO_ACTIVIDAD` FOREIGN KEY (`fecha_comen`, `fecha_act`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`) REFERENCES `comentario_actividad` (`fecha_comen`, `fecha_act`, `id_animal_user`, `id_persona_user`, `id_animal_contacto`, `id_persona_contacto`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_comentario_actividad: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `me_gusta_comentario_actividad` DISABLE KEYS */;
/*!40000 ALTER TABLE `me_gusta_comentario_actividad` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.me_gusta_fotos
CREATE TABLE IF NOT EXISTS `me_gusta_fotos` (
  `fecha_foto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_ledamegusta` int(11) NOT NULL,
  `id_persona_ledamegusta` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `me_gusta` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`fecha_foto`,`id_animal_user`,`id_persona_user`,`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_FOTOS_ANIMAL` (`id_animal_ledamegusta`,`id_persona_ledamegusta`),
  KEY `index_FK_ME_GUSTA_FOTOS_FOTOS` (`fecha_foto`,`id_animal_user`,`id_persona_user`),
  CONSTRAINT `FK_ME_GUSTA_FOTOS_ANIMAL` FOREIGN KEY (`id_animal_ledamegusta`, `id_persona_ledamegusta`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_ME_GUSTA_FOTOS_FOTOS` FOREIGN KEY (`fecha_foto`, `id_animal_user`, `id_persona_user`) REFERENCES `fotos` (`fecha_foto`, `id_animal_user`, `id_persona_user`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.me_gusta_fotos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `me_gusta_fotos` DISABLE KEYS */;
/*!40000 ALTER TABLE `me_gusta_fotos` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.persona
CREATE TABLE IF NOT EXISTS `persona` (
  `nick` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `contra` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellido1` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `apellido2` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.persona: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.seguidores
CREATE TABLE IF NOT EXISTS `seguidores` (
  `id_animal_user` int(11) NOT NULL,
  `id_persona_user` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `id_animal_contacto` int(11) NOT NULL,
  `id_persona_contacto` varchar(50) COLLATE latin1_spanish_ci NOT NULL,
  `bloqueo` tinyint(1) DEFAULT '0',
  `visto_por_contacto` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_animal_user`,`id_persona_user`,`id_animal_contacto`,`id_persona_contacto`),
  KEY `index_FK_SEGUIDORES_ANIMAL_USER` (`id_animal_user`,`id_persona_user`),
  KEY `index_FK_SEGUIDORES_ANIMAL_CONTACTO` (`id_animal_contacto`,`id_persona_contacto`),
  CONSTRAINT `FK_SEGUIDORES_ANIMAL_CONTACTO` FOREIGN KEY (`id_animal_contacto`, `id_persona_contacto`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE,
  CONSTRAINT `FK_SEGUIDORES_ANIMAL_USER` FOREIGN KEY (`id_animal_user`, `id_persona_user`) REFERENCES `animal` (`id_animal`, `id_persona`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.seguidores: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `seguidores` DISABLE KEYS */;
/*!40000 ALTER TABLE `seguidores` ENABLE KEYS */;


-- Volcando estructura para tabla guaubook.tipos_logro
CREATE TABLE IF NOT EXISTS `tipos_logro` (
  `id_logro` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion_logro` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  PRIMARY KEY (`id_logro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

-- Volcando datos para la tabla guaubook.tipos_logro: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipos_logro` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipos_logro` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
