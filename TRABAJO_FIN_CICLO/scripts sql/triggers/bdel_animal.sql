
delimiter //

CREATE TRIGGER adel_animal AFTER DELETE ON guaubook.animal
  FOR EACH ROW
  BEGIN
	UPDATE guaubook.animal SET siguiendo = siguiendo -1 where id_animal IN (
		SELECT id_animal_user
		FROM guaubook.seguidores 
		WHERE id_animal_contacto = old.id_animal AND id_persona_contacto = old.id_persona);

	UPDATE guaubook.animal SET seguidores = seguidores -1 where id_animal IN (
		SELECT id_animal_contacto
		FROM guaubook.seguidores 
		WHERE id_animal_user = old.id_animal AND id_persona_user = old.id_persona);
		
  END;
//

delimiter ;

