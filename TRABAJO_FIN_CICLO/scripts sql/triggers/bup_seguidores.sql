

delimiter //

CREATE TRIGGER bup_seguidores BEFORE UPDATE ON guaubook.seguidores
  FOR EACH ROW
  BEGIN
  
	if(old.aceptado = 0 AND new.aceptado = 1) THEN
		update guaubook.animal set seguidores = seguidores +1, peticiones_amistad =  peticiones_amistad -1 where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto;
		update guaubook.animal set siguiendo = siguiendo +1 where id_animal = new.id_animal_user and id_persona = new.id_persona_user;
		
	end if;
	
  END;
//

delimiter ;