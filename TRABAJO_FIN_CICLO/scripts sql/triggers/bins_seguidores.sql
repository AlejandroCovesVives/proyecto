delimiter //

CREATE TRIGGER bins_seguidores BEFORE INSERT ON guaubook.seguidores
  FOR EACH ROW
  BEGIN
  
	DECLARE profile_followed integer;
	
	SELECT perfil from guaubook.animal where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto into profile_followed;
	
	if(profile_followed = 1) THEN
		update guaubook.animal set peticiones_amistad = peticiones_amistad +1 where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto;
		set new.aceptado = 0;
		
	end if;
	
	if(profile_followed = 0) THEN
		update guaubook.animal set seguidores = seguidores +1 where id_animal = new.id_animal_contacto and id_persona = new.id_persona_contacto;
		update guaubook.animal set siguiendo = siguiendo +1 where id_animal = new.id_animal_user and id_persona = new.id_persona_user;
	end if;
	
  END;
//

delimiter ;