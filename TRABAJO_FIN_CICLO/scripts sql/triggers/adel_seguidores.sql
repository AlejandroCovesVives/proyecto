delimiter //

CREATE TRIGGER adel_seguidores AFTER DELETE ON guaubook.seguidores
  FOR EACH ROW
  BEGIN
		update guaubook.animal set seguidores = seguidores -1 where id_animal = old.id_animal_contacto and id_persona = old.id_persona_contacto;
		update guaubook.animal set siguiendo = siguiendo -1 where id_animal = old.id_animal_user and id_persona = old.id_persona_user;
  END;
//

delimiter ;