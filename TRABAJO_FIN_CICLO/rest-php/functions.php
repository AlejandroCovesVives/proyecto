<?php
define("HOST", "localhost");
define("USER", "root");
define("PASS", "");
define("BBDD", "guaubook");

function fecha_actual(){
    return date('d//m//Y');
}

function conectar($user,$pass){
    $conexion=new mysqli(HOST,$user,$pass,BBDD);
    if($conexion->errno!=null){
        die("Error: ".$conexion->errno." tienes un problema en: ".$conexion->error);
    }
    return $conexion;
}
function conectar_root(){
    $conexion=new mysqli(HOST,USER,PASS,BBDD);
    if($conexion->errno!=null){
        die("Error: ".$conexion->errno." tienes un problema en: ".$conexion->error);
    }
    return $conexion;
}

function conectar_simple(){
    $conexion=new mysqli(HOST,USER,PASS,"");
    if($conexion->errno!=null){
        die("Error: ".$conexion->errno." tienes un problema en: ".$conexion->error);
    }
    return $conexion;
}

function desconectar($conexion){
    $conexion->close();
}


function compInicial(){
    $conexion=conectar_simple();
    $basedatos=BBDD;
    $sql="SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$basedatos'";
    $consulta=$conexion->query($sql);
    if($consulta->num_rows>0){
        return true;
    }else{
        return false;
    }
}


function limpia($txt) {
    if (!is_array($txt)) {
        $txt = htmlspecialchars(strip_tags(stripslashes($txt)));

        return strtr($txt, array(
            "\0" => "",
            "'"  => "&#39;",
            "\"" => "&#34;",
            "\\" => "&#92;",
            "<"  => "&lt;",
            ">"  => "&gt;",
        ));
    }
}

function cleanData($data) {
    foreach($data as $key => $val)
    {
        $cleaned[$key] = limpia($val);
    }
    return $cleaned;
}


function seleccionado_error($introducido, $actual){
    if($introducido==$actual){
        return "class='error'";
    }else{
        return "";
    }
}

function seleccionado($introducido, $actual){
    if($introducido==$actual){
        return "selected";
    }else{
        return "";
    }
}
function esEntero($numero){
    if(!is_numeric($numero)){
        return false;
    }
    if($numero<=0){
        return false;
    }
    return true;
}

function resizeImagen($ruta, $nombre, $alto, $ancho,$nombreN,$extension){
    $rutaImagenOriginal = $ruta.$nombre;
    if($extension == 'GIF' || $extension == 'gif'){
    $img_original = imagecreatefromgif($rutaImagenOriginal);
    }
    if($extension == 'jpg' || $extension == 'JPG'){
    $img_original = imagecreatefromjpeg($rutaImagenOriginal);
    }
    if($extension == 'png' || $extension == 'PNG'){
    $img_original = imagecreatefrompng($rutaImagenOriginal);
    }
    $max_ancho = $ancho;
    $max_alto = $alto;
    list($ancho,$alto)=getimagesize($rutaImagenOriginal);
    $x_ratio = $max_ancho / $ancho;
    $y_ratio = $max_alto / $alto;
    if( ($ancho <= $max_ancho) && ($alto <= $max_alto) ){//Si ancho 
  	$ancho_final = $ancho;
		$alto_final = $alto;
	} elseif (($x_ratio * $alto) < $max_alto){
		$alto_final = ceil($x_ratio * $alto);
		$ancho_final = $max_ancho;
	} else{
		$ancho_final = ceil($y_ratio * $ancho);
		$alto_final = $max_alto;
	}
    $tmp=imagecreatetruecolor($ancho_final,$alto_final);
    imagecopyresampled($tmp,$img_original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
    imagedestroy($img_original);
    $calidad=70;
    imagejpeg($tmp,$ruta.$nombreN,$calidad);
    
}