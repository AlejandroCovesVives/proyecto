<?php
require_once("../functions.php");
session_start();
if(isset($_POST['nick'])){
    $user=$_POST['nick'];
    $arrayAnimales=Array();
    $conexion=conectar("root","");
    $sql="SELECT * FROM animal WHERE id_persona=?";
    $consulta_prep=$conexion->prepare($sql);
    $consulta_prep->bind_param("s",$user);
    $ok=$consulta_prep->execute();
    $consulta=$consulta_prep->get_result();
    if($consulta->num_rows>0){
        while($linea=$consulta->fetch_assoc()){
            $arrayAnimales[]=array_map('utf8_encode',$linea);
        }
    }
    $arrayTodo=Array();
    $arrayClases=Array();
    $sql="SELECT * FROM clase order by tipo asc";
    $consulta=$conexion->query($sql);
    if ($consulta->num_rows>0){
        while($linea=$consulta->fetch_assoc()){
            $arrayClases[]=array_map('utf8_encode',$linea);
        }
    }
    $arrayTodo["clases"]=$arrayClases;
    $arrayTodo["animales"]=$arrayAnimales;

    desconectar($conexion);
    echo json_encode($arrayTodo);

}

if(isset($_POST['user'])){

    $_SECPOST = cleanData($_POST); // limpia todos los datos enviados por POST
    $user=$_SECPOST["user"];
    $animal=utf8_decode($_SECPOST["animal"]);
    $clase=$_SECPOST["clase"];
    $perfil=$_SECPOST["perfil"];
    $ciudad=utf8_decode($_SECPOST["ciudad"]);
    $poblacion=$_SECPOST["poblacion"];

    $conexion2=conectar("root","");
    $sql="INSERT INTO ANIMAL(id_persona, nombre, clase, perfil, ciudad, poblacion) VALUES (?,?,?,?,?,?)";
    $consulta_prep=$conexion2->prepare($sql);
    $consulta_prep->bind_param("sssiss",$userUser, $animalUser, $claseUser, $perfilUser,$ciudadUser,$poblacionUser);
    $userUser=$user;
    $animalUser=$animal;
    $claseUser=$clase;
    $ciudaduser=$ciudad;
    $perfilUser=$perfil;
    $ciudadUser=$ciudad;
    $poblacionUser=$poblacion;
    $ok=$consulta_prep->execute();
    if((($consulta_prep->affected_rows)==0) || !$ok){
        $valor="no";
    }else{
        $sql= "SELECT * FROM ANIMAL WHERE id_persona = ? order by fecha_creacion desc LIMIT 1 ";
        $consulta_prep=$conexion2->prepare($sql);
        $consulta_prep->bind_param("s", $newUser);
        $newUser= $user;
        $ok=$consulta_prep->execute();
        $consulta=$consulta_prep->get_result();
        $linea=$consulta->fetch_assoc();
        $valor=$linea["id_animal"];
        $carpeta = '../../usuarios/'.$user."/".$valor;
        if (!file_exists($carpeta)) {
            mkdir($carpeta, 0777, true);
        }
    }
    desconectar($conexion2);
    echo json_encode($valor);
    //header("location:../../chooseAnimal.php?creacion=$respuesta");
}


?>