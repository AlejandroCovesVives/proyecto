<?php
require_once("../functions.php");
session_start();
$_SESSION = Array();
if(isset($_POST['human'])){
    $_SECPOST = cleanData($_POST); // limpia todos los datos enviados por POST
    $nick = $_SECPOST['human'];
    $pwd = $_SECPOST['pass'];
    $conexion=conectar("root","");
    //$conexion=conectar_root();
    $sql="SELECT * FROM persona WHERE nick=? AND contra=?";
    $consulta_prep=$conexion->prepare($sql);
    $consulta_prep->bind_param("ss",$usuario,$contrasena);
    $usuario=$nick;
    $contrasena=$pwd;
    $ok=$consulta_prep->execute();
    $consulta=$consulta_prep->get_result();
    if((($consulta->num_rows)==0) || !$ok){
        desconectar($conexion);
        session_start();
        header("location:../../login.php?error=1&human=$nick");
    }else{
        $_SESSION["human"]=$nick;
        desconectar($conexion);
        header("location:../../chooseAnimal.php");
    }

}else{
    header("location:../../login.php");
}

?>