/**
 * Created by Administrador on 19/04/16.
 */
window.onload=iniciar();
var arrayAnimales=new Array();
var arrayClases=new Array();
function iniciar(){
    //alert("entra");
     document.getElementById("nuevo").addEventListener("click",mostrarCrear,false);
     document.getElementById("ocultarCrear").addEventListener("click",ocultarCrear,false);
     document.getElementById("register").addEventListener("click",crearAnimal,false);

    recogerAnimales();
}

function mostrarCrear(){
    document.getElementById("formulario").style.display="inline";
    document.getElementById("nuevo").style.display="none";
}

function ocultarCrear(){
    document.getElementById("formulario").style.display="none";
    document.getElementById("nuevo").style.display="inline";
    document.getElementById("formularioform").reset();
    limpiar_formulario("formularioform");
}

function crearAnimal(){

    var arrayFallos = new Array();
    arrayFallos.push(validacionNombre("animal"));
    arrayFallos.push(validacionNombre("poblacion"));
    arrayFallos.push(validacionNombre("ciudad"));

    var flag = true;

    for (var i=0; i<arrayFallos.length;i++){
        if(!arrayFallos[i]){
            flag = false;
        }
    }

    if(flag){
        var radios = document.getElementsByName('perfil');
        var value;
        for (var i = 0; i < radios.length; i++) {
            if (radios[i].checked) {
                value = radios[i].value;
            }
        }

        var parametros = {
            user:document.getElementById("user").value,
            animal:document.getElementById("animal").value,
            clase:document.getElementById("clase").value,
            perfil:value,
            ciudad:document.getElementById("ciudad").value,
            poblacion:document.getElementById("poblacion").value
        };

        $.ajax({
            data:  parametros,
            url:   'rest-php/login/chooseAnimal.php',
            type:  'post',
            /*beforeSend: function () {

             },*/
            success:  function (response) {
                var nuevo=JSON.parse(response);
                if(nuevo!="no"){
                    var animal= new Animal(nuevo,document.getElementById("user").value,document.getElementById("animal").value);
                    animal.clase=document.getElementById("clase").value;
                    pintarAnimal(animal);
                    $("#CreacionCorrecta").delay(10).slideToggle("slow");
                    $("#CreacionCorrecta").delay(5000).slideToggle("slow");
                    document.getElementById("formularioform").reset();
                    limpiar_formulario("formularioform");
                    ocultarCrear();
                }
            }

        });
    }

}



function recogerAnimales(){
    var post= $.post("rest-php/login/chooseAnimal.php",{nick:document.getElementById("user").value});
    var tempanimal;
        post.done(function(msg){
            //alert(msg);
            var nuevo=JSON.parse(msg);
           // alert(nuevo);
            var selectclase=document.getElementById("clase");
            var tempoption;
            var temptextnode;
            for (var i=0;i<nuevo.clases.length;i++){
                arrayClases.push(nuevo.clases[i].tipo);
                tempoption=document.createElement("option");
                tempoption.setAttribute("value",nuevo.clases[i].tipo);
                temptextnode=document.createTextNode(nuevo.clases[i].tipo);
                tempoption.appendChild(temptextnode);
                if(nuevo.clases[i].tipo=="Perro"){
                    tempoption.setAttribute("selected","");
                }
                selectclase.appendChild(tempoption);
            }


            for (var i=0; i<nuevo.animales.length;i++){
                tempanimal=new Animal();
                tempanimal.fromJSON(nuevo.animales[i]);
                arrayAnimales.push(tempanimal);
            }
            $("#listaAnimales").empty();
            for(var i=0;i<arrayAnimales.length;i++){
                pintarAnimal(arrayAnimales[i]);
            }

        });
}


function validacionNombre(id){
    var nombre=document.getElementById(id).value;
    if(validarPalabra(nombre)){
        acierto_id(id);
        return true;
    }else{
        error_id(id);
        var name;

        $("#"+id+"Alert").delay(10).slideToggle("slow");
        $("#"+id+"Alert").delay(5000).slideToggle("slow");
        return false;
    }
}





function pintarAnimal(animal){
        var divanimales=document.getElementById("listaAnimales");
        var tempdiv;
        var temptext;
        var tempp;
        var tempfoto;
        var temphiden;
        var tempform = document.createElement("form");
        tempform.setAttribute("method","post");
        tempform.setAttribute("action", "starter.php");
        tempdiv=document.createElement("div");
        tempdiv.setAttribute("class","small-box bg-aqua col-md-6 with-border");
        tempp=document.createElement("p");
        temptext=document.createTextNode(animal.clase+"  "+animal.nombre);
        tempp.appendChild(temptext);
        tempdiv.appendChild(tempp);
        tempp.setAttribute("style","text-align:center;");
        tempbutton=document.createElement("button");
        tempfoto=document.createElement("input");
        tempfoto.setAttribute("type","image");
        tempfoto.setAttribute("name","animal");
        tempfoto.setAttribute("value",animal.id_animal);
        tempfoto.setAttribute("id",animal.id_animal);
        tempfoto.setAttribute("class","img-circle img-responsive");
        tempfoto.setAttribute("style","width: 150px;height:150px;");
        temphiden=document.createElement("input");
        temphiden.setAttribute("type","hidden");
        temphiden.setAttribute("value",animal.id_animal);
        temphiden.setAttribute("name","animal_id");
        tempfoto.setAttribute("alt","User Image");
        if(animal.ruta_foto_perfil){
            tempfoto.setAttribute("src","usuarios/"+animal.id_persona+"/"+animal.id_animal+"/"+animal.ruta_foto_perfil);
        }else{
            tempfoto.setAttribute("src","usuarios/nofile.png");
        }
        tempform.appendChild(tempfoto);
        tempform.appendChild(temphiden);
        tempdiv.appendChild(tempform);
        tempdiv.setAttribute("style","margin-botton:10px; padding-botton: 10px;")
        divanimales.appendChild(tempdiv);
}

