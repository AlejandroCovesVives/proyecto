/**
 * Created by Administrador on 20/02/16.
 */
"use strict";

//para validar que el numero introducido es mayor que 0 y positivo
// y entero y dentro del limite introducido (limite entra)
function validarNumPosLim(num, limite){
    //si no es un numero o menor de 0 o mayor que el limite
    if(isNaN(num) || num<=0 || num>limite){
        return false;
    }
    for(var i=0;i<num.length;i++){ //para que no tenga coma
        if(num.charAt(i)=="."){
            return false;
        }
    }
    return true;
}

//para validar que el numero introducido es mayor que 0 y positivo y entero
function validarNumPos(num){
    //si no es un numero o menor o igual de 0 entra
    if(isNaN(num) || num<=0 || num==""){
        return false;
    }
    for(var i=0;i<num.length;i++){ //para que no tenga coma
        if(num.charAt(i)=="."){
            return false;
        }
    }
    return true;
}

function validarNumPosDecimal(num){
    //si no es un numero o menor o igual de 0 entra
    if(isNaN(num) || num<=0 || num==""){
        return false;
    }
    return true;
}

//para validar que el numero introducido es mayor o igual que 0 y positivo y entero
function validarNumCon0(num){
    //si no es un numero o menor de 0 entra
    if(isNaN(num) || num<0 || num==""){
        return false;
    }
    for(var i=0;i<num.length;i++){ //para que no tenga coma
        if(num.charAt(i)=="."){
            return false;
        }
    }
    return true;
}

function validarNumCon0a100(num){
    //si no es un numero o menor de 0 entra
    if(isNaN(num) || num<0 || num=="" || num>100){
        return false;
    }
    for(var i=0;i<num.length;i++){ //para que no tenga coma
        if(num.charAt(i)=="."){
            return false;
        }
    }
    return true;
}




//comprueba que lo que has introducido sea una palabra
function validarPalabra(pal){
    var palabra=pal.toLowerCase();
    //comprueba letra a letra que sus char esten dentro del rango de letras minusculas del asci
    for(var i=0; i<palabra.length;i++){
        if(palabra.charCodeAt(i)!=241){
            if(palabra.charCodeAt(i)<97 || palabra.charCodeAt(i)>122) {
                return false;
            }
        }
    }
    //en caso de no que se introduzca nada
    if((palabra.length<=0) || (palabra=="") || (palabra==null) || (palabra==undefined)){
        return false;
    }
    return true;
}

//comprueba que lo que has introducido sea una palabra
function validarPalabrasconNums(pal){
    var palabra=pal;
    //en caso de que no se haya introducido nada
    if((palabra.length<=0) || (palabra=="") || (palabra==null) || (palabra==undefined)){
        return false;
    }
    return true;
}

//para validar que el dni esta en formato correcto
function esDNI(dni) {
    var letter_DNI;
    var numbers_DNI;
    var letter_calculated;
    // Comprobamos si tiene longitud 9
    if(dni.length == 9) {
        // Extraemos los 8 primeros caracteres
        numbers_DNI = dni.substring(0,8);

        // Funci�n que comprueba si un n�mero es un
        // entero no negativo
        var isInteger = function(n) {
            var intRegex = /^\d+$/;
            if(intRegex.test(n)) return true;
            return false;
        }

        // Comprobamos si los 8 primeros caracteres
        // son n�meros
        if(!isInteger(numbers_DNI)) return false;

        // Extraemos el �ltimo caracter
        letter_DNI = dni.substring(8);

        // Funci�n que hemos elaborado antes para
        // el c�lculo de la letra
        var get_letter_DNI = function(dni) {
            var table = "TRWAGMYFPDXBNJZSQVHLCKE";
            return table.charAt(dni % 23);
        }

        // Calculamos la letra de las cifras que se
        // han introducido
        letter_calculated = get_letter_DNI(numbers_DNI);

        // Si la letra es correcta damos por v�lido el DNI
        if(letter_calculated.toUpperCase() == letter_DNI.toUpperCase()) return true;
    }
    return false;
}


function validarEmail( email ) {
    var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if ( expr.test(email) ){
        return true;
    }else{
        return false;
    }
}

function validarTelefono(tel){
    var expresionRegular1=/^([0-9]+){9}$/;//<--- con esto vamos a validar el numero
    var expresionRegular2=/\s/;//<--- con esto vamos a validar que no tenga espacios en blanco

    if(tel==''){
        return false;
    }  else if(expresionRegular2.test(tel)){
        return false;
    }
    else if(!expresionRegular1.test(tel)){
        return false;
    }else{
        return true;
    }
}

function cif_valido(cif) {

    cif = cif.toUpperCase();
    var cifRegEx1 = /^[ABEH][0-9]{8}$/i;
    var cifRegEx2 = /^[KPQS][0-9]{7}[A-J]$/i;
    var cifRegEx3 = /^[CDFGJLMNRUVW][0-9]{7}[0-9A-J]$/i;
    if (cifRegEx1.test(cif) || cifRegEx2.test(cif) || cifRegEx3.test(cif)) {
        var control = cif.charAt(cif.length - 1);
        var suma_A = 0;
        var suma_B = 0;
        for (var i = 1; i < 8; i++) {
            if (i % 2 == 0) suma_A += parseInt(cif.charAt(i));
            else {
                var t = (cif.charAt(i) * 2) + "";
                var p = 0;
                for (var j = 0; j < t.length; j++) {
                    p += parseInt(t.charAt(j));
                }
                suma_B += p;
            }
        }

        var suma_C = (suma_A + suma_B) + "";
        var suma_D = (10 - parseInt(suma_C.charAt(suma_C.length - 1))) % 10;
        var letras = "JABCDEFGHI";
        if (control >= "0" && control <= "9") return (control == suma_D);
        else return (control == letras.charAt(suma_D));
    }
    else return false;
}