/**
 * Created by Administrador on 26/01/16.
 */
"use strict";

//para ocultar un nodo
function ocultar_id(id){
    document.getElementById(id).style.display="none";
}

//para mostrar un nodo oculo
function mostrar_id(id){
    document.getElementById(id).style.display="inline";
}

//para mostrar que el campo es correcto
function acierto_id(id){
    document.getElementById(id).style.borderColor = "green";
}

function error_id(id){
    document.getElementById(id).style.borderColor = "red";
}

function limpiar_id(id){
    document.getElementById(id).style.borderColor = "lightgrey";
    document.getElementById(id).value= "";
}

function sacar_array_inicial_errores(idformulario){
    var array=new Array();
    var temp;
    var formulario=document.getElementById(idformulario);
    for(var i=0;i<formulario.elements.length;i++){
        if(formulario.elements[i].type=="text"){
            temp=new Array();
            temp.push(formulario.elements[i].name);
            temp.push(false);
            array.push(temp);
        }
        /*if(formulario.elements[i].type=="password"){
            temp=new Array();
            temp.push("contra");
            temp.push(false);
            array.push(temp);
        }*/
    }
    return array;
}

function acierto_validacion_nombre(array,nombre){
    var arrayadevolver=array;
    for(var i=0;i<arrayadevolver.length;i++){
        if(arrayadevolver[i][0]==nombre){
            arrayadevolver[i][1]=true;
        }
    }
    return arrayadevolver;
}

function error_validacion_nombre(array,nombre){
    var arrayadevolver=array;
    for(var i=0;i<arrayadevolver.length;i++){
        if(arrayadevolver[i][0]==nombre){
            arrayadevolver[i][1]=false;
        }
    }
    return arrayadevolver;
}

function limpiar_formulario(id){
    var formulario=document.getElementById(id);
    for(var i=0;i<formulario.elements.length;i++){
        if(formulario.elements[i].type=="text" || formulario.elements[i].type=="password" || formulario.elements[i].type=="email"){
            formulario.elements[i].value="";
            formulario.elements[i].style.borderColor = "lightgrey";
        }
    }
}

function colores_formulario(id){
    var formulario=document.getElementById(id);
    for(var i=0;i<formulario.elements.length;i++){
        if(formulario.elements[i].type=="text" || formulario.elements[i].type=="password"){
            formulario.elements[i].style.borderColor = "lightgrey";
        }
    }
}


function createDateFromMySql (mysql_string){
    var t, result = null;
    
    if(typeof mysql_string === 'string'){
        t = mysql_string.split(/[- :]/);
        result = new Date(t[0],t[1],t[2],t[3] || 0,t[4] || 0,t[5] || 0);
    }
    return result;
}

function monthInSpanish (numeroMes){
    switch(numeroMes){
            
        case 0: return "Enero";
        case 1: return "Febrero";
        case 2: return "Marzo";
        case 3: return "Abril";
        case 4: return "Mayo";
        case 5: return "Junio";
        case 6: return "Julio";
        case 7: return "Agosto";
        case 8: return "Septiembre";
        case 9: return "Octubre";
        case 10: return "Noviembre";
        case 11: return "Diciembre";
        default: return "";   
    }    
}


function fileExists(url){
    var flag = false;
    var parametros = {
            urlimage:url
        };

        $.ajax({
            data:  parametros,
            url:   'rest-php/getters/info-animal.php',
            type:  'post',
            async: false,
            /*beforeSend: function () {

             },*/
            success:  function (response) {
                //alert(response);
                var nuevo=JSON.parse(response);
                if(nuevo == "si"){
                    flag = true;
                    return true;
                }
            }
        });
    
    return flag;
    
}

function getAnimal(id_animal){
    var animalreturn = new Animal();
    var parametros = {
            animal:id_animal
        };

        $.ajax({
            data:  parametros,
            url:   'rest-php/getters/info-animal.php',
            type:  'post',
            async: false,
            /*beforeSend: function () {

             },*/
            success:  function (response) {
                var nuevo=JSON.parse(response);
                var animal = new Animal();
                animal.fromJSON(nuevo.animal);
                animalreturn = animal;
                return animal;
            }

        });
    return animalreturn;
}



function getSpanishTime(fecha){
    var fecha = createDateFromMySql(fecha);
    var stringfecha = fecha.getDate()+"/"+(fecha.getMonth())+"/"+fecha.getFullYear();
    var stringHora = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
    return (stringfecha+"   "+stringHora);
    
}



function hacerEncabezado(arraycabecera){
    var nodotr=document.createElement("tr");
    var nodoth;
    var texto;
    for(var i=0; i<arraycabecera.length;i++){
        nodoth=document.createElement("th");
        texto=document.createTextNode(arraycabecera[i]);
        nodoth.appendChild(texto);
        nodotr.appendChild(nodoth);
    }
    return nodotr;
}

function encode_utf8(s){
    return unescape(encodeURIComponent(s));
}

function decode_utf8(s){
    return decodeURIComponent(escape(s));
}

