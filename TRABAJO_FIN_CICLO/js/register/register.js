"use strict";
window.onload=iniciar();
function iniciar(){
    limpiarform();
    //document.getElementById("user").addEventListener("blur",validacionNick,false);
    document.getElementById("register").addEventListener("click",validarTodo,false);
    document.getElementById("limpiar").addEventListener("click",limpiarform,false);
}

//para volver para atras

function limpiarform(){
    limpiar_formulario("formulario");

}

function validacionNombre(){
    var nombre=document.getElementById("name").value;
    if(validarPalabra(nombre)){
        acierto_id("name");
        return true;
    }else{
        error_id("name");
        $("#nameAlert").delay(10).slideToggle("slow");
        $("#nameAlert").delay(5000).slideToggle("slow");
        return false;
    }
}

function validacionApellido(id){
    var nombre=document.getElementById(id).value;
    if(validarPalabra(nombre)){
        acierto_id(id);
        return true;
    }else{
        error_id(id);
        $("#"+id+"Alert").delay(10).slideToggle("slow");
        $("#"+id+"Alert").delay(5000).slideToggle("slow");
        return false;
    }
}


//validar pass no vacio
function validacionContra(){
    var contra=document.getElementById("password").value;
    if(validarPalabrasconNums(contra)){
        if(contra.length<6){
            error_id("password");
            $("#passwordAlert").delay(10).slideToggle("slow");
            $("#passwordAlert").delay(5000).slideToggle("slow");
            document.getElementById("password").value = "";
            document.getElementById("password2").value = "";
            return false;
        }else{
            var contra2 = document.getElementById("password2").value;
            if(contra == contra2){
                acierto_id("password");
                acierto_id("password2");
                return true;
            }else{
                error_id("password");
                error_id("password2");
                $("#password2Alert").delay(10).slideToggle("slow");
                $("#password2Alert").delay(5000).slideToggle("slow");
                document.getElementById("password").value = "";
                document.getElementById("password2").value = "";
                return false;
            }
        }
    }else{
        error_id("password");
        document.getElementById("password").value = "";
        document.getElementById("password2").value = "";
        $("#passwordAlert").delay(10).slideToggle("slow");
        $("#passwordAlert").delay(5000).slideToggle("slow");
        return false;
    }
}



//validar email
function validacionEmail(){
    var email=document.getElementById("email").value;
    if(validarEmail(email)){
        acierto_id("email");
        return true;
    }else{
        error_id("email");
        $("#emailAlert").delay(10).slideToggle("slow");
        $("#emailAlert").delay(5000).slideToggle("slow");
        return false;
    }
}


function validarTodo(){
    var arrayFallos = new Array();
    arrayFallos.push(validacionNick());
    arrayFallos.push(validacionNombre());
    arrayFallos.push(validacionApellido("ape1"));
    arrayFallos.push(validacionApellido("ape2"));
    arrayFallos.push(validacionEmail());
    arrayFallos.push(validacionContra());

    var flag = true;

    for (var i=0; i<arrayFallos.length;i++){
        if(!arrayFallos[i]){
            flag = false;
        }
    }
    if(flag){
        var post= $.post("rest-php/register/register.php",{user:document.getElementById("user").value,
            name:document.getElementById("name").value,ape1:document.getElementById("ape1").value,
            ape2:document.getElementById("ape2").value,email:document.getElementById("email").value,
            password:document.getElementById("password").value});
        post.done(function(msg){
            var nuevo=JSON.parse(msg);
            if(nuevo.resultado=="si"){
                $("#CreacionCorrecta").delay(10).slideToggle("slow");
                $("#CreacionCorrecta").delay(5000).slideToggle("slow");
            }else{
                $("#error").delay(10).slideToggle("slow");
                $("#error").delay(5000).slideToggle("slow");
            }
            limpiarform();
        });
    }
}

//validar nick
function validacionNick(){
    var flag = true;
    var nick=document.getElementById("user").value;
    if(validarPalabrasconNums(nick)){
        var post= $.post("rest-php/register/register.php",{nick:document.getElementById("user").value});
        post.done(function(msg){
			alert("entra "+nuevo.resultado);
            var nuevo=JSON.parse(msg);
            if(nuevo.resultado=="si"){
                document.getElementById("user").style.borderColor = "green";
                //$("#nickvalido").delay(10).slideToggle("slow");
                //$("#nickvalido").delay(5000).slideToggle("slow");
                flag = true;
            }else{
                error_id("user");
                $("#userOcupado").delay(10).slideToggle("slow");
                $("#userOcupado").delay(5000).slideToggle("slow");
                flag = false;
            }
        });
    }else{
        error_id("user");
        $("#userAlert").delay(10).slideToggle("slow");
        $("#userAlert").delay(5000).slideToggle("slow");
        flag = false;
    }
    return flag;
}


