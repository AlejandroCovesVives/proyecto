/**
 * Created by Administrador on 7/05/16.
 */

"use strict";
window.onload=iniciar();
var animal = new Animal();
var limiteActividad = 10;

function iniciar(){
    cargaInicial();
    document.getElementById("cambiarDatos").addEventListener("click",modificarAnimal,false);
    document.getElementById("cambiarFoto").addEventListener("click",cambiarImagen,false);
    document.getElementById("botonVerMas").addEventListener("click",verMasActividad,false);
    $(function(){
        $("body").delegate(".inputComentario","keypress",function(event){
            if(event.which === 13){           

            if(this.value.length>0){
                var that = this;
                var arrayThings = this.name.split("%%%");
                var fechaAct = arrayThings[0];
                var idAnimalActivity =  arrayThings[1];
                var idPersonActivity =  arrayThings[2];
                var idAnimalYou = arrayThings[3];
                var idPersonYou = arrayThings[4];
                
                var divcomenttoappend="";
                var parametros = {
                    personToComent:idPersonYou,
                    animalToComent:idAnimalYou,
                    comentToComent: this.value,
                    dateActivityToComent: fechaAct,
                    animalActivityToComent: idAnimalActivity,
                    personActivityToComent: idPersonActivity

                };

                $.ajax({
                       data:  parametros,
                        url:   'rest-php/starter/starter.php',
                        type:  'post',
                        async: false,
                        success:  function (response) {
                            var nuevo=JSON.parse(response);
                            var nuevaComent = new ComentarioActividad();
                            nuevaComent.fromJSON(nuevo.comentarioActividad);
                            var divcoment = nuevaComent.createDivComen(nuevaComent, idAnimalYou,idPersonYou);
                            divcomenttoappend = divcoment;
                            var ahia = document.getElementById(fechaAct+"%%%"+idAnimalActivity+"%%%"+idPersonActivity);
                            $(ahia).prepend(divcoment);
                            $(that).val("");
                            $('.numeroComensActividad').each(function(index){
                                if(this.attributes[1].value == (fechaAct+"%%%"+idAnimalActivity+"%%%"+idPersonActivity)){
                                    var numerocomens = parseInt(this.attributes[2].value);
                                    numerocomens++;
                                    $(this).empty();
                                    this.setAttribute("name",numerocomens);
                                    $(this).append(" - "+numerocomens+" Comentarios");
                                }
                            });
                        }
                    });
                }   
            }
        });
    });
    
     $(function(){
        $("body").delegate(".botonMeGustaActividad","click",function(){          

                var that = this;
                var arrayThings = this.value.split("%%%");
                var fechaAct = arrayThings[0];
                var idAnimalActivity =  arrayThings[1];
                var idPersonActivity =  arrayThings[2];
                var idAnimalYou = animal.id_animal;
                var idPersonYou = animal.id_persona;
                var parametros = {
                    personToLike:idPersonYou,
                    animalToLike:idAnimalYou,
                    dateActivityToLike: fechaAct,
                    animalActivityToLike: idAnimalActivity,
                    personActivityToLike: idPersonActivity

                };

                $.ajax({
                       data:  parametros,
                        url:   'rest-php/starter/starter.php',
                        type:  'post',
                        async: false,
                        success:  function (response) {
                            var nuevo=JSON.parse(response);
                    
                            if(nuevo.resultado=="si"){
                                that.setAttribute("class","btn btn-primary btn-xs botonMeGustaActividad");
                                that.childNodes[0].childNodes[0].className = "fa fa-thumbs-up";
                    
                            }else{
                                that.setAttribute("class","btn btn-default btn-xs botonMeGustaActividad");
                                that.childNodes[0].childNodes[0].className = "fa fa-thumbs-o-up";
                            }
                            $('.megustaactividad').each(function(index){
                                if(this.attributes[1].value == (fechaAct+"%%%"+idAnimalActivity+"%%%"+idPersonActivity)){
                                    var numerocomens = parseInt(this.attributes[2].value);
                                    if(nuevo.resultado=="si"){
                                        numerocomens++;
                                    }else{
                                        numerocomens--;
                                    }
                                    $(this).empty();
                                    this.setAttribute("name",numerocomens);
                                    $(this).append(numerocomens+" Ladridos");
                                }
                            });
                           
                        }
                });
        });
    });
    
   
    $(function(){
        $("body").delegate(".botonMeGustaComentarioActividad","click",function(){          

                var that = this;
                var arrayThings = this.value.split("%%%");
                var fechaAct = arrayThings[0];
                var idAnimalActivityComent =  arrayThings[1];
                var idPersonActivityComent =  arrayThings[2];
            
                var fechaComent = arrayThings[3];
                var idAnimalComent =  arrayThings[4];
                var idPersonComent =  arrayThings[5];
            
                var idAnimalYou = animal.id_animal;
                var idPersonYou = animal.id_persona;
            
            
                var parametros = {
                    personToLike:idPersonYou,
                    animalToLike:idAnimalYou,
                    dateActivityComentToLike: fechaAct,
                    animalActivityComentToLike: idAnimalActivityComent,
                    personActivityComentToLike: idPersonActivityComent,
                    dateComentToLike: fechaComent,
                    animalComentToLike: idAnimalComent,
                    personComentToLike: idPersonComent
                };

                $.ajax({
                       data:  parametros,
                        url:   'rest-php/starter/starter.php',
                        type:  'post',
                        async: false,
                        success:  function (response) {
                            var nuevo=JSON.parse(response);
                    
                            if(nuevo.resultado=="si"){
                                that.setAttribute("class","btn btn-primary btn-xs botonMeGustaComentarioActividad");
                                that.childNodes[0].childNodes[0].className = "fa fa-heart";
                    
                    
                            }else{
                                that.setAttribute("class","btn btn-default btn-xs botonMeGustaComentarioActividad");
                                that.childNodes[0].childNodes[0].className = "fa fa-heart-o";
                            }
                            $('.megustacomentarioactividad').each(function(index){
                                if(this.attributes[1].value == (fechaAct+"%%%"+idAnimalActivityComent+"%%%"+idPersonActivityComent+"%%%"+fechaComent+"%%%"+idAnimalComent+"%%%"+idPersonComent)){
                                    var numerocomens = parseInt(this.attributes[2].value);
                                    if(nuevo.resultado=="si"){
                                        numerocomens++;
                                    }else{
                                        numerocomens--;
                                    }
                                    $(this).empty();
                                    this.setAttribute("name",numerocomens);
                                    $(this).append("\u00A0\u00A0\u00A0\u00A0"+numerocomens+" Ladridos ");
                                }
                            });
                           
                        }
                });
        });
    });

}

function cargaInicial(){
	
	var nick=document.getElementById("user").value;
	var animal2=document.getElementById("animal").value;
    if(document.getElementById("modificado").value==1){
        $("#modificacionCorrecta").delay(10).slideToggle("slow");
        $("#modificacionCorrecta").delay(5000).slideToggle("slow");
    }
    if(document.getElementById("modificado").value==2){
         $("#imagenBien").delay(10).slideToggle("slow");
         $("#imagenBien").delay(5000).slideToggle("slow");
    }

	var post= $.post("rest-php/starter/starter.php",{
			nick:nick,
			animal:	animal2});
		post.done(function(msg){
			var nuevo=JSON.parse(msg);
            animal = new Animal();
            animal.fromCompleteJSON(nuevo);
            //alert(animal.arrayActividad);
            var name;
            
            //duda de esto a esteban, solo aparece una vez 
            $(".nombreAnimal").each(function(index){
                $(this).append(animal.nombre);
            });
            
            $(".claseAnimal").each(function(index){
               $(this).append(animal.clase);
            });
             var numberMessages; 
             $(".numberMessages").each(function(index){
                 numberMessages = document.createTextNode(animal.mensajesSinLeer);
               $(this).append(numberMessages);
            });
            
            
            $(".memberSince").each(function(index){
                var dateSince = document.createTextNode("Miembro desde "+monthInSpanish(animal.fecha_creacion.getMonth())+" de "+animal.fecha_creacion.getFullYear());
                
               $(this).append(dateSince);
            });
            if(animal.ruta_foto_perfil){
            $(".imageProfile").each(function(index){
               $(this).attr("src","usuarios/"+animal.id_persona+"/"+animal.id_animal+"/"+animal.ruta_foto_perfil);
            });
            }else{
             $(".imageProfile").each(function(index){
               $(this).attr("src","usuarios/nofile.png");
            });
            }
             $(".newFollowers").each(function(index){
               $(this).append(animal.arraySeguidoresSinLeer.length);
            });
            $(".newCombat").each(function(index){
               $(this).append(animal.combatesSinLeer(animal.arrayCombatesDefensa).length);
            });

            $(".newTrophies").each(function(index){
               $(this).append(animal.logrosSinVer(animal.arrayLogros).length);
            });
            if(animal.perfil == "1"){
                $("#peticionesAmistad").show();
            }
            $(".followRequests").each(function(index){
               $(this).append(animal.peticiones_amistad);
            });
            $(".followers").each(function(index){
               $(this).append(animal.seguidores);
            });
            $(".following").each(function(index){
               $(this).append(animal.siguiendo);
            });
            $(".ciudadYPoblacion").each(function(index){
               $(this).append(animal.ciudad+", "+animal.poblacion);
            });
            $(".levelAnimal").each(function(index){
               $(this).append(animal.nivel);
            });
            $(".ladrar").on("click",function(){
                var image;
                if(animal.ruta_foto_perfil){
                    image = "usuarios/"+animal.id_persona+"/"+animal.id_animal+"/"+animal.ruta_foto_perfil;
                }else{
                    image = "usuarios/nofile.png";
                }

                swal({
                    title: "GUAU!!",
                    text: "Eres guatástico!",
                    imageUrl: image,
                    timer: 2000,
                    showConfirmButton: false
                });
            });
            $(".experience").each(function(index){
               $(this).append(animal.experiencia);
            });
            $(".combatsWin").each(function(index){
               $(this).append(animal.combatesGanados(animal));
            });
            $(".logrosTotales").each(function(index){
               $(this).append(animal.arrayLogros.length);
            });
            
            var selectclase=document.getElementById("clase");
            var tempoption;
            var temptextnode;
            for (var i=0;i<nuevo.clases.length;i++){
                tempoption=document.createElement("option");
                tempoption.setAttribute("value",nuevo.clases[i]);
                temptextnode=document.createTextNode(nuevo.clases[i]);
                tempoption.appendChild(temptextnode);
                if(nuevo.clases[i]==animal.clase){
                    tempoption.setAttribute("selected","");
                }
                selectclase.appendChild(tempoption);
            }
            document.getElementById("inputName").value = animal.nombre;
            document.getElementById("ciudad").value = animal.ciudad;
            document.getElementById("poblacion").value = animal.poblacion;
            var selectperfil = document.getElementById("perfil");
            for(var i=0; i<selectperfil.length;i++){
                if(selectperfil[i].value == animal.perfil){
                    selectperfil[i].setAttribute("selected","");
                }
            }
            
            var imgMy = document.getElementById("imgActivityToPost");
            
            if(fileExists("../../usuarios/"+animal.id_persona+"/"+animal.id_animal+"/perfil.jpg")){      
                imgMy.setAttribute("src", "usuarios/"+animal.id_persona+"/"+animal.id_animal+"/perfil.jpg");
            }else{
                imgMy.setAttribute("src", "usuarios/nofile.png");
            }
            
            var activity = document.getElementById("activityMany");
            for(var i=0; i<animal.arrayActividad.length; i++){
                var divact = animal.arrayActividad[i].createDiv(animal.arrayActividad[i],animal.id_animal,animal.id_persona);
                var divpsot = document.createElement("div");
                divact.setAttribute("style","background-color: white;");
                divpsot.setAttribute("class","post");
                divpsot.appendChild(divact);
                activity.appendChild(divpsot);
            }
            if(animal.arrayActividad.length==10){
                mostrar_id("botonVerMas");
            }
            
            
		});
    
        $('#activityMessage').on('keypress', function(event){
            if(event.which === 13){           
                
            if(this.value.length>0){
                var parametros = {
                    personToPost:animal.id_persona,
                    animalToPost:animal.id_animal,
                    activityToPost:this.value
                };
                
                $.ajax({
                       data:  parametros,
                        url:   'rest-php/starter/starter.php',
                        type:  'post',
                        success:  function (response) {
                            //alert(response);
                            var nuevo=JSON.parse(response);
                            var nuevaAct = new Actividad();
                            nuevaAct.fromJSON(nuevo.actividad);
                            var activityDom = $("#activityMany");
                            var newDiv = nuevaAct.createDiv(nuevaAct,animal.id_animal,animal.id_persona);
                            $("#activityMany").prepend(newDiv);
                            $('#activityMessage').val("");
                            limiteActividad++;
                        }
                    });
                }   
            }
        });
  
}


function verMasActividad(){
    	var nick=document.getElementById("user").value;
	    var animal2=document.getElementById("animal").value;
        var parametros = {
                    limiteNuevoActividad:limiteActividad,
                    personaNuevoActividad: nick,
			        animalNuevoActividad:  animal2
                };
                
        $.ajax({
                data:  parametros,
                url:   'rest-php/starter/starter.php',
                type:  'post',
                success:  function (response) {
                    var nuevo=JSON.parse(response);
                    var animalNuevo = new Animal();
                    animalNuevo.fromCompleteJSON(nuevo);
                    
                    var activity = document.getElementById("activityMany");
                    for(var i=0; i<animalNuevo.arrayActividad.length; i++){
                        var divact = animalNuevo.arrayActividad[i].createDiv(animalNuevo.arrayActividad[i],animalNuevo.id_animal,animalNuevo.id_persona);
                        var divpsot = document.createElement("div");
                        divact.setAttribute("style","background-color: white;");
                        divpsot.setAttribute("class","post");
                        divpsot.appendChild(divact);
                        activity.appendChild(divpsot);
                        animal.addActividad(animalNuevo.arrayActividad[i]);
                    }
                    if(animalNuevo.arrayActividad.length<10){
                        ocultar_id("botonVerMas");
                    }
                    limiteActividad = limiteActividad + 10;
                }
        }); 
}





function validacionNombre(id){
    var nombre=document.getElementById(id).value;
    if(validarPalabra(nombre)){
        acierto_id(id);
        return true;
    }else{
        error_id(id);
        var name;

        $("#"+id+"Alert").delay(10).slideToggle("slow");
        $("#"+id+"Alert").delay(5000).slideToggle("slow");
        return false;
    }
}


function modificarAnimal(){
    var arrayFallos = new Array();
    arrayFallos.push(validacionNombre("inputName"));
    arrayFallos.push(validacionNombre("poblacion"));
    arrayFallos.push(validacionNombre("ciudad"));

    var flag = true;

    for (var i=0; i<arrayFallos.length;i++){
        if(!arrayFallos[i]){
            flag = false;
        }
    }
    
    if(flag){
        var parametros = {
            user:document.getElementById("user").value,
            nomanimal:document.getElementById("inputName").value,
            clase:document.getElementById("clase").value,
            perfil:document.getElementById("perfil").value,
            ciudad:document.getElementById("ciudad").value,
            poblacion:document.getElementById("poblacion").value,
            idAnimal:document.getElementById("animal").value,
            petisAmistad:animal.peticiones_amistad
        };

        $.ajax({
            data:  parametros,
            url:   'rest-php/starter/starter.php',
            type:  'post',
            /*beforeSend: function () {

             },*/
            success:  function (response) {
                var nuevo=JSON.parse(response);
                if(nuevo!="no"){
                    window.location.href="starter.php?modificado=1";
                }else{
                    $("#failSqlAlert").delay(10).slideToggle("slow");
                    $("#failSqlAlert").delay(5000).slideToggle("slow");
                }
            }

        });
    }  
}

function cambiarImagen(){
    if($("#exampleInputFile").get(0).files.length === 0){
        $("#noImagen").delay(10).slideToggle("slow");
        $("#noImagen").delay(5000).slideToggle("slow");
    }else{
        var form_data = new FormData();
        var file_data = $("#exampleInputFile").prop('files')[0];
        form_data.append('file',file_data);
        form_data.append('userHuman2',document.getElementById("user").value);
        form_data.append('idAnimalImage',document.getElementById("animal").value);

        $.ajax({
            data:  form_data,
            url:   'rest-php/starter/starter.php',
            type:  'post',
            cache: false,
            contentType: false,
            processData: false,
            /*beforeSend: function () {

             },*/
            success:  function (response) {
                var nuevo=JSON.parse(response);
                if(nuevo.respuesta =="si"){
                    window.location.href="starter.php?modificado=2";
                                        
                }else if (nuevo.respuesta == "malFormato"){
                    $("#malFormato").delay(10).slideToggle("slow");
                    $("#malFormato").delay(5000).slideToggle("slow");
                }else{
                    $("#errorImagen").delay(10).slideToggle("slow");
                    $("#errorImagen").delay(5000).slideToggle("slow");
                }
            }

        });
        
        
    }

}


function ladrar(animal){
    var image;
    if(animal.ruta_foto_perfil){
        image = "usuarios/"+animal.id_persona+"/"+animal.id_animal+"/"+animal.ruta_foto_perfil;
    }else{
        image = "usuarios/nofile.png";
    }
        
    swal({
        title: "GUAU!!",
        text: "Eres guatástico!",
        imageUrl: image,
        timer: 2000
    });
    

}

