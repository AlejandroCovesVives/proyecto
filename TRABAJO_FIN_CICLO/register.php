<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GuauBook | Página de registro</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="plugins/AdminLTE-2.3.0/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/AdminLTE-2.3.0/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="plugins/html5shiv-master/dist/html5shiv.min.js"></script>
    <script src="plugins/Respond-master/dest/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition register-page">
    <div class="register-box">
      <div class="register-logo">
        <a href="login.php"><b>Guau</b>Book</a>
      </div>

      <div class="register-box-body">
        <p class="login-box-msg">Registrar un nuevo humano poseedor de mascotas</p>
        <form  id="formulario">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="user" id="user" placeholder="Usuario humano (nick)">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="name" id="name" placeholder="Nombre">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="ape1" id="ape1" placeholder="Primer apellido">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="ape2" id="ape2" placeholder="Segundo apellido">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="email" class="form-control" name="email" id="email" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password2" id="password2" placeholder="Vuelve a escribir la contraseña">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
          </div>
        </form>

        <div class="row">
          <div class="col-xs-6">
            <button class="btn btn-primary btn-block btn-flat" id="limpiar" name="limpiar">Limpiar formulario</button>
            <!--<input type="reset" class="btn btn-primary btn-block btn-flat" id="reset" name="reset" value="Limpiar formulario"></input>-->
            <!--<div class="checkbox icheck">
              <label>
                <input type="checkbox"> Estoy de acuerdo con los <a href="#">términos</a>
              </label>
            </div>-->
          </div><!-- /.col -->
          <div class="col-xs-6">
            <button class="btn btn-primary btn-block btn-flat" id="register" name="register">Registrarse</button>
          </div><!-- /.col -->
        </div>
        <a href="login.php" class="text-center">Ya tengo cuenta registrada</a>
        <div class="alerts">

          <div id="userAlert" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
            <h4><i class="icon fa fa-ban"></i> ¡User mal escrito!</h4>
            El nick no tiene que tener espacios y solo puede contar con letras
          </div>
          <div id="userOcupado" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
            <h4><i class="icon fa fa-ban"></i> ¡Nombre de usuario en uso!</h4>
            Por favor, seleccione otro nombre de usuario
          </div>
          <div id="nameAlert" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
            <h4><i class="icon fa fa-ban"></i> ¡Nombre vacio!</h4>
            Has de rellenar el campo de nombre de usuario
          </div>
          <div id="ape1Alert" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
            <h4><i class="icon fa fa-ban"></i> ¡Apellido primero en blanco!</h4>
            Tienes que introducir tu primer apellido
          </div>
          <div id="ape2Alert" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
            <h4><i class="icon fa fa-ban"></i> ¡Apellido segundo en blanco!</h4>
            Tienes que introducir tu segundo apellido
          </div>
          <div id="emailAlert" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
            <h4><i class="icon fa fa-ban"></i> ¡Email incorrecto!</h4>
            Has de introducir tu email en formato correcto
          </div>
          <div id="passwordAlert" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
            <h4><i class="icon fa fa-ban"></i> ¡Contraseña incorrecta!</h4>
            Tu contraseña ha de tener como mínimo 6 carácteres
          </div>
          <div id="password2Alert" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
            <h4><i class="icon fa fa-ban"></i> ¡Contraseñas no coinciden!</h4>
            Las dos contraseñas tienen que coincidir
          </div>
          <div id="error" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
            <h4><i class="icon fa fa-ban"></i> ¡Error!</h4>
            Ocurrio un error durante la creación de usuario, vuelve a realizar el formulario
          </div>
          <div id="CreacionCorrecta" class="alert alert-success alert-dismissable row" style="display: none; margin-top:10px;">
            <h4>	<i class="icon fa fa-check"></i> ¡Usuario creado correctamente!</h4>
            Prueba a loguearte en la página de login. Puedes acceder pulsando el enlace de "Ya tengo cuenta registrada"
          </div>
          <div id="nickvalido" class="alert alert-success alert-dismissable row" style="display: none; margin-top:10px;">
            <h4>	<i class="icon fa fa-check"></i> ¡Nick disponible!</h4>
            Puedes utilizar este nick para la creacion de usuario
          </div>
        </div>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    <script src="plugins/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="plugins/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/AdminLTE-2.3.0/plugins/iCheck/icheck.min.js"></script>
    <script src="js/funcionesValidaciones/validaciones.js"></script>
    <script src="js/funcionesValidaciones/funcionesjs.js"></script>
    <script src="js/register/register.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
