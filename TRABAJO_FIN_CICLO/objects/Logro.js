/**
 * Created by Administrador on 9/04/16.
 */

"use strict";
function Logro(fecha_logro,id_logro) {
    //por si no se crea el objeto
    if (this === window) {
        return new Logro(fecha_logro,id_logro);
    }


    var _fecha_logro = fecha_logro;
    var _id_logro=id_logro;
    var _descripcion_logro;
    var _visto;
    var _animal;
    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "visto", {
        get: function () {
            return _visto;
        },
        set: function (valor) {
            _visto = valor;
        },
        enumerable: true
    });
    
    Object.defineProperty(this, "fecha_logro", {
        get: function () {
            return _fecha_logro;
        },
        set: function (valor) {
            _fecha_logro = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "animal", {
        get: function () {
            return _animal;
        },
        set: function (valor) {
            _animal = valor;
        },
        enumerable: true
    });
    
    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "id_logro", {
        get: function () {
            return _id_logro;
        },
        set: function (valor) {
            _id_logro = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido1 para poder llamar a la variable privada apellido1
    Object.defineProperty(this, "descripcion_logro", {
        get: function () {
            return _descripcion_logro;
        },
        set: function (valor) {
            _descripcion_logro = valor;
        },
        enumerable: true
    });

}


Logro.prototype.fromJSON=function(objeto){
    
    this.fecha_logro = objeto.fecha_logro;
    this.id_logro = objeto.id_logro;
    this.visto = objeto.visto;
    this.descripcion_logro = objeto.descripcion_logro;
    var animal = new Animal();
    animal.id_animal = objeto.id_animal_user;
    animal.id_persona = objeto.id_persona_user;
    
}