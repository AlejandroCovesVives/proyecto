"use strict";
function MeGustaComentarioFoto(comentario,animalLeGusta) {
    //por si no se crea el objeto
    if (this === window) {
        return new ComentarioFoto(comentario,animalLeGusta);
    }


    var _comentario = comentario;
    var _animalLeGusta=animalLeGusta;
    var _me_gusta;
    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "comentario", {
        get: function () {
            return _comentario;
        },
        set: function (valor) {
            _comentario = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "animalLeGusta", {
        get: function () {
            return _animalLeGusta;
        },
        set: function (valor) {
            _animalLeGusta = valor;
        },
        enumerable: true
    });

      Object.defineProperty(this, "me_gusta", {
        get: function () {
            return _me_gusta;
        },
        set: function (valor) {
            _me_gusta = valor;
        },
        enumerable: true
    });
}


MeGustaComentarioFoto.prototype.fromJSON=function(objeto){
    var comentarioFoto = new ComentarioFoto();
    comentarioFoto.fecha_comentario = objeto.fecha_comentario;
    var foto = new Foto();
    
    foto.fecha_foto = objeto.fecha_foto;
    var animal = new Animal();
    animal.id_animal = objeto.id_animal_user;
    animal.id_persona = objeto.id_persona_user;
    foto.animal = animal;
    
    comentarioFoto.foto = foto;
    this.comentario = comentarioFoto;
    var animal2 = new Animal();
    animal2.id_animal = objeto.id_animal_ledamegusta;
    animal2.id_persona = objeto.id_persona_ledamegusta;
    this.animalLeGusta = animal2;
    
}