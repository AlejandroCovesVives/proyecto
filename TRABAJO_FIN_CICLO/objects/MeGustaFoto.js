"use strict";
function MeGustaFoto(foto,animalLeGusta) {
    //por si no se crea el objeto
    if (this === window) {
        return new MeGustaFoto(foto,animalLeGusta);
    }


    var _foto = foto;
    var _animalLeGusta=animalLeGusta;
    var _me_gusta;
    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "foto", {
        get: function () {
            return _foto;
        },
        set: function (valor) {
            _foto = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "animalLeGusta", {
        get: function () {
            return _animalLeGusta;
        },
        set: function (valor) {
            _animalLeGusta = valor;
        },
        enumerable: true
    });

      Object.defineProperty(this, "me_gusta", {
        get: function () {
            return _me_gusta;
        },
        set: function (valor) {
            _me_gusta = valor;
        },
        enumerable: true
    });
}


MeGustaFoto.prototype.fromJSON=function(objeto){
    var foto = new Foto();
    
    foto.fecha_foto = objeto.fecha_foto;
    var animal = new Animal();
    animal.id_animal = objeto.id_animal_user;
    animal.id_persona = objeto.id_persona_user;
    foto.animal = animal;
    
    this.foto = foto;
    var animal2 = new Animal();
    animal2.id_animal = objeto.id_animal_ledamegusta;
    animal2.id_persona = objeto.id_persona_ledamegusta;
    this.animalLeGusta = animal2;
    
}