/**
 * Created by Administrador on 7/04/16.
 */
"use strict";
function Animal(id_animal,persona, nombre) {
    //por si no se crea el objeto
    if (this === window) {
        return new Animal(id_animal,persona, nombre);
    }


    var _id_animal = id_animal; // el dni del cliente
    var _id_persona;
    var _persona=persona;
    var _nombre=nombre;
    var _clase;
    var _perfil;
    var _nivel;
    var _seguidores;
    var _siguiendo;
    var _peticiones_amistad;
    var _fecha_creacion;
    var _experiencia;
    var _ruta_foto_perfil;
    var _ciudad;
    var _poblacion;
    var _arrayFotos = new Array();
    var _arrayActividad = new Array();
    var _arrayCombatesAtaque = new Array();
    var _arrayCombatesDefensa = new Array();
    var _arraySeguidores = new Array();
    var _arrayLogros = new Array();
    var _arraySiguiendo = new Array();
	var _arrayMensajes = new Array();
    var self = this;
    var _mensajesSinLeer = 0;
    var _arraySeguidoresSinLeer = new Array();

	Object.defineProperty(this,"arraySeguidoresSinLeer", {
        get: function () {
            return _arraySeguidoresSinLeer;
        },
        set: function (valor) {
            _arraySeguidoresSinLeer = valor;
        },
        enumerable: true
    });    
    
    
    
	Object.defineProperty(this,"arrayMensajes", {
        get: function () {
            return _arrayMensajes;
        },
        set: function (valor) {
            _arrayMensajes = valor;
        },
        enumerable: true
    });
    
	Object.defineProperty(this,"mensajesSinLeer", {
        get: function () {
            return _mensajesSinLeer;
        },
        set: function (valor) {
            _mensajesSinLeer = valor;
        },
        enumerable: true
    });	
	
        //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this,"seguidores", {
        get: function () {
            return _seguidores;
        },
        set: function (valor) {
            _seguidores = valor;
        },
        enumerable: true
    });
	
   Object.defineProperty(this,"id_persona", {
        get: function () {
            return _id_persona;
        },
        set: function (valor) {
            _id_persona = valor;
        },
        enumerable: true
    });
    
    
    Object.defineProperty(this,"siguiendo", {
        get: function () {
            return _siguiendo;
        },
        set: function (valor) {
            _siguiendo = valor;
        },
        enumerable: true
    });
    
    Object.defineProperty(this,"peticiones_amistad", {
        get: function () {
            return _peticiones_amistad;
        },
        set: function (valor) {
            _peticiones_amistad = valor;
        },
        enumerable: true
    });
    
    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this,"id_animal", {
        get: function () {
            return _id_animal;
        },
        set: function (valor) {
            _id_animal = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this,"nombre", {
        get: function () {
            return _nombre;
        },
        set: function (valor) {
            _nombre = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido1 para poder llamar a la variable privada apellido1
    Object.defineProperty(this,"persona", {
        get: function () {
            return _persona;
        },
        set: function (valor) {
            _persona = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido2 para poder llamar a apellido2
    Object.defineProperty(this,"clase", {
        get: function () {
            return _clase;
        },
        set: function (valor) {
            _clase = valor;
        },
        enumerable: true
    });



    Object.defineProperty(this,"perfil", {
        get: function () {
            return _perfil;
        },
        set: function (valor) {
            _perfil = valor;
        },
        enumerable: true
    });



    Object.defineProperty(this, "nivel", {
        get: function () {
            return _nivel;
        },
        set: function (valor) {
            _nivel = valor;
        },
        enumerable: true
    });


    Object.defineProperty(this,"fecha_creacion", {
        get: function () {
            return _fecha_creacion;
        },
        set: function (valor) {
            _fecha_creacion = valor;
        },
        enumerable: true
    });



    Object.defineProperty(this, "experiencia", {
        get: function () {
            return _experiencia;
        },
        set: function (valor) {
            _experiencia = valor;
        },
        enumerable: true
    });



    Object.defineProperty(this,"ruta_foto_perfil", {
        get: function () {
            return _ruta_foto_perfil;
        },
        set: function (valor) {
            _ruta_foto_perfil = valor;
        },
        enumerable: true
    });


    Object.defineProperty(this,"ciudad", {
        get: function () {
            return _ciudad;
        },
        set: function (valor) {
            _ciudad = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this,"poblacion", {
        get: function () {
            return _poblacion;
        },
        set: function (valor) {
            _poblacion = valor;
        },
        enumerable: true
    });

	
	Object.defineProperty(this,"arrayFotos", {
        get: function () {
            return _arrayFotos;
        },
        set: function (valor) {
            _arrayFotos = valor;
        },
        enumerable: true
    });
	
	
    Object.defineProperty(this, "arrayCombatesAtaque", {
        get: function () {
            return _arrayCombatesAtaque;
        },
        set: function (valor) {
            _arrayCombatesAtaque = valor;
        },
        enumerable: true
    });
	
    Object.defineProperty(this, "arrayCombatesDefensa", {
        get: function () {
            return _arrayCombatesDefensa;
        },
        set: function (valor) {
            _arrayCombatesDefensa = valor;
        },
        enumerable: true
    });
    
	
	Object.defineProperty(this,"arrayActividad", {
        get: function () {
            return _arrayActividad;
        },
        set: function (valor) {
            _arrayActividad = valor;
        },
        enumerable: true
    });
	
	
	Object.defineProperty(this,"arraySeguidores", {
        get: function () {
            return _arraySeguidores;
        },
        set: function (valor) {
            _arraySeguidores = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this,"arraySiguiendo", {
        get: function () {
            return _arraySiguiendo;
        },
        set: function (valor) {
            _arraySiguiendo = valor;
        },
        enumerable: true
    });
	
	Object.defineProperty(this,"arrayLogros", {
        get: function () {
            return _arrayLogros;
        },
        set: function (valor) {
            _arrayLogros = valor;
        },
        enumerable: true
    });

}

Animal.prototype.addActividad=function(actividad){
	this.arrayActividad.push(actividad);
}

Animal.prototype.addActividad=function(actividad){
	this.arrayActividad.push(actividad);
}

Animal.prototype.addFoto=function(foto){
	this.arrayFotos.push(foto);
}

Animal.prototype.addCombateAtaque=function(combate){
	this.arrayCombatesAtaque.push(combate);
}

Animal.prototype.addCombateDefensa=function(combate){
	this.arrayCombatesDefensa.push(combate);
}

Animal.prototype.addSeguidores=function(animalSeguidor){
	this.arraySeguidores.push(animalSeguidor);
}

Animal.prototype.addSiguiendo=function(animalSiguiendo){
	this.arraySiguiendo.push(animalSiguiendo);
}

Animal.prototype.addLogros=function(logro){
	this.arrayLogros.push(logro);
}

Animal.prototype.addMensaje=function(mensaje){
	this.arraySeguidores.push(mensaje);
}

Animal.prototype.addSeguidorNoVisto=function(seguidor){
	this.arraySeguidoresSinLeer.push(seguidor);
}

Animal.prototype.combatesSinLeer=function(combatesDefensa){
	var arrayNuevosCombates = new Array();
    for(var i=0; i<combatesDefensa.length; i++){
        if(combatesDefensa[i].visto_contacto==0){
            arrayNuevosCombates.push(combatesDefensa[i]);
        }
    }
    return arrayNuevosCombates;
}

Animal.prototype.logrosSinVer=function(logros){
	var arrayNuevosLogros = new Array();
    for(var i=0; i<logros.length; i++){
        if(logros[i].visto==0){
            arrayNuevosLogros.push(logros[i]);
        }
    }
    return arrayNuevosLogros;
}

Animal.prototype.combatesGanados=function(animal){
	var num = 0;

    for(var i=0; i<animal.arrayCombatesAtaque.length; i++){
        if(animal.arrayCombatesAtaque[i].animalVencedor.id_animal==animal.id_animal){
            num++;
        }
    }
   for(var i=0; i<animal.arrayCombatesDefensa.length; i++){
        if(animal.arrayCombatesDefensa[i].animalVencedor.id_animal==animal.id_animal){
            num++;
        }
    }
    return num;  
}



Animal.prototype.fromJSON=function(objeto){
    this.id_animal=objeto.id_animal;
    this.id_persona = objeto.id_persona;
    this.nombre=objeto.nombre;
    this.clase=objeto.clase;
    this.perfil=objeto.perfil;
    this.nivel=objeto.nivel;
    var fecha = createDateFromMySql(objeto.fecha_creacion);
    //this.fecha_creacion=objeto.fecha_creacion;
    this.fecha_creacion = fecha;
    this.experiencia=objeto.experiencia;
    this.ruta_foto_perfil=objeto.ruta_foto_perfil;
    this.ciudad=objeto.ciudad;
    this.poblacion=objeto.poblacion;
    this.seguidores = objeto.seguidores;
    this.siguiendo = objeto.siguiendo;
    this.peticiones_amistad = objeto.peticiones_amistad;
    this.mensajesSinLeer = objeto.mensajes_sin_leer;
}

Animal.prototype.fromCompleteJSON = function (objeto){
    
    //datos del animal
    this.fromJSON(objeto.animal.datos_animal);
   /* var persona = new Persona();
    persona.fromJSON(objeto.persona);
    this.persona = persona;*/
    //combates 
    if(objeto.animal.combates){
        var combate;
        for(var i=0; i<objeto.animal.combates.ataque.length; i++){
            combate = new Combate();
            combate.fromJSON(objeto.animal.combates.ataque[i]);
            this.addCombateAtaque(combate);
        }
        for(var i=0; i<objeto.animal.combates.defensa.length; i++){
            combate = new Combate();
            combate.fromJSON(objeto.animal.combates.defensa[i]);
            this.addCombateDefensa(combate);
        }
    }
    //actividad con megusta y comentarios y me gusta de comentarios
    if(objeto.animal.actividad){
            var actividad;
        for(var x = 0; x<objeto.animal.actividad.length; x++){
            actividad = new Actividad();
            actividad.fromJSON(objeto.animal.actividad[x].datos_actividad);

            if(objeto.animal.actividad[x].me_gusta){
                var me_gusta_actividad;
                for(var i=0; i<objeto.animal.actividad[x].me_gusta.length; i++){
                    me_gusta_actividad = new MeGustaActividad();
                    me_gusta_actividad.fromJSON(objeto.animal.actividad[x].me_gusta[i]);
                    actividad.addMegusta(me_gusta_actividad);
                }
            }
            
            if(objeto.animal.actividad[x].comentario){
                var comentarioActividad;
                for(var i=0; i<objeto.animal.actividad[x].comentario.length;i++){
                    comentarioActividad = new ComentarioActividad();
                    comentarioActividad.fromJSON(objeto.animal.actividad[x].comentario[i].datos_comentario);
                    if(objeto.animal.actividad[x].comentario[i].me_gusta_comentario){
                        var meGustaComentarioActividad;
                        for(var j =0; j<objeto.animal.actividad[x].comentario[i].me_gusta_comentario.length;j++){
                            meGustaComentarioActividad = new MeGustaComentarioActividad();
                            meGustaComentarioActividad.fromJSON(objeto.animal.actividad[x].comentario[i].me_gusta_comentario[j]);
                            comentarioActividad.addMegustaComentario(meGustaComentarioActividad);
                        }
                    }
                    actividad.addComentario(comentarioActividad);
                }
            }
            this.addActividad(actividad); 
        } 
    }
    //fotos con comentarios y me gusta de fotos y me gusta de comentarios
    if(objeto.animal.fotos){
        var foto;
        for(var i = 0; i<objeto.animal.fotos.length; i++){
            foto = new Foto();
            foto.fromJSON(objeto.animal.fotos[i].datos);
            var me_gusta;
            if(objeto.animal.fotos[i].me_gusta){
                var meGustaFoto;
                for(var x=0; x<objeto.animal.fotos[i].me_gusta[x]; x++){
                    meGustaFoto = new MeGustaFoto();
                    meGustaFoto.fromJSON(objeto.animal.fotos[i].me_gusta[x]);
                    foto.addMeGustaFoto(meGustaFoto);
                }
            }
            if(objeto.animal.fotos[i].comentarios){
                var comentarioFoto;
                
                for(var z=0; z<objeto.animal.fotos[i].comentarios.length;z++){
                    comentarioFoto = new ComentarioFoto();
                    comentarioFoto.fromJSON(objeto.animal.fotos[i].comentarios[z]);
                    
                    if(objeto.animal.fotos[i].comentarios[z].me_gusta){
                        var me_gusta_comentario_foto;
                        for (var y=0; y<objeto.animal.fotos[i].comentarios[z].me_gusta.length; y++){
                            me_gusta_comentario_foto = new MeGustaComentarioFoto();
                            me_gusta_comentario_foto.fromJSON(objeto.animal.fotos[i].comentarios[z].me_gusta[y]);
                            comentarioFoto.addMegustaComentario(me_gusta_comentario_foto);
                        }
                    }
                    foto.addComentario(comentarioFoto);
                }
            }
            this.addFoto(foto);
        }
    }
    //mensajes
    if(objeto.animal.mensajes){
        var animal;
        var mensaje;
        for(var c =0; c<objeto.animal.mensajes.length; c++){
            mensaje = new Mensaje();
            mensaje.fromJSON(objeto.animal.mensajes[c]);
            this.addMensaje(mensaje);
            }
    }
    //logros
    if(objeto.animal.logros){
        var logro;
        for(var i=0; i<objeto.animal.logros.length;i++){
            logro = new Logro();
            logro.fromJSON(objeto.animal.logros[i]);
            this.addLogros(logro);
        }   
    }
    
    if(objeto.animal.seguidoresSinLeer){
        var seguidorSinLeer;
        
        for(var i=0; i<objeto.animal.seguidoresSinLeer.length;i++){
            seguidorSinLeer = new Seguidor();
            seguidorSinLeer.fromJSON(objeto.animal.seguidoresSinLeer[i]);
            this.addSeguidorNoVisto(seguidorSinLeer);
        } 
        
        
        
    }
    
}
                                               