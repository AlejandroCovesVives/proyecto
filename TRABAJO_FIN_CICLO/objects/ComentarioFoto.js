
"use strict";
function ComentarioFoto(fecha_comentario,foto,animalComenta) {
    //por si no se crea el objeto
    if (this === window) {
        return new ComentarioFoto(fecha_comentario,foto,animalComenta);
    }


    var _fecha_comentario = fecha_comentario;
    var _foto=foto;
    var _animalComenta=animalComenta;
    var _comentario_contacto;
    var _visto;
    var _vecesEditado;
    var _arrayMeGusta;
    var _veces_editado;
    var _fecha_ult_edicion;
    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "fecha_comentario", {
        get: function () {
            return _fecha_comentario;
        },
        set: function (valor) {
            _fecha_comentario = valor;
        },
        enumerable: true
    });
    
    Object.defineProperty(this, "fecha_ult_edicion", {
        get: function () {
            return _fecha_ult_edicion;
        },
        set: function (valor) {
            _fecha_ult_edicion = valor;
        },
        enumerable: true
    });    
    
    Object.defineProperty(this, "veces_editado", {
        get: function () {
            return _veces_editado;
        },
        set: function (valor) {
            _veces_editado = valor;
        },
        enumerable: true
    });    
    
    Object.defineProperty(this, "vecesEditado", {
        get: function () {
            return _vecesEditado;
        },
        set: function (valor) {
            _vecesEditado = valor;
        },
        enumerable: true
    });
    
    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "foto", {
        get: function () {
            return _foto;
        },
        set: function (valor) {
            _foto = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido1 para poder llamar a la variable privada apellido1
    Object.defineProperty(this, "animalComenta", {
        get: function () {
            return _animalComenta;
        },
        set: function (valor) {
            _animalComenta = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido2 para poder llamar a apellido2
    Object.defineProperty(this, "comentario_contacto", {
        get: function () {
            return _comentario_contacto;
        },
        set: function (valor) {
            _comentario_contacto = valor;
        },
        enumerable: true
    });



    Object.defineProperty(this, "arrayMeGusta", {
        get: function () {
            return _arrayMeGusta;
        },
        set: function (valor) {
            _arrayMeGusta = valor;
        },
        enumerable: true
    });



    Object.defineProperty(this, "visto", {
        get: function () {
            return _visto;
        },
        set: function (valor) {
            _visto = valor;
        },
        enumerable: true
    });

}

ComentarioFoto.prototype.addMegustaComentario = function (meGustaComentario){
	this.arrayMeGusta.push(meGustaComentario);
}

ComentarioFoto.prototype.fromJSON=function(objeto){
    
	this.fecha_comentario = objeto.fecha_comentario;
    var foto = new Foto();
    
    foto.fecha_foto = objeto.fecha_foto;
    var animal = new Animal();
    animal.id_animal = objeto.id_animal_user;
    animal.id_persona = objeto.id_persona_user;
    foto.animal = animal;
    
    var animalConctacto = new Animal();
    animalConctacto.id_animal = objeto.id_animal_contacto;
    animalConctacto.id_persona = objeto.id_persona_contacto;
    this.animalComenta = animalConctacto;
    this.comentario_contacto = objeto.comentario_contacto;
    this.veces_editado = objeto.veces_editado;
    this.visto = objeto.visto_user;
    this.vecesEditado = objeto.veces_editado;
	this.fecha_ult_edicion = objeto.fecha_ult_edicion;
}

