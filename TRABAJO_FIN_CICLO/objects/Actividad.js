/**
 * Created by Administrador on 9/04/16.
 */
function Actividad(fecha_act,animal) {
    //por si no se crea el objeto
    if (this === window) {
        return new Actividad(fecha_act, animal);
    }


    var _fechaAct = fecha_act;
    var _animal = animal;
    var _arrayMeGusta = new Array();
    var _mensaje;
    var _vecesEditado;
    var _fecha_ult_edicion;
    var _arrayComentarioActividad = new Array();

    var self = this;


    Object.defineProperty(this, "vecesEditado", {
        get: function () {
            return _vecesEditado;
        },
        set: function (valor) {
            _vecesEditado = valor;
        },
        enumerable: true
    });
    
    
    
    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "fechaAct", {
        get: function () {
            return _fechaAct;
        },
        set: function (valor) {
            _fechaAct = valor;
        },
        enumerable: true
    });
    
    Object.defineProperty(this, "fecha_ult_edicion", {
        get: function () {
            return _fecha_ult_edicion;
        },
        set: function (valor) {
            _fecha_ult_edicion = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "animal", {
        get: function () {
            return _animal;
        },
        set: function (valor) {
            _animal = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "arrayMeGusta", {
        get: function () {
            return _arrayMeGusta;
        },
        set: function (valor) {
            _arrayMeGusta = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "mensaje", {
        get: function () {
            return _mensaje;
        },
        set: function (valor) {
            _mensaje = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "arrayComentarioActividad", {
        get: function () {
            return _arrayComentarioActividad;
        },
        set: function (valor) {
            _arrayComentarioActividad = valor;
        },
        enumerable: true
    });


}

Actividad.prototype.addComentario = function (comentario){
	this.arrayComentarioActividad.push(comentario);
}

Actividad.prototype.addMegusta = function (meGusta){
	this.arrayMeGusta.push(meGusta);
}

Actividad.prototype.likeOrNot = function(actividades, id_animal, id_persona){
    if(!actividades.arrayMeGusta){
        return false;
    }
    for (var i=0; i<actividades.arrayMeGusta.length; i++){
        if(actividades.arrayMeGusta[i].likeOrNot(actividades.arrayMeGusta[i],id_animal, id_persona)){
            return true;
        }
    }
    return false;
}



Actividad.prototype.fromJSON=function(objeto){
    this.fechaAct=objeto.fecha_act;
    var animal = new Animal();
    animal.id_animal = objeto.id_animal_user;
    animal.id_persona = objeto.id_persona_user;
    this.animal = animal;
    this.mensaje = objeto.mensaje;
    this.vecesEditado = objeto.veces_editado;
    this.fecha_ult_edicion = objeto.fecha_ult_edicion;
}

//para crear el div de actividad mediante dom y append
Actividad.prototype.createDiv = function(actividad, id_animal, id_persona){
        var divboxwidget = document.createElement("div");
        divboxwidget.setAttribute("class","box box-widget");
            var divboxheader = document.createElement("div");
            divboxheader.setAttribute("class","box-header with-border");
                var divuserblock = document.createElement("div");
                divuserblock.setAttribute("class","user-block");
                    var imageuserblock = document.createElement("img");
                    if(fileExists("../../usuarios/"+actividad.animal.id_persona+"/"+actividad.animal.id_animal+"/perfil.jpg")){      imageuserblock.setAttribute("src", "usuarios/"+actividad.animal.id_persona+"/"+actividad.animal.id_animal+"/perfil.jpg");
                    }else{
                        imageuserblock.setAttribute("src", "usuarios/nofile.png");
                    }
                    imageuserblock.setAttribute("class","img-circle");                                            imageuserblock.setAttribute("alt","user image");
                    var animal = getAnimal(actividad.animal.id_animal);
                    var spanname = document.createElement("span");
                    spanname.setAttribute("class","username");
                    var text = document.createTextNode(animal.nombre);
                    var arroute = document.createElement("a");
                    arroute.setAttribute("href","profilePeople.html?id="+actividad.animal.id_animal);
                    arroute.appendChild(text);
                    spanname.appendChild(arroute);
                    var vecesEdit = "";
                    if(actividad.vecesEditado>0){
                        vecesEdit = "Veces editado: "+actividad.vecesEditado+". Última edición: "+getSpanishTime(actividad.fecha_ult_edicion);
                    }

                    var text2 = document.createTextNode("Publicado en "+getSpanishTime(actividad.fechaAct)+vecesEdit);
                    var spandescription = document.createElement("span");
                    spandescription.setAttribute("class","description");
                    spandescription.appendChild(text2);
                    divuserblock.appendChild(imageuserblock);
                    divuserblock.appendChild(spanname);
                    divuserblock.appendChild(spandescription);
            divboxheader.appendChild(divuserblock);
        divboxwidget.appendChild(divboxheader);
    
            var divboxbody =  document.createElement("div");
            divboxbody.setAttribute("class","box-body");
                var textmessage = document.createTextNode(actividad.mensaje);     
                var ptext = document.createElement("p");
                ptext.appendChild(textmessage);
            divboxbody.appendChild(ptext);

            var buttonLike = document.createElement("button");
            buttonLike.setAttribute("class","btn btn-default btn-xs botonMeGustaActividad");
            buttonLike.setAttribute("value", actividad.fechaAct+"%%%"+actividad.animal.id_animal+"%%%"+actividad.animal.id_persona);
            buttonLike.setAttribute("name",actividad.arrayMeGusta.length);
                var ilike = document.createElement("i");
                if(actividad.likeOrNot(actividad, id_animal, id_persona)){
                    ilike.setAttribute("class","fa fa-thumbs-up");
                    buttonLike.setAttribute("class","btn btn-primary btn-xs botonMeGustaActividad");
                }else{
                    ilike.setAttribute("class","fa fa-thumbs-o-up");
                }
    
                var spanNumberLikes = document.createElement("span");
                var textLikes = document.createTextNode(" ¡Ladra!");
                spanNumberLikes.appendChild(ilike);
                spanNumberLikes.appendChild(textLikes);
            buttonLike.appendChild(spanNumberLikes);
    
                var comentariosymegustaactividad = document.createElement("span");
                comentariosymegustaactividad.setAttribute("class","pull-right text-muted");
    
                var spanlikes = document.createElement("span");
                spanlikes.setAttribute("class","megustaactividad");
                spanlikes.setAttribute("value",actividad.fechaAct+"%%%"+actividad.animal.id_animal+"%%%"+actividad.animal.id_persona);
                spanlikes.setAttribute("name",actividad.arrayMeGusta.length);
                var textmegusta = document.createTextNode(actividad.arrayMeGusta.length+" Ladridos");
                spanlikes.appendChild(textmegusta);
    
                var spannumerocomens = document.createElement("span");
                spannumerocomens.setAttribute("class","numeroComensActividad"); spannumerocomens.setAttribute("value",actividad.fechaAct+"%%%"+actividad.animal.id_animal+"%%%"+actividad.animal.id_persona);
                spannumerocomens.setAttribute("name",actividad.arrayComentarioActividad.length);
                var textcomen = document.createTextNode(" - "+actividad.arrayComentarioActividad.length+" Comentarios");
                spannumerocomens.appendChild(textcomen);
                comentariosymegustaactividad.appendChild(spanlikes);
                comentariosymegustaactividad.appendChild(spannumerocomens);
    
            divboxbody.appendChild(buttonLike);
            divboxbody.appendChild(comentariosymegustaactividad);
        divboxwidget.appendChild(divboxbody);
        var boxcoments = document.createElement("div");
        boxcoments.setAttribute("class","box-footer box-comments");
        for(var x=0; x<actividad.arrayComentarioActividad.length;x++){
            boxcoments.appendChild(actividad.arrayComentarioActividad[x].createDivComen(actividad.arrayComentarioActividad[x], id_animal, id_persona));
        }
    boxcoments.setAttribute("id",actividad.fechaAct+"%%%"+actividad.animal.id_animal+"%%%"+actividad.animal.id_persona);
    divboxwidget.appendChild(boxcoments);

    var footer = document.createElement("div");
    footer.setAttribute("class","box-footer");
    var img2 = imageuserblock.cloneNode(true);
    img2.setAttribute("class","img-responsive img-circle img-sm");
        if(fileExists("../../usuarios/"+id_persona+"/"+id_animal+"/perfil.jpg")){         
            img2.setAttribute("src", "usuarios/"+id_persona+"/"+id_animal+"/perfil.jpg");
        }else{
            img2.setAttribute("src", "usuarios/nofile.png");
        }
    footer.appendChild(img2);
        var divimgpush = document.createElement("div");
        divimgpush.setAttribute("class","img-push");
            var inputcoment = document.createElement("input");
            inputcoment.setAttribute("type","text");
            inputcoment.setAttribute("class","form-control input-sm inputComentario");
            inputcoment.setAttribute("placeholder","Responde a la actividad con un guacomentario"); inputcoment.setAttribute("name",actividad.fechaAct+"%%%"+actividad.animal.id_animal+"%%%"+actividad.animal.id_persona+"%%%"+id_animal+"%%%"+id_persona);
        divimgpush.appendChild(inputcoment);
     footer.appendChild(divimgpush);   
    divboxwidget.appendChild(footer);
    
    return divboxwidget;
      
}

