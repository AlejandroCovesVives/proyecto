
"use strict";

function ComentarioActividad(fecha_comen,actividad,animalComenta) {
    //por si no se crea el objeto
    if (this === window) {
        return new ComentarioActividad(fecha_comen, actividad, animalComenta);
    }


    var _fecha_comen = fecha_comen;
    var _actividad = actividad;
    var _animalComenta=animalComenta;
    var _mensaje;
    var _arrayMeGustaComentario = new Array();
    var _veces_editado;
    var _fecha_ult_edicion;

    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "fecha_comen", {
        get: function () {
            return _fecha_comen;
        },
        set: function (valor) {
            _fecha_comen = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "actividad", {
        get: function () {
            return _actividad;
        },
        set: function (valor) {
            _actividad = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "animalComenta", {
        get: function () {
            return _animalComenta;
        },
        set: function (valor) {
            _animalComenta = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "mensaje", {
        get: function () {
            return _mensaje;
        },
        set: function (valor) {
            _mensaje = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "arrayMeGustaComentario", {
        get: function () {
            return _arrayMeGustaComentario;
        },
        set: function (valor) {
            _arrayMeGustaComentario = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "veces_editado", {
        get: function () {
            return _veces_editado;
        },
        set: function (valor) {
            _veces_editado = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "fecha_ult_edicion", {
        get: function () {
            return _fecha_ult_edicion;
        },
        set: function (valor) {
            _fecha_ult_edicion = valor;
        },
        enumerable: true
    });
}

ComentarioActividad.prototype.addMegustaComentario = function (meGustaComentario){
	this.arrayMeGustaComentario.push(meGustaComentario);
}

ComentarioActividad.prototype.likeOrNot = function(comentarios, id_animal, id_persona){
    if(!comentarios.arrayMeGustaComentario){
        return false;
    }
    for (var i=0; i<comentarios.arrayMeGustaComentario.length; i++){
        if(comentarios.arrayMeGustaComentario[i].likeOrNot(comentarios.arrayMeGustaComentario[i],id_animal, id_persona)){
            return true;
        }
    }
    return false;
}



ComentarioActividad.prototype.fromJSON=function(objeto){
	this.fecha_comen = objeto.fecha_comen;
	var actividad = new Actividad();
	var animal = new Animal();
	animal.id_animal = objeto.id_animal_user;
	animal.id_persona = objeto.id_persona_user;
	actividad.animal = animal;
	actividad.fechaAct = objeto.fecha_act;
	this.actividad = actividad;
	this.fecha_comen = objeto.fecha_comen;
	var animalContacto = new Animal();
	animalContacto.id_animal = objeto.id_animal_contacto;
	animalContacto.id_persona = objeto.id_persona_contacto;
	this.animalComenta = animalContacto;
	this.mensaje = objeto.mensaje;
	this.veces_editado = objeto.veces_editado;
	this.fecha_ult_edicion = objeto.fecha_ult_edicion;
}

ComentarioActividad.prototype.createDivComen = function(comentario, id_animal, id_persona){
    var divbox = document.createElement("div");
    
         var imgcoment = document.createElement("img"); if(fileExists("../../usuarios/"+comentario.animalComenta.id_persona+"/"+comentario.animalComenta.id_animal+"/perfil.jpg")){      imgcoment.setAttribute("src", "usuarios/"+comentario.animalComenta.id_persona+"/"+comentario.animalComenta.id_animal+"/perfil.jpg");
        }else{
            imgcoment.setAttribute("src", "usuarios/nofile.png");
        }
        imgcoment.setAttribute("class","img-circle img-sm");                                            
        imgcoment.setAttribute("alt","user image");
        divbox.appendChild(imgcoment);
        
        var commentText = document.createElement("div");
        commentText.setAttribute("class","comment-text");
    
            var animal = getAnimal(comentario.animalComenta.id_animal);
            var text = document.createTextNode(animal.nombre);
            var spanname = document.createElement("span");
            spanname.setAttribute("class","username");
            spanname.appendChild(text);

    
        var buttonLike = document.createElement("button");
            buttonLike.setAttribute("class","btn btn-default btn-xs botonMeGustaComentarioActividad");
            buttonLike.setAttribute("value", comentario.actividad.fechaAct+"%%%"+comentario.actividad.animal.id_animal+"%%%"+comentario.actividad.animal.id_persona+"%%%"+comentario.fecha_comen+"%%%"+comentario.animalComenta.id_animal+"%%%"+comentario.animalComenta.id_persona);
                var ilike = document.createElement("i");
                if(comentario.likeOrNot(comentario, id_animal, id_persona)){
                    ilike.setAttribute("class","fa fa-heart");
                    buttonLike.setAttribute("class","btn btn-primary btn-xs botonMeGustaComentarioActividad");
                }else{
                    ilike.setAttribute("class","fa fa-heart-o");
                }
    
                var spanNumberLikes = document.createElement("span");
                var textLikes = document.createTextNode(" ¡Guau!");
                spanNumberLikes.appendChild(ilike);
                spanNumberLikes.appendChild(textLikes);
        buttonLike.appendChild(spanNumberLikes);
    
                var spantime = document.createElement("span");
                spantime.setAttribute("class","text-muted pull-right");
    
                    var comentariosymegustaactividad = document.createElement("span");
                    comentariosymegustaactividad.setAttribute("class"," text-muted");
                        var spanlikes = document.createElement("span");
                        spanlikes.setAttribute("class","megustacomentarioactividad");
                        spanlikes.setAttribute("value",comentario.actividad.fechaAct+"%%%"+comentario.actividad.animal.id_animal+"%%%"+comentario.actividad.animal.id_persona+"%%%"+comentario.fecha_comen+"%%%"+comentario.animalComenta.id_animal+"%%%"+comentario.animalComenta.id_persona);
                        spanlikes.setAttribute("name",comentario.arrayMeGustaComentario.length);
                        var textmegusta = document.createTextNode("\u00A0\u00A0\u00A0\u00A0"+comentario.arrayMeGustaComentario.length+" Ladridos ");
                        spanlikes.appendChild(textmegusta);
                    comentariosymegustaactividad.appendChild(spanlikes);

                var vecesEdit = "";
                if(comentario.veces_editado>0){
                    vecesEdit = "Veces editado: "+comentario.veces_editado+". Última edición: "+getSpanishTime(comentario.fecha_ult_edicion)+"\u00A0\u00A0\u00A0\u00A0";
                }

                var text2 = document.createTextNode(vecesEdit+"Publicado en "+getSpanishTime(comentario.fecha_comen));
                spantime.appendChild(text2);
    
            var espaces = document.createTextNode("\u00A0\u00A0\u00A0\u00A0");
            spanname.appendChild(spantime);
            spanname.appendChild(espaces);
            spanname.appendChild(buttonLike);
            spanname.appendChild(comentariosymegustaactividad);
        commentText.appendChild(spanname);

            var message = document.createTextNode(comentario.mensaje);
        var p = document.createElement("p");
        p.setAttribute("style", "margin-top: 5px;");
        p.appendChild(message);
        //commentText.appendChild(message);  
        commentText.appendChild(p); 
   divbox.appendChild(commentText); 
            
return  divbox;
    
}
