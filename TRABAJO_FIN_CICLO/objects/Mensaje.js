/**
 * Created by Administrador on 7/04/16.
 */
"use strict";
function Mensaje(fecha_mensaje, animal_user, animal_contacto) {
    //por si no se crea el objeto
    if (this === window) {
        return new Mensaje(fecha_mensaje, animal_user, animal_contacto);
    }


    var _fecha_mensaje = fecha_mensaje; 
    var _animal_user = animal_user;
    var _animal_contacto = animal_contacto;
	var _mensaje;
	var _visto;
	
    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "fecha_mensaje", {
        get: function () {
            return _fecha_mensaje;
        },
        set: function (valor) {
            _fecha_mensaje = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "animal_user", {
        get: function () {
            return _animal_user;
        },
        set: function (valor) {
            _animal_user = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido1 para poder llamar a la variable privada apellido1
    Object.defineProperty(this, "animal_contacto", {
        get: function () {
            return _animal_contacto;
        },
        set: function (valor) {
            _animal_contacto = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido2 para poder llamar a apellido2
    Object.defineProperty(this, "mensaje", {
        get: function () {
            return _mensaje;
        },
        set: function (array) {
            _mensaje = array;
        },
        enumerable: true
    });

    Object.defineProperty(this, "visto", {
        get: function () {
            return _visto;
        },
        set: function (array) {
            _visto = array;
        },
        enumerable: true
    });

}

Mensaje.prototype.fromJSON=function(objeto){
	
	this.fecha_mensaje = objeto.fecha_mensaje;
	var animal_user = new Animal();
	animal_user.id_animal = objeto.id_animal_user;
	animal_user.id_persona = objeto.id_persona_user;
	this.animal_user = animal_user;
	var animal_contacto = new Animal();
	animal_contacto.id_animal = objeto.id_animal_contacto;
	animal_contacto.id_persona = objeto.id_persona_contacto;
	this.animal_contacto = animal_contacto;
	this.mensaje = objeto.mensaje;
	this.visto = objeto.visto;
}


