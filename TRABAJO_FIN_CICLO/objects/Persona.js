/**
 * Created by Administrador on 7/04/16.
 */
"use strict";
function Persona(nick) {
    //por si no se crea el objeto
    if (this === window) {
        return new Persona(nick);
    }


    var _nick = nick; // el dni del cliente
    var _contra;
    var _nombre;
    var _apellido1;
    var _apellido2;
    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "contra", {
        get: function () {
            return _contra;
        },
        set: function (valor) {
            _contra = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "nombre", {
        get: function () {
            return _nombre;
        },
        set: function (valor) {
            _nombre = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido1 para poder llamar a la variable privada apellido1
    Object.defineProperty(this, "apellido1", {
        get: function () {
            return _apellido1;
        },
        set: function (valor) {
            _apellido1 = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido2 para poder llamar a apellido2
    Object.defineProperty(this, "apellido2", {
        get: function () {
            return _apellido2;
        },
        set: function (array) {
            _apellido2 = array;
        },
        enumerable: true
    });

    Object.defineProperty(this, "nick", {
        get: function () {
            return _nick;
        },
        set: function (array) {
            _nick = array;
        },
        enumerable: true
    });

}

Persona.prototype.fromJSON=function(objeto){
	this.nick = objeto.nick;
    this.apellido1 = objeto.apellido1;
    this.apellido2 = objeto.apellido2;
    this.nombre = objeto.nombre;
    this.email = objeto.email;
    this.contra = objeto. contra;
}