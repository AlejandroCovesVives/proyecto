/**
 * Created by Administrador on 9/04/16.
 */
"use strict";
function MeGustaActividad(actividad,animalLeGusta) {
    //por si no se crea el objeto
    if (this === window) {
        return new MeGustaActividad(actividad,animalLeGusta);
    }


    var _actividad = actividad;
    var _animalLeGusta=animalLeGusta;
    var _me_gusta;
    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "actividad", {
        get: function () {
            return _actividad;
        },
        set: function (valor) {
            _actividad = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "animalLeGusta", {
        get: function () {
            return _animalLeGusta;
        },
        set: function (valor) {
            _animalLeGusta = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "me_gusta", {
        get: function () {
            return _me_gusta;
        },
        set: function (valor) {
            _me_gusta = valor;
        },
        enumerable: true
    });
}

MeGustaActividad.prototype.likeOrNot = function(megusta, id_animal, id_persona){
   if(megusta.animalLeGusta.id_animal == id_animal && megusta.animalLeGusta.id_persona == id_persona){
       return true;
   }else{
       return false;
   }
}


MeGustaActividad.prototype.fromJSON=function(objeto){
	var actividad = new Actividad();
	actividad.fecha_act = objeto.fecha_act;
	var animalActividad = new Animal();
	animalActividad.id_animal = objeto.id_animal_user;
	animalActividad.id_persona = objeto.id_persona_user;
	actividad.animal = animalActividad;
	this.actividad = actividad;
    var animal_megusta = new Animal();
    animal_megusta.id_animal = objeto.id_animal_contacto;
    animal_megusta.id_persona = objeto.id_persona_contacto;
    this.animalLeGusta = animal_megusta;
    this.me_gusta = objeto.me_gusta;
}