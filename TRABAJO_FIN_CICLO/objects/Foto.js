/**
 * Created by Administrador on 7/04/16.
 */

"use strict";
function Foto(fecha_foto,animal) {
    //por si no se crea el objeto
    if (this === window) {
        return new Foto(fecha_foto,animal);
    }


    var _fecha_foto = fecha_foto;
    var _animal=animal;
    var _nombre_foto;
    var _comentario_foto;
    var _lugar;
    var _arrayMeGusta = new Array();
    var _arrayComentarios = new Array();
    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "fecha_foto", {
        get: function () {
            return _fecha_foto;
        },
        set: function (valor) {
            _fecha_foto = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "animal", {
        get: function () {
            return _animal;
        },
        set: function (valor) {
            _animal = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido1 para poder llamar a la variable privada apellido1
    Object.defineProperty(this, "nombre_foto", {
        get: function () {
            return _nombre_foto;
        },
        set: function (valor) {
            _nombre_foto = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido2 para poder llamar a apellido2
    Object.defineProperty(this, "comentario_foto", {
        get: function () {
            return _comentario_foto;
        },
        set: function (valor) {
            _comentario_foto = valor;
        },
        enumerable: true
    });



    Object.defineProperty(this, "arrayMeGusta", {
        get: function () {
            return _arrayMeGusta;
        },
        set: function (valor) {
            _arrayMeGusta = valor;
        },
        enumerable: true
    });



    Object.defineProperty(this, "lugar", {
        get: function () {
            return _lugar;
        },
        set: function (valor) {
            _lugar = valor;
        },
        enumerable: true
    });


    Object.defineProperty(this, "arrayComentarios", {
        get: function () {
            return _arrayComentarios;
        },
        set: function (valor) {
            _arrayComentarios = valor;
        },
        enumerable: true
    });

}

Foto.prototype.addMeGustaFoto=function(meGustaFoto){
	this.arrayMeGusta.push(meGustaFoto);
}

Foto.prototype.addComentario=function(comentario){
	this.arrayComentarios.push(comentario);
}

Foto.prototype.fromJSON=function(objeto){
	this.fecha_foto = objeto.fecha_foto;
	var animal = new Animal();
	animal.id_animal = objeto.id_animal_user;
	animal.id_persona = objeto.id_persona_user;
	this.animal = animal;
	this.nombre_foto = objeto.nombre_foto;
	this.comentario_foto = objeto.comentario_foto;
	this.lugar = objeto.lugar;
	
}