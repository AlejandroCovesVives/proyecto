/**
 * Created by Administrador on 7/04/16.
 */

"use strict";
function Seguidor(animalContacto) {
    //por si no se crea el objeto
    if (this === window) {
        return new Seguidor(animalContacto);
    }


    var _animalContacto = animalContacto;
	var _clase; //seguidor o siguiendo
    var _bloqueo_user;
    var _bloqueo_contacto;
    var _aceptado;
    var _visto_por_contacto;
    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "animalContacto", {
        get: function () {
            return _animalContacto;
        },
        set: function (valor) {
            _animalContacto = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "bloqueo_user", {
        get: function () {
            return _bloqueo_user;
        },
        set: function (valor) {
            _bloqueo_user = valor;
        },
        enumerable: true
    });
	
    Object.defineProperty(this, "clase", {
        get: function () {
            return _clase;
        },
        set: function (valor) {
            _clase = valor;
        },
        enumerable: true
    });	

    //funciones get y set de apellido1 para poder llamar a la variable privada apellido1
    Object.defineProperty(this, "bloqueo_contacto", {
        get: function () {
            return _bloqueo_contacto;
        },
        set: function (valor) {
            _bloqueo_contacto = valor;
        },
        enumerable: true
    });

    //funciones get y set de apellido2 para poder llamar a apellido2
    Object.defineProperty(this, "aceptado", {
        get: function () {
            return _aceptado;
        },
        set: function (valor) {
            _aceptado = valor;
        },
        enumerable: true
    });



    Object.defineProperty(this, "visto_por_contacto", {
        get: function () {
            return _visto_por_contacto;
        },
        set: function (valor) {
            _visto_por_contacto = valor;
        },
        enumerable: true
    });


}


Seguidor.prototype.fromJSON=function(objeto){
	var animal = new Animal();
	animal.id_animal = objeto.id_animal_contacto;
	animal.id_persona = objeto.id_persona_contacto;
	this.animal = animal;
	this.bloqueo_user = objeto.bloqueo_user;
	this.bloqueo_contacto = objeto.bloqueo_contacto;
	this.aceptado = objeto.aceptado;
	this.visto_por_contacto = objeto.visto_por_contacto;
		
}