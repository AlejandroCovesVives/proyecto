/**
 * Created by Administrador on 9/04/16.
 */
"use strict";
function MeGustaComentarioActividad(comentarioActividad,animalLeGusta) {
    //por si no se crea el objeto
    if (this === window) {
        return new MeGustaComentarioActividad(comentarioActividad,animalLeGusta);
    }


    var _comentarioActividad = comentarioActividad;
    var _animalLeGusta=animalLeGusta;
    var _me_gusta;
    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "comentarioActividad", {
        get: function () {
            return _comentarioActividad;
        },
        set: function (valor) {
            _comentarioActividad = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "animalLeGusta", {
        get: function () {
            return _animalLeGusta;
        },
        set: function (valor) {
            _animalLeGusta = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "me_gusta", {
        get: function () {
            return _me_gusta;
        },
        set: function (valor) {
            _me_gusta = valor;
        },
        enumerable: true
    });

}

MeGustaComentarioActividad.prototype.likeOrNot = function(megusta, id_animal, id_persona){
   if(megusta.animalLeGusta.id_animal == id_animal && megusta.animalLeGusta.id_persona == id_persona){
       return true;
   }else{
       return false;
   }
}




MeGustaComentarioActividad.prototype.fromJSON=function(objeto){
    this.me_gusta = objeto.me_gusta;
    var animalLeGusta = new Animal();
    animalLeGusta.id_animal = objeto.id_animal_ledamegusta;
    animalLeGusta.id_persona = objeto.id_persona_ledamegusta;
    this.animalLeGusta = animalLeGusta;
    
    var comentarioActividad = new ComentarioActividad();
    var actividad = new Actividad();
    var animal = new Animal();
    animal.id_animal = objeto.id_animal_user;
    animal.id_persona = objeto.id_persona_user;
    actividad.animal = animal;
    actividad.fecha_act = objeto.fecha_act;
    comentarioActividad.actividad = actividad;
    var animalComentario = new Animal();
    animalComentario.id_animal = objeto.id_animal_contacto;
    animalComentario.id_persona = objeto.id_persona_contacto;
    comentarioActividad.animalComenta = animalComentario;
    this.comentarioActividad = comentarioActividad;
}

