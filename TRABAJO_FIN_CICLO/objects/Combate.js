/**
 * Created by Administrador on 9/04/16.
 */
"use strict";
function Combate(animal1,animal2,fecha_combate,animalVencedor) {
    //por si no se crea el objeto
    if (this === window) {
        return new Combate(animal1,animal2,fecha_combate,animalVencedor);
    }


    var _animal1 = animal1;
    var _animal2 = animal2;
    var _fechacombate=fecha_combate;
    var _animalVencedor=animalVencedor;
	var _visto_user;
	var _visto_contacto;
	var _fecha_combate;
    var self = this;


    //funciones get y set de contra para poder llamar a la variable privada contra
    Object.defineProperty(this, "animal1", {
        get: function () {
            return _animal1;
        },
        set: function (valor) {
            _animal1 = valor;
        },
        enumerable: true
    });

    //funciones get y set de nombre para poder llamar a la variable privada nombre
    Object.defineProperty(this, "animal2", {
        get: function () {
            return _animal2;
        },
        set: function (valor) {
            _animal2 = valor;
        },
        enumerable: true
    });

    Object.defineProperty(this, "fecha_combate", {
        get: function () {
            return _fecha_combate;
        },
        set: function (valor) {
            _fecha_combate = valor;
        },
        enumerable: true
    });


    Object.defineProperty(this, "animalVencedor", {
        get: function () {
            return _animalVencedor;
        },
        set: function (valor) {
            _animalVencedor = valor;
        },
        enumerable: true
    });
	
	Object.defineProperty(this, "visto_user", {
        get: function () {
            return _visto_user;
        },
        set: function (valor) {
            _visto_user = valor;
        },
        enumerable: true
    });
	
 
	Object.defineProperty(this, "visto_contacto", {
        get: function () {
            return _visto_contacto;
        },
        set: function (valor) {
            _visto_contacto = valor;
        },
        enumerable: true
    });    
    
    
    
	Object.defineProperty(this, "fechacombate", {
        get: function () {
            return _fechacombate;
        },
        set: function (valor) {
            _fechacombate = valor;
        },
        enumerable: true
    });


}

Combate.prototype.fromJSON=function(objeto){
	var animal1 = new Animal();
	var animal2 = new Animal();
	var animalGana = new Animal();
	
	animal1.id_animal = objeto.id_animal_user;
	animal1.id_persona = objeto.id_persona_user;
	
	animal2.id_animal = objeto.id_animal_contacto;
	animal2.id_persona = objeto.id_persona_contacto;
	
	animalGana.id_animal = objeto.id_animal_vencedor;
	animalGana.id_persona = objeto.id_persona_vencedor;
	
	this.animal1 = animal1;
	this.animal2 = animal2;
	this.animalVencedor = animalGana;
	this.fechacombate = objeto.fecha_combate;
	this.visto_user = objeto.visto_user;
	this.visto_contacto = objeto.visto_contacto;

}
