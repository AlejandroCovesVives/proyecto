<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GuauBook 2 | Pagina de inicio</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="plugins/AdminLTE-2.3.0/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="plugins/AdminLTE-2.3.0/dist/css/skins/skin-blue.min.css">
    <script src="js/funcionesValidaciones/funcionesjs.js"></script> 
    <script src="js/funcionesValidaciones/validaciones.js"></script> 
    <script src="objects/Actividad.js"></script>
    <script src="objects/Animal.js"></script>  
    <script src="objects/Combate.js"></script>
    <script src="objects/ComentarioActividad.js"></script>
    <script src="objects/ComentarioFoto.js"></script>
    <script src="objects/Foto.js"></script>
    <script src="objects/Logro.js"></script>
    <script src="objects/MeGustaActividad.js"></script>
    <script src="objects/MeGustaComentarioActividad.js"></script>
    <script src="objects/MeGustaComentarioFoto.js"></script>
    <script src="objects/Mensaje.js"></script>
    <script src="objects/Persona.js"></script>
    <script src="objects/Seguidor.js"></script>
    <script src="objects/MeGustaFoto.js"></script>
   <!-- <script src="plugins/AdminLTE-2.3.0/dist/js/demo.js"></script> -->
    <script src="plugins/sweetalert-master/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="plugins/sweetalert-master/dist/sweetalert.css">

<!-- for IE support -->
      <!--<script src="bower_components/es6-promise-polyfill/promise.min.js"></script>-->
      
      
      
      
      
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="plugins/html5shiv-master/dist/html5shiv.min.js"></script>
    <script src="plugins/Respond-master/dest/respond.min.js"></script>
    <![endif]-->
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <?php
  session_start();
  if(isset($_SESSION["human"])){
    $user=$_SESSION["human"];

    if(isset($_SESSION["animal"])){
      $animal=$_SESSION["animal"];
    }else{
      if(isset($_POST["animal_id"])){
        $_SESSION["animal"]=$_POST["animal_id"];
        $animal=$_POST["animal_id"];
      }else{
        $animal="";
        header("location:chooseAnimal.php");
      }
    }

  }else{
    header("location:login.php");
  }
    if(isset($_GET["modificado"])){
        $value=1;
    }else{
        $value=0;
    }
  ?>

    

  <body class="hold-transition skin-blue sidebar-mini">
  <input type="hidden" id="user" name="user" value="<?php echo $user; ?>">
  <input type="hidden" id="animal" name="animal" value="<?php echo $animal; ?>">
  <input type="hidden" id="modificado" name="modificado" value="<?php echo $value; ?>">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="starter.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>G</b>B</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Guau</b>Book</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Barra de navegación</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <!-- Menu toggle button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o"></i>
                  <span class="label label-success numberMessages"></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Tienes <span class="messages numberMessages"></span> mensajes sin leer</li>
                  <li>
                    <!-- inner menu: contains the messages -->
                    <ul class="menu" id="menuMessages">
                        
                      <li><!-- start message -->
                        <a href="#">
                          <div class="pull-left">
                            <!-- User Image -->
                            <img src="" class="img-circle imageProfile img-responsive  img-md" alt="User Image">
                          </div>
                          <!-- Message title and timestamp -->
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <!-- The message -->
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
                    </ul><!-- /.menu -->
                      
                      
                  </li>
                  <li class="footer"><a href="#">Ver todos los mensajes</a></li>
                </ul>
              </li><!-- /.messages-menu -->

                             <!-- Notifications Menu -->
              <li class="dropdown notifications-menu">
                <!-- Menu toggle button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user-plus"></i>
                  <span class="label label-success newFollowers"></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Tienes <span class="newFollowers"></span> notificacion/es</li>
                  <li>
                    <!-- Inner Menu: contains the notifications -->
                    <ul class="menu">
                      <li><!-- start notification -->
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> Te sigue/n <span class="newFollowers"></span> nuevo/s animal/es
                        </a>
                      </li><!-- end notification -->
                    </ul>
                  </li>
                  <li class="footer"><a href="#">Ver nuevo/s seguidor/es</a></li>
                </ul>
              </li> 
            
                <li class="dropdown notifications-menu" id="peticionesAmistad" style="display: none;">
                <!-- Menu toggle button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user-plus"></i>
                  <span class="label label-success followRequests"></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header"><span class="followRequests"></span> animales quiere/n seguirte</li>
                  <li>
                    <!-- Inner Menu: contains the notifications -->
                    <ul class="menu">
                      <li><!-- start notification -->
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> Te intentan seguir <span class="followRequests"></span> animal/es
                        </a>
                      </li><!-- end notification -->
                    </ul>
                  </li>
                  <li class="footer"><a href="#">Ver animales que quieren seguirte</a></li>
                </ul>
              </li>               
               
                              <!-- Combat Menu -->
              <li class="dropdown notifications-menu">
                <!-- Menu toggle button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o"></i>
                  <span class="label label-warning newCombat"></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Tienes <span class="newCombat"></span> notificacion/es de combate</li>
                  <li>
                    <!-- Inner Menu: contains the notifications -->
                    <ul class="menu">
                      <li><!-- start notification -->
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> Te has defendido en <span class="newCombat"></span> nuevo/s combate/es que te han retado
                        </a>
                      </li><!-- end notification -->
                    </ul>
                  </li>
                  <li class="footer"><a href="#">Ver todos los resultados</a></li>
                </ul>
              </li>
                
                                           <!-- trophy Menu -->
              <li class="dropdown notifications-menu">
                <!-- Menu toggle button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-trophy"></i>
                  <span class="label label-success newTrophies"></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Tienes <span class="newTrophies"></span> notificacion/es de logros</li>
                  <li>
                    <!-- Inner Menu: contains the notifications -->
                    <ul class="menu">
                      <li><!-- start notification -->
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> Tienes <span class="newTrophies"></span> nuevo/s logro/es por ver
                        </a>
                      </li><!-- end notification -->
                    </ul>
                  </li>
                  <li class="footer"><a href="#">Ver todos los logros</a></li>
                </ul>
              </li>   
                

                
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                 <!-- <img src="plugins/AdminLTE-2.3.0/dist/img/user2-160x160.jpg" class="user-image imageProfile" alt="User Image">-->
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <img src="" class="user-image imageProfile img-responsive img-md" alt="User Image" >
                    <span class="hidden-xs nameAnimal"></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="" class="img-circle imageProfile" alt="User Image">
                    <p>
                      <span class="nombreAnimal"></span>  <span class = "claseAnimal"></span>
                      <small class = "memberSince"></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="col-xs-6 text-center">
                      <a href="followers.php">Seguidores</a>
                    </div>
                    <div class="col-xs-6 text-center">
                      <a href="following.html">Siguiendo</a>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Perfil</a>
                    </div>
                    <div class="pull-right">
                      <a href="chooseAnimal.php" style="margin-right: 10px" class="btn btn-default btn-flat">Cambiar animal</a>
                      <a href="rest-php/login/logout.php" class="btn btn-default btn-flat">Salir</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->

            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="" class="user-image img-circle imageProfile" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><span class="nombreAnimal"></span>  <br>
              <!-- Status -->
                <div> <a href=""><i class="fa fa-paw"></i> <span class = "claseAnimal"></span></a></div>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Buscar animales...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">MI GUAUMENU</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="#"><i class="fa fa-link"></i> <span>Mi perfíl guatástico</span></a></li>
            <li><a href="#"><i class="fa fa-link"></i> <span>Combates virtuales</span></a></li>
            <li><a href="#"><i class="fa fa-link"></i> <span>Mensajes</span></a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-link"></i> <span>Mis animales</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="#">Siguiendo</a></li>
                <li><a href="#">Seguidores</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-trophy"></i> <span>Logros</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="#">Mis logros</a></li>
                <li><a href="#">Logros semanales</a></li>
              </ul>
            </li> 
              
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Actividad animal 
            <!--<small>GuauBook. Tu nueva red social de Animales</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> </a></li>
            <!--<li class="active">Here</li>-->
          </ol>
        </section>
          
          
          
        <!-- Main content -->
        <section class="content">
            <!--<div class="ibox">
                <div class="ibox-content" id="prueba"></div>
             </div>-->
            
            <div class="row">
                <div class="col-md-3">
                              <!-- Profile Image -->
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle imageProfile" src="" alt="User profile picture">
                            <h3 class="profile-username text-center nombreAnimal"></h3>
                            <p class="text-muted text-center"><i class="fa fa-paw"></i> <span class = "claseAnimal"></span></p>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Seguidores</b> <a class="pull-right followers"></a>
                                </li>
                                <li class="list-group-item">
                                    <b>Siguiendo</b> <a class="pull-right following"></a>
                                </li>
                            </ul>
                            <a class="btn btn-primary btn-block ladrar"><b>Ladra</b></a>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <div id="modificacionCorrecta" class="alert alert-success alert-dismissable row" style="display: none; margin:10px;">
                              <h4>	<i class="icon fa fa-check"></i> ¡Modificación correcta del perfil!</h4>
                              Los datos de su animal se han modificado correctamente
                    </div>
                    <div id="imagenBien" class="alert alert-success alert-dismissable row" style="display: none; margin:10px;">
                              <h4>	<i class="icon fa fa-check"></i>¡Imagen de perfil cambiada con éxito!</h4>
                              Disfruta de tu nuevo look Guau!
                    </div>
                    <!-- About Me Box -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sobre mi fantástico yo</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            
                            <strong><i class="fa fa-map-marker margin-r-5"></i> Lugar de residencia</strong>
                            <p class="text-muted ciudadYPoblacion"></p>

                            <hr>
                            <strong><i class="fa fa-book margin-r-5"></i>  Nivel</strong>
                            <p class="text-muted levelAnimal">
                                
                            </p>

                            <hr>

                            <strong><i class="fa fa-file-text-o margin-r-5"></i> Experiencia actual</strong>
                            <p class="experience"></p>
                            
                            
                            <hr>
                            <strong><i class="fa fa-star margin-r-5"></i> Combates ganados</strong>
                            <p class="combatsWin"></p>
                            <hr>
                            <strong><i class="fa fa-trophy margin-r-5"></i> Logros conseguidos</strong>
                            <p class="logrosTotales"></p>
                        </div><!-- /.box-body -->
                        
                    </div><!-- /.box -->
                </div>
                
                <div class="col-md-9">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#activity" data-toggle="tab">Actividad animal</a></li>
                            <li><a href="#timeline" data-toggle="tab">Guautividad de fotos</a></li>
                            <li><a href="#settings" data-toggle="tab">Editar perfil</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="active tab-pane" id="activity">
               
                                <div style=" height:70px; margin-botton:20px;" class="box box-widget text-center" style="border:0;">    
                                    <div class="text-center">
                                        <img alt="user image" id="imgActivityToPost" class="img-responsive img-circle img-md" src="">
                                        <div class="img-push text-center center-block" style="padding-top:15px;">
                                            <input id="activityMessage" placeholder="Comenta una guauactividad o lo que estes olisqueando ¡Guau!" class="form-control input-md " type="text">
                                        </div>
                                    </div>
                                </div>

                                <div id="activityMany"></div>
                                <div class="text-center">
                                  <button style="display: none;" type="button" id="botonVerMas" class="btn btn-info text-center">Ver más actividades de animales</button>
                                </div>
                                
                            </div><!-- /.tab-pane -->
                        
                        
                        
                        
                        
                        <div class="tab-pane" id="timeline">
                                <!-- The timeline -->
                                <ul class="timeline timeline-inverse">
                                    <!-- timeline time label -->
                                    <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2014
                        </span>
                                    </li>
                                    <!-- /.timeline-label -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-envelope bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
                                            <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
                                            <div class="timeline-body">
                                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                                weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                                quora plaxo ideeli hulu weebly balihoo. I'm drunk
                                            </div>
                                            <div class="timeline-footer">
                                                <a class="btn btn-primary btn-xs">Read more</a>
                                                <a class="btn btn-danger btn-xs">Delete</a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-user bg-aqua"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>
                                            <h3 class="timeline-header no-border"><a href="#">Goofy Young</a> accepted your friend request</h3>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-comments bg-yellow"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>
                                            <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
                                            <div class="timeline-body">
                                                Take me to your leader!
                                                ahiahahahaha!
                                                We are more like Germany, ambitious and misunderstood!
                                            </div>
                                            <div class="timeline-footer">
                                                <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <!-- timeline time label -->
                                    <li class="time-label">
                        <span class="bg-green">
                          3 Jan. 2014
                        </span>
                                    </li>
                                    <!-- /.timeline-label -->
                                    <!-- timeline item -->
                                    <li>
                                        <i class="fa fa-camera bg-purple"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>
                                            <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>
                                            <div class="timeline-body">
                                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                                <img src="http://placehold.it/150x100" alt="..." class="margin">
                                            </div>
                                        </div>
                                    </li>
                                    <!-- END timeline item -->
                                    <li>
                                        <i class="fa fa-clock-o bg-gray"></i>
                                    </li>
                                </ul>
                            </div><!-- /.tab-pane -->
                            
                            
                            <div class="tab-pane" id="settings">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-2 control-label">Nombre</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="inputName" id="inputName" placeholder="Nombre animalesco">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="ciudad" class="col-sm-2 control-label">Ciudad</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Ciudad">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="poblacion" class="col-sm-2 control-label">Población</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="poblacion" id="poblacion" placeholder="Población">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="clase" class="col-sm-2 control-label">Tipo de perfil</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="perfil" id="perfil">
                                                <option value="0">Público (se aceptarán las peticiones de amistad si antes eras privado)</option>
                                                <option value="1">Privado</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="clase" class="col-sm-2 control-label">Clase</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="clase" id="clase">
                                        </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile" class="col-sm-2 control-label">Subir foto de perfil</label>
                                        <input id="exampleInputFile" type="file">
                                        <p class="help-block">Cambia tu foto de perfil (se redimensionará cuadrada guau!).</p>
                                    </div>    
                                </form>
                                <div class="form-group text-center">
                                        <div class="">
                                            <button id="cambiarDatos" class="btn btn-danger">Modificar datos</button>
                                            <button id="cambiarFoto" class="btn btn-danger">Cambiar foto de perfil</button>
                                        </div> 
                                </div>
                                <div class="alerts">
                                    <div id="inputNameAlert" class="alert alert-danger alert-dismissable row" style="display: none; margin:30px;">
                                      <h4><i class="icon fa fa-ban"></i> ¡Nombre mal escrito!</h4>
                                      El nombre no tiene que tener espacios y solo puede contar con letras
                                    </div>
                                    <div id="ciudadAlert" class="alert alert-danger alert-dismissable row" style="display: none; margin:30px;">
                                      <h4><i class="icon fa fa-ban"></i> ¡Ciudad en blanco!</h4>
                                      Tienes que introducir el nombre de la ciudad del animal
                                    </div>
                                    <div id="poblacionAlert" class="alert alert-danger alert-dismissable row" style="display: none; margin:30px;">
                                      <h4><i class="icon fa fa-ban"></i> ¡Población en blanco!</h4>
                                      Tienes que introducir el nombre de la población del animal
                                    </div>
                                      <div id="noImagen" class="alert alert-danger alert-dismissable row" style="display: none; margin:30px;">
                                      <h4>	<i class="icon fa fa-check"></i> ¡No hay imagen!</h4>
                                      Tiene que seleccionar una imagen si desea cambiar la foto de perfil
                                    </div>
                                    <div id="malFormato" class="alert alert-danger alert-dismissable row" style="display: none; margin:30px;">
                                      <h4>	<i class="icon fa fa-check"></i> ¡Mal formato de imagen!</h4>
                                      Tiene que subir una imagen en formato "jpg", "jpeg", "gif", "png", "JPG", "GIF" o "PNG
                                    </div>
                                      <div id="errorImagen" class="alert alert-danger alert-dismissable row" style="display: none; margin:30px;">
                                      <h4>	<i class="icon fa fa-check"></i> ¡Ocurrio un error al subir la imagen!</h4>
                                      Seguramente no este subiendo una imagen, por favor compruebe que lo que esta subiendo es una imagen en formato "jpg", "jpeg", "gif", "png", "JPG", "GIF" o "PNG
                                    </div>
                                     <div id="failSqlAlert" class="alert alert-danger alert-dismissable row" style="display: none; margin:30px;">
                                          <h4>	<i class="icon fa fa-check"></i> ¡Ocurrio un error!</h4>
                                          Tus datos no se han modificado porque ocurrio un error en la conexión
                                    </div>
                                  </div>
                            </div><!-- /.tab-pane -->
                        </div><!-- /.tab-content -->
                    </div><!-- /.nav-tabs-custom -->
                </div><!-- /.col -->
                
                
                
                
            </div>
            
            
            
          <!-- Your Page Content Here -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          
          
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2016 Alejandro Coves vives.</strong> Todos los derechos reservados.
      </footer>

      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <script src="plugins/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
      	  <script src="js/starter/starter.js"></script>
    <script src="plugins/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/AdminLTE-2.3.0/plugins/iCheck/icheck.min.js"></script>
	      
	  <script src="plugins/AdminLTE-2.3.0/plugins/fastclick/fastclick.min.js"></script>
<!-- AdminLTE App -->
	  <script src="plugins/AdminLTE-2.3.0/dist/js/app.min.js"></script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
  </body>
</html>
