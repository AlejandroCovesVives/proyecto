<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>GuauBook | Elige animal</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="plugins/AdminLTE-2.3.0/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/AdminLTE-2.3.0/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="plugins/html5shiv-master/dist/html5shiv.min.js"></script>
    <script src="plugins/Respond-master/dest/respond.min.js"></script>
    <![endif]-->
  </head>

  <?php
  session_start();
  if(isset($_SESSION["human"])){
    $user=$_SESSION["human"];
    $_SESSION = Array();
    $_SESSION["human"] = $user;
  }else{
    header("location:login.php");
  }
  ?>

  <body class="hold-transition register-page">
    <input type="hidden" id="user" name="user" value="<?php echo $user; ?>">
    <div class="register-box">
      <div class="register-logo">
        <a href="#"><b>Guau</b>Book</a>
      </div>


      <div class="register-box-body">
        <p class="login-box-msg">Elija una mascota de entre todas o cree un perfil de nueva mascota</p>

        <div id="listaAnimales" class="row">

        </div>


        <div class="animales">
          <button id="nuevo" class="btn btn-primary btn-block btn-flat">Crear otro animal</button>
          <div id="formulario" style="display: none;">
              <form id="formularioform">
                <div class="form-group has-feedback">
                  <input type="text" class="form-control" name="animal" id="animal" placeholder="Nombre del animal">
                  <i class="fa fa-paw form-control-feedback" aria-hidden="true"></i>
                </div>
                <div class="form-group has-feedback">
                  <select class="form-control" name="clase" id="clase">
                    </select>
                </div>
                <div class="form-group has-feedback">
                  <div aria-disabled="false" aria-checked="true" style="position: relative;" class="iradio_flat-green checked">
                    Perfil<br>
                    <input style="position: absolute; opacity: 0;" id="perfil" name="perfil" value="0" class="flat-red" checked="" type="radio"> Publico
                    <input style="position: absolute; opacity: 0;" id="perfil" name="perfil" value="1" class="flat-red"  type="radio">Privado
                  </div>
                </div>
                <div class="form-group has-feedback">
                  <input type="text" class="form-control" name="ciudad" id="ciudad" placeholder="Ciudad">
                  <i class="fa fa-university form-control-feedback" aria-hidden="true"></i>
                </div>
                <div class="form-group has-feedback">
                  <input type="text" class="form-control" name="poblacion" id="poblacion" placeholder="Población">
                  <i class="fa fa-home form-control-feedback" aria-hidden="true"></i>
                </div>
              </form>
              <div class="row">
                <div class="col-xs-6">
                  <button class="btn btn-primary btn-block btn-flat" id="ocultarCrear">Ocultar</button>
                </div>
                <div class="col-xs-6">
                  <button class="btn btn-primary btn-block btn-flat" id="register" name="register">Crear</button>
                </div><!-- /.col -->
              </div>
            </div>
          <div class="alerts">
            <div id="animalAlert" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
              <h4><i class="icon fa fa-ban"></i> ¡Nombre mal escrito!</h4>
              El nombre no tiene que tener espacios y solo puede contar con letras
            </div>
            <div id="ciudadAlert" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
              <h4><i class="icon fa fa-ban"></i> ¡Ciudad en blanco!</h4>
              Tienes que introducir el nombre de la ciudad del animal
            </div>
            <div id="poblacionAlert" class="alert alert-danger alert-dismissable row" style="display: none; margin-top:10px;">
              <h4><i class="icon fa fa-ban"></i> ¡Población en blanco!</h4>
              Tienes que introducir el nombre de la población del animal
            </div>
            <div id="CreacionCorrecta" class="alert alert-success alert-dismissable row" style="display: none; margin-top:10px;">
              <h4>	<i class="icon fa fa-check"></i> ¡Animal creado correctamente!</h4>
              Prueba a loguearte con el pulsando en su imagen por defecto y ¡adentrate en GuauBook!
            </div>

          </div>
        </div>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    <script src="plugins/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="plugins/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/AdminLTE-2.3.0/plugins/iCheck/icheck.min.js"></script>
    <script src="objects/Actividad.js"></script>
    <script src="objects/Animal.js"></script>
    <script src="objects/Combate.js"></script>
    <script src="objects/ComentarioActividad.js"></script>
    <script src="objects/ComentarioFoto.js "></script>
    <script src="objects/Foto.js"></script>
    <script src="objects/Logro.js"></script>
    <script src="objects/MeGustaComentarioActividad.js"></script>
    <script src="objects/MeGustaComentarioFoto.js"></script>
    <script src="objects/Persona.js"></script>
    <script src="objects/Seguidor.js"></script>

    <script src="js/funcionesValidaciones/validaciones.js"></script>
    <script src="js/funcionesValidaciones/funcionesjs.js"></script>
    <script src="js/chooseAnimal/chooseAnimal.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
