<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Guabook Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="plugins/AdminLTE-2.3.0/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/AdminLTE-2.3.0/plugins/iCheck/square/blue.css">

    <script type="text/javascript" src="js/funcionesValidaciones/validaciones.js"></script>
    <script type="text/javascript" src="js/funcionesValidaciones/funcionesjs.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="plugins/html5shiv-master/dist/html5shiv.min.js"></script>
        <script src="plugins/Respond-master/dest/respond.min.js"></script>
    <![endif]-->
  </head>
  <?php
  session_start();
  if(isset($_GET["human"])){
    $user=$_GET["human"];
  }else{
    $user="";
  }
  if(isset($_SESSION["human"])){
    header("location:starter.php");
  }

  ?>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="starter.php"><b>Guau</b>Book</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Conectate para empezar a utilizar GuauBook</p>
        <form action="rest-php/login/login.php" method="post" id=formulario>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="human" name="human" placeholder="Nombre del usuario humano" value="<?php echo $user; ?>">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" id="pass" name="pass" placeholder="Contraseña">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <label><a href="register.php" class="text-center">Registrate para empezar a utilizar GuauBook</a></label>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
            </div><!-- /.col -->
          </div>
        </form>
        <?php
          if(isset($_GET["error"])){
            ?>
        <div id="poblacionAlert" class="alert alert-danger alert-dismissable row" style="margin-top:10px;">
          <h4><i class="icon fa fa-ban"></i>¡Nombre de usuario humano o contraseña incorrecto!</h4>
          Tienes que introducir un nombre valido o introducir la contraseña correctamente
        </div>
        <?php
          }

        ?>
        <!--<a href="#">¿Olvidaste tu contraseña?</a><br>-->
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="plugins/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/AdminLTE-2.3.0/plugins/iCheck/icheck.min.js"></script>
    <script src="js/login/login.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
